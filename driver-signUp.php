<?php

//database connection info db createAccount
include 'connect.php';

if(isset($_POST['driver_signup']))
{
   try {
        $stmt = $db->prepare("INSERT INTO user (user_id,user_type,first_name,last_name,phone,gender,address1,
								city,state,zip_code,email,password,emergency_first_name,emergency_last_name,emergency_phone,timeStamp,active) 
					  VALUES (:user_id,:user_type,:first_name,:last_name,:phone,:gender,:address1,
								:city,:state,:zip_code,:email,:password,:emergency_first_name,:emergency_last_name,:emergency_phone,:timeStamp,:active)"); 
						
						$date = new DateTime();

		
								       
        
		$stmt->bindValue(':user_id', uniqid() );
		$stmt->bindValue(':user_type', 3); //USER_TYPE = DRIVER
		$stmt->bindValue(':first_name', $_POST['first_name']);
		$stmt->bindValue(':last_name', $_POST['last_name']);
		$stmt->bindValue(':phone', $_POST['phone']);
		$stmt->bindValue(':gender', NULL);
		$stmt->bindValue(':address1', NULL);
		$stmt->bindValue(':city', $_POST['city']);
		$stmt->bindValue(':state', NULL);
		$stmt->bindValue(':zip_code', NULL);
		$stmt->bindValue(':email', $_POST['email']);
		$stmt->bindValue(':password', $_POST['password']);
		$stmt->bindValue(':emergency_first_name', NULL);
		$stmt->bindValue(':emergency_last_name', NULL);
		$stmt->bindValue(':emergency_phone', NULL);
		$stmt->bindValue(':timeStamp', $date->format('Y-m-d H:i:s') );
		$stmt->bindValue(':active', 1);


$stmt->execute();

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}


if(isset($_POST['driverDetails']))
{
   try {
        $driverDetails = $db->prepare("INSERT INTO driver_details (driver_details_id, user_id, company_id, driver_account_type, hometown_zip, vehicle_type) 
					  			VALUES (:driver_details_id, :user_id, :company_id, :driver_account_type, :hometown_zip, :vehicle_type)"); 
						
						$date = new DateTime();

		
								       
        
		$driverDetails->bindValue(':driver_details_id', uniqid() );
		$driverDetails->bindValue(':user_id', NULL);
		$driverDetails->bindValue(':company_id', NULL);
		$driverDetails->bindValue(':driver_account_type', $date->format('Y-m-d H:i:s') );
		$driverDetails->bindValue(':hometown_zip', 1);
		$driverDetails->bindValue(':vehicle_type', 1);


$driverDetails->execute();

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}


   $getCompanies = $db->prepare("SELECT * FROM companies");
   $getCompanies->execute();
   
   
   
   


?>






<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Pages - Admin Dashboard UI Kit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    
     <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  street_number_des: 'short_name',
  route_des: 'long_name',
  locality_des: 'long_name',
  administrative_area_level_1_des: 'short_name',
  country_des: 'long_name',
  postal_code_des: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });

  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
 
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
 
  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
     
    });
  }
}
// [END region_geolocation]




    </script>
    
    
    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>




    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
  </head>
 
 
 
 
 
 
 
 
 
  <body class="fixed-header"   onload="initialize()">
    <!-- START PAGE-CONTAINER -->
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom p-l-20 m-b-20">
          <h2 class="text-white">MediCoupe needs partners like you!</h2>
          <br>
          <h4 class="text-white">Make Good Money</h4>
		  <h4 class="text-white">Advance in Your Company</h4>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
      	<div class="padding-25">
     
     
     
     
     
     
     
     
     
     
     
   
                <div class="row">
                  <div class="col-sm-12">
                   
                    
                    <div class="panel panel-transparent">
                      
                      
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-tabs-linetriangle hide">
                        <li class="active">
                          <a data-toggle="tab" href="#fade1"><span>Hello World</span></a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#fade2"><span>Hello Two</span></a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#fade3"><span>Hello Three</span></a>
                        </li>
                      </ul>
       
       <!-------------------------------FORM------------------------------->               
       <form id="driverSignUp" class="p-t-15" role="form" action="login.php" method="post">
                  	<input type="hidden" name="driver_signup" id="driver_signup" value="1"/>
                
                       
                      <!-- Tab panes -->
                      <div class="tab-content p-l-25 p-r-25">
                      
                        <div class="tab-pane fade in active" id="fade1">
                        
                         <div class="row">
                        <div class="col-sm-12">
                        	<h4>User Sign Up</h4>
                        </div>
                        </div>
                      
                              
                              
                               <div class="row">
                               
                               
                               
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>First Name</label>
                    <input type="text" name="first_name" placeholder="John" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Last Name</label>
                    <input type="text" name="last_name" placeholder="Smith" class="form-control" required>
                  </div>
                </div>
              </div>
             
             
             
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Gender</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Male" name="gender" id="male">
                            <label for="male">Male</label>
                            <input type="radio" checked="checked" value="Female" name="gender" id="female">
                            <label for="female">Female</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder="(999) 123-4567" required>
                  </div>
                </div>
              </div>
              
            
              
              <div class="row">
              <div class="col-sm-12">
              	<h4>Account</h4>
              </div>
              </div>
              
              
              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Email</label>
                    <input type="email" name="email" placeholder="We will send loging details to you" class="form-control" required>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Password</label>
                    <input type="password" name="pass" placeholder="Minimum of 4 Charactors" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
              
             <div class="p-t-25"> 
             	<a data-toggle="tab" href="#fade2"><span class="btn btn-success">Continue</span></a>
             </div>
          
          
          
          </div>
                        
                        
                        
                        
                        
                        
                        
                        
 <!------------------------------------------------ SECOND PAGE OF DRIVER SIGN UP FORM ------------------------------------------------------------------------->                       
                        
                        
                        <div class="tab-pane fade" id="fade2">
                        <div class="row">
                        <div class="col-sm-12">
                        	<h4>Driver Details</h4>
                        </div>
                        </div>
                        
                        
                                    
             
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Driver Type</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Individual" name="driver_account_type" id="individual">
                            <label for="individual">Individual</label>
                            <input type="radio" checked="checked" value="Company" name="driver_account_type" id="company">
                            <label for="company">Company</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              
              
              
              
              
              
              <div class="row">
              <div class="col-sm-12">
               <div class="form-group form-group-default">        
               
                 <label>What Company Do you work for?</label>
                    <select class="full-width form-control-custom" name="company_id">
                    
                    
                    <option value="NULL" name="company_id">
                    
                    <span>Select Your Parent Company from the Drop Down</span>
                    
                    </option>

                    
                    
                             <?php while ($row = $getCompanies->fetch(PDO::FETCH_ASSOC)) : ?>
                             
                             
                             
                                                  
                                                   <option value="<? echo $row['company_id'] ;  ?>" name="company_id">
                                                   
                                                   		                                                   
                                                   		<span name="company_id"><? echo $row['company_name'] ;  ?> (<em><? echo $row['company_id'] ;  ?></em>)</span>
                                                   
                                                   </option>
                     
							<?php endwhile; ?>
                      
                      
                    </select>                          
            </div>
              </div>
            </div>
            


		
		
		
		
		
		    <div class="row">
		                <div class="col-sm-12">
		                  <div class="form-group form-group-default">
		                  <label>Vehicle Type</label>
		                       <div>
		                          <div class="radio radio-success">
		                            <input type="radio" value="Black Car" name="vehicle_type" id="black_car">
		                            <label for="individual">Black Car</label>
		                            <input type="radio" checked="checked" value="Ambulette" name="vehicle_type" id="ambulette">
		                            <label for="company">Ambulette</label>
		                          </div>
		                        </div>
		                      </div>
		                </div>
		              </div>
		              
		
		            
		            
		            
		            
		          
		              
		              
		              
		              
		              
		              
		                  <div class="row">
		                <div class="col-sm-12">
		                  <div class="form-group form-group-default" id="locationField">
		                    <label>Hometown</label>
		                    <input id="autocomplete" type="text" name="hometown_zip" placeholder="Enter Your Hometown City or Zip Code" class="form-control"  onFocus="geolocate()" required>
		                  </div>
		                </div>
		              </div>
		              
		    
		
		    <table id="address" hidden="">
		      <tr>
		        <td class="label">Street address</td>
		        <td class="slimField"><input class="field" id="street_number" name="company_address1"
		              disabled="true"></input></td>
		        <td class="wideField" colspan="2"><input class="field" id="route"
		              disabled="true"></input></td>
		      </tr>
		      <tr>
		        <td class="label">City</td>
		        <td class="wideField" colspan="3"><input class="field" id="locality" name="company_city"
		              disabled="true"></input></td>
		      </tr>
		      <tr>
		        <td class="label">State</td>
		        <td class="slimField"><input class="field"
		              id="administrative_area_level_1" name="company_state" disabled="true"></input></td>
		        <td class="label">Zip code</td>
		        <td class="wideField"><input class="field" id="postal_code" name="hometown_zipcode"
		              disabled="true"></input></td>
		      </tr>
		      <tr>
		        <td class="label">Country</td>
		        <td class="wideField" colspan="3"><input class="field"
		              id="country" disabled="true"></input></td>
		      </tr>
		    </table>
		              

             
             
             
             
             
             
             
             
             
                           
              <div class="p-t-25">
               <a data-toggle="tab" href="#fade1"><span class="btn btn-success">Back</span></a>
                <a data-toggle="tab" href="#fade3"><span class="btn btn-success">Continue</span></a>
              </div>
          
          
          </div>
          
          
          
          
          
          
                        
                        <div class="tab-pane fade" id="fade3">
                          <div class="row">
                            <div class="col-md-12">
                              <h3>Follow us &amp; get updated!</h3>
                              <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite celebrities, and breaking news.</p>
                              <br>
                              
                              
                                <div class="p-t-25">
               <a data-toggle="tab" href="#fade2"><span class="btn btn-success">Back</span></a>
                
                
                <button class="btn btn-success">Post</button>
                
              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                      </div> <!--tab-content-->
                      
                    </div>
                  </div>
                  
                  
                
                </div>
          
            <!-- END PANEL -->
            
            </form>
     
     
     
     
     
     
     
     
     
     
		</div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- END PAGE CONTAINER -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
  </body>
</html>
