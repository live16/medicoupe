<?php
/* Connect to an ODBC database using driver invocation */

require_once 'libs/constants.php';


try {
    $db = new PDO(MEDI_DB_DSN, MEDI_DB_USER, MEDI_DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)); // See ./libs/constants.php for details
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage(); exit;
}

require_once 'libs/mailchimp.php';
require_once 'libs/notifi.php';
require_once 'libs/paypal.php';

$NOTIFI = new Notifi($db);
$NOTIFI->set_log(0);
$NOTIFI->set_debug(0);

$MAILCHIMP = new Mailchimp($db);

$PAYPAL = new Paypal($db);


?>
