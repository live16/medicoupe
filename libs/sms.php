<?php

require_once 'trumpia.php';


class  Sms {
	
	private $listName = 'MyContacts';
	private $trumpia = null;
	private $db = null;
	
	public $debug = false;
	
	function __construct($db){
		$this->trumpia = new trumpia;
		$this->db = $db;
	}
	
	
	public function set_debug($bool){
		$this->debug = (bool) $bool;
		$this->trumpia->set_debug($this->debug);
	}
	
	public function set_log($bool){
		$this->trumpia->set_log((bool) $bool);
	}
	
	
	/**
	* 
	* @param string $user_id User id from database
	* @param string $msg SMS message body must be in plain text, Supported Character Set
	* @param string $desc Description of message for your reference.
	* 
	* @return bool
	*/
	public function send($user_id, $msg, $desc='Medicoupe')
	{
		if(!$contact_id = $this->getContactId($user_id)){
			$this->debug('can\'t get $contact_id!');
			return false;
		} 
		#--
		$result = $this->trumpia->sendToContact('FALSE', 'FALSE', 'TRUE', 'FALSE', trim($desc), $contact_id,'','','', trim($msg));
		
		$this->debug("sendToContact result @ ".trim(print_r($result,true)));
		
		return !empty($result->statuscode) && $result->statuscode==1 ? true : false;
		
	}
	
	
	
	
	private function getContactId($user_id)
	{
		$user = $this->db->prepare("SELECT * FROM sms WHERE user_id=:user_id");
		$user->bindValue(':user_id', $user_id);
		$user->execute();
		$row = $user->fetch();
		if(empty($row['contact_id'])){
			return $this->addContact($user_id);	
		}
		#---
		return $row['contact_id'];
	}
	
	
	private function addContact($user_id)
	{
		$stmt = $this->db->prepare("SELECT * FROM user WHERE user_id=:user_id");
		$stmt->bindValue(':user_id', $user_id);
		$stmt->execute();
		$row = $stmt->fetch();
		
		if($row)
		{
			if(strlen(preg_replace('|[^\d]+|','',$row['phone']))!=10){
				$this->debug("Phone error(".$row['phone']."), user_id=$user_id");
				return false;
			}
			
			$result = $this->trumpia->addContact($this->listName, $row['first_name'], $row['last_name'], $row['email'], '', $row['phone']);			
			
			$this->debug("addContact result @ ".trim(print_r($result,true)));
			
			if(!empty($result->statuscode) || !empty($result->contactid))
			{
				$stmt = $this->db->prepare('INSERT INTO sms SET user_id=:user_id, contact_id=:contact_id');
				$stmt->bindValue(':user_id', $user_id);
        		$stmt->bindValue(':contact_id', $result->contactid);
        		#-
        		if($stmt->execute()){
					return $result->contactid;
				}
			}
		}
		else $this->debug('user_id "'.$user_id.'" not exist in table "user"!');
		#--
		return false;
	}
	
	
	private function debug($msg){
		if($this->debug){
			echo 'SMS::'.$msg.PHP_EOL;
		}
	}
	
	
}


?>
