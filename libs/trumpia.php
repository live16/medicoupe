<?php


class Trumpia {
	
	private $apikey = '610821e3ff6e2215e2e6aa76b7f615e8';
	private $api_url = 'http://api.trumpia.com/http/v2';
	
	//  u: medicoupe p: V1ll@nova
	
	private $debug = false;
	private $log  = false;
	
	public function set_debug($bool){
		$this->debug = (bool) $bool;
	}
	
	public function set_log($bool){
		$this->log = (bool) $bool;
	}
	
	
	/**
	* 
	* Add a new contact to your database and to a distribution list, Once the request is completed, a request ID is returned, There can be only one instance of a tool, such as a unique mobile number, on a list; however, the same contact or some of the same contact information may exist on multiple lists.
	* 
	* @param string $list_name *List to add the contact to.
	* @param string $first_name *First name of the contact.
	* @param string $last_name Last name of the contact.
	* @param string $email Email address of the contact.
	* @param int $country_code Mobile phone number's country code, If left blank then it will be assumed to be a US number, (Default : 1)
	* @param int $mobile_number For US, the 10-digit number without the leading 0 or 1, For some international numbers, the leading 0 or 1 must be omitted, No symbols or spaces.
	* @param string $aim AOL Instant Messenger screen name.
	* @param string $send_verification TRUE or FALSE, If set to TRUE, this sends a verification message to each tool before they are added to your distribution list, If the tool is not verified, they will be marked as not verified and cannot be used. If set to FALSE, the verification step is bypassed and contacts will be added directly into your distribution list, A verification is omitted for an international phone number (not US), (Default : FALSE)
	* @param string $custom_verification_msg Character limit of 60 characters.
	* 
	* @return object object or false
	*/
	public function addContact($list_name, $first_name, $last_name='', $email='', $country_code='', $mobile_number='', $aim='', $send_verification='', $custom_verification_msg='')
	{
		if($contact_data = $this->getContactId(2, $mobile_number)){
			return $contact_data;
		}
		
		$post = array();
		foreach(get_defined_vars() as $k=>$v){
			if(!is_array($v)) $post[$k]=$v;
		}
		$result = $this->request(strtolower(__FUNCTION__), $post);		
		return !empty($result->requestID) ? $this->checkResponse($result->requestID) : false;
	}

	
	
	/**
	* 
	* This function is used to send a message to a single contact or multiple selected contacts using their contact ID numbers
	* 
	* @param string $email_mode *Value must be TRUE or FALSE, Used for sending your message via email.
	* @param string $im_mode *Value must be TRUE or FALSE, Used for sending your message via instant messenger.
	* @param string $sms_mode *Value must be TRUE or FALSE, Used for sending your message via mobile text (SMS).
	* @param string $sb_mode *Value must be TRUE or FALSE, Used to enable SmartBlast.
	* @param string $description *Description of message for your reference.
	* @param string $contact_ids *Enter the contact_ids to send the message to, You can specify up to 500 contact_ids, You will have to separate multiple contact_ids with commas (ex: contact_id1, contact_id2, contact_id3,,,).
	* @param string $email_subject Required if email_mode or sb_mode is TRUE, Subject line for email, Plain text only.
	* @param string $email_message Required if email_mode is TRUE, Email message body with support for basic HTML.
	* @param string $im_message Required if im_mode is TRUE, IM message body must be in plain text, Character limit is 500 characters.
	* @param string $sms_message Required if sms_mode is TRUE, SMS message body must be in plain text, Supported Character Set
	* @param string $sb_message Required if sb_mode is TRUE, SmartBlast message body must be in plain text.
	* @param string $change_org_name This parameter is for SMS and Smart Blasts. By setting this parameter, you can temporarily change your organization name for this message blast, The default structure of a mobile-text message is as follows: <Organization Name>: <your message content> Reply HELP for help.
	* @param string $mail_merge_first_name Mail merge replacement for null values if email content contains mail merge variables.
	* @param string $mail_merge_last_name See mail merge feature online in compose email.
	* 
	* @return
	*/
	public function sendToContact($email_mode, $im_mode, $sms_mode, $sb_mode, $description, $contact_ids, $email_subject='', $email_message='', $im_message='', $sms_message='', $sb_message='', $change_org_name='', $mail_merge_first_name='', $mail_merge_last_name='')
	{
		$post = array();
		foreach(get_defined_vars() as $k=>$v){
			if(!is_array($v)) $post[$k]=$v;
		}
		$result = $this->request(strtolower(__FUNCTION__), $post);		
		return !empty($result->requestID) ? $this->checkResponse($result->requestID) : false;
	}
	
	
	
	
	
	
	
	private function getContactId($tool_type, $tool_data)
	{
		$post = array();
		$post['tool_type'] = $tool_type;
		$post['tool_data'] = $tool_data;
		$post['list_name'] = 'MyContacts';
		$result = $this->request(strtolower(__FUNCTION__), $post);
		return !empty($result->contactid) ? $result : false;
	}
	
	
	
	
	private function checkResponse($request_id)
	{
		return $this->request(strtolower(__FUNCTION__),array('request_id'=>$request_id));
	}
	
	
	private function request($target, $post=array())
	{
		
		if($target == 'sendtocontact' && $this->log("target=$target, post=".print_r($post,true))){
			return 'LOG MODE';
		}
		
		$ch=curl_init();
				
		curl_setopt($ch, CURLOPT_URL, $this->api_url.'/'.$target);
		if(!empty($post))
		{
			$post['apikey'] = $this->apikey;
			
			curl_setopt($ch, CURLOPT_POST, 1);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));			
		}
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);		
		$info = curl_getinfo($ch);
		
		if($this->debug){
			print_r($info);
			echo curl_error($ch);
		}

		if (empty($response) || $info['http_code']!=200 ){
			if($this->debug){
				var_dump($response);
			}
			return false;
		}
		return json_decode($response);
	}
	
	
	private function log($msg)
	{
		if($this->log){
			$log_file = $_SERVER['DOCUMENT_ROOT'].'/log.txt';
			$log = date('Y-m-d H:i ')."@ $msg\n";
			file_put_contents($log_file, $log, FILE_APPEND);
			return true;	
		}
		return;
	}
	
	
}


?>
