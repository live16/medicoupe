<?php

require_once 'sms.php';



class Notifi {
	
	private $debug = false;
	
	
	function __construct($db){
		$this->db = $db;
		$this->sms = new sms($db);
		$this->sms->gebug = $this->debug;
	}
	
	public function set_debug($bool){
		$this->debug = (bool) $bool;
		$this->sms->set_debug($this->debug);
	}
	
	public function set_log($bool){
		$this->sms->set_log((bool) $bool);
	}
	
	
	public function driverArrivedMsg($user_id, $appt_id)
	{
		$user = $this->getRowByVal('user',array('user_id'=>$user_id));
		$appt = $this->getRowByVal('appointments',array('appointment_id'=>$appt_id));
		$driver = $this->getRowByVal('user',array('user_id'=>$appt->driver_user_id));
		
		$msg = "$user->first_name, the driver $driver->first_name $driver->last_name arrived";
	}


	
	public function apptStatusChenge($user_id, $appt_id, $status)
	{
		if(!in_array($status,array('', '', '')))
			//return;
		
		$user = $this->getRowByVal('user',array('user_id'=>$user_id));
		$appt = $this->getRowByVal('appointments',array('appointment_id'=>$appt_id));
		$msg = "$user->first_name, status of appointment \"".trim(strtoupper($appt->appointment_title))."\" was changed to ".trim(strtoupper($appt->appointment_status));
		$this->sms->send($user_id, $msg);
	}
	
	
	public function apptDriverAssign($user_id, $appt_id)
	{
		$user = $this->getRowByVal('user',array('user_id'=>$user_id));
		$appt = $this->getRowByVal('appointments',array('appointment_id'=>$appt_id));
		$msg = preg_replace("|\s+|"," ", "$user->first_name, you were assigned to the appointment \"".strtoupper($appt->appointment_title)."\" at ".date("g:i A",strtotime($appt->appointment_time)))." ".date("m/d/y",strtotime($appt->appointment_date));
		$this->sms->send($user_id, $msg);
	}



	
	private function getRowByVal($table, $val_arr)
	{
		$key = key($val_arr);
		$val = $val_arr[$key];
		$query = $this->db->prepare("SELECT * FROM $table WHERE $key=:$key");
		$query->bindValue(":$key", $val);
		$query->execute();
		return $query->fetch(PDO::FETCH_OBJ);
	}
	

	
	
}



?>