<?php


function siteURL(){
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}



/**
* Displays php blocks (includes) located in ./blocks
* 
* @param string $filename Path to the file without extension (php)
* @param array $data Array of variables (key=>value) which will be used inside the block
* 
* @return void
*/
function block($filename, $data=array()){
	extract($data);
	include 'blocks/'.$filename.'.php';
}



?>