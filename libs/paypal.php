<?php


class Paypal {
	
	
	private $db_table = 'paypal';
	
	
	function __construct($db){
		$this->db = $db;
	}
	
	
	
	
	public function getPreApprovalKey()
	{
		$urlArray = array();
		$urlArray['cancelUrl'] = PAYPAL_PAY_CANCEL_URL;
		$urlArray['currencyCode'] = 'USD';
		$urlArray['endingDate'] = date('c',strtotime('+ 1 year'));
		$urlArray['maxAmountPerPayment'] = PAYPAL_MAX_AMOUNT_PER_PAYMENT;
		$urlArray['maxNumberOfPayments'] = PAYPAL_MAX_NUMBER_OF_PAYMENTS;
		$urlArray['maxTotalAmountOfAllPayments'] = PAYPAL_MAX_TOTAL_AMOUNT_OF_ALL_PAYMENTS;
		$urlArray['requestEnvelope.errorLanguage'] = 'en_US';
		$urlArray['returnUrl'] = PAYPAL_PAY_RETURN_URL;
		$urlArray['startingDate'] = date('c');

		$result = $this->request(PAYPAL_PRE_APPROVAL_URL, $urlArray);
		return empty($result['preapprovalKey']) ? null : $result['preapprovalKey'];
	}
	
	
	
	public function savePreApprovalDetails($user_id, $preapprovalKey)
	{
		$urlArray = array();
		$urlArray['preapprovalKey'] = $preapprovalKey;
		$urlArray['requestEnvelope.errorLanguage'] = 'en_US';
		$result = $this->request(PAYPAL_PRE_APPROVAL_DETAILS_URL, $urlArray);
		
		if(empty($result['sender']['accountId'])) return;
		
		$user_email = $result['senderEmail'];
		$PP_id = $result['sender']['accountId'];

		$delete = $this->db->prepare("DELETE FROM paypal WHERE user_id=:user_id");
		$delete->bindValue(':user_id', $user_id);
		$delete->execute();
    
	    $stmt = $this->db->prepare("INSERT INTO paypal SET paypal_id=:paypal_id, user_id=:user_id, paypal_email=:paypal_email, pre_approval_key=:pre_approval_key");
	    $stmt->bindValue(':paypal_id', $PP_id);
	    $stmt->bindValue(':user_id',$user_id);
	    $stmt->bindValue(':paypal_email', $user_email);
	    $stmt->bindValue(':pre_approval_key', $preapprovalKey);
	    return $stmt->execute();
	}
	
	
	
	public function payAppt($user_id, $appt_id)
	{
		if(!$user_paypal = $this->getUserRow($user_id)){
			return;
		}
		
		$appt = $this->db->prepare("SELECT trip_cost FROM appointments WHERE appointment_id = '".$appt_id."'");
	    $appt->execute();
	    $appt = $appt->fetch();
	    
	    if(empty($appt['trip_cost']) || floatval($appt['trip_cost'])==0){
			return;
		}else $amount = floatval($appt['trip_cost']);
		
		
		
		$urlArray = array();
		$urlArray['actionType'] = 'PAY';
		$urlArray['currencyCode'] = 'USD';
		$urlArray['feesPayer'] = 'EACHRECEIVER';
		$urlArray['preapprovalKey'] = $user_paypal->pre_approval_key;
		$urlArray['receiverList.receiver(0).amount'] = $amount;
		$urlArray['receiverList.receiver(0).email'] = PAYPAL_RECEIVER_EMAIL;
		$urlArray['senderEmail'] = $user_paypal->paypal_email;
		$urlArray['returnUrl'] = PAYPAL_PAY_RETURN_URL;
		$urlArray['cancelUrl'] = PAYPAL_PAY_CANCEL_URL;
		$urlArray['requestEnvelope.errorLanguage'] = 'en_US';

		$result = $this->request(PAYPAL_PAY_URL, $urlArray);
		
		if(empty($result['responseEnvelope']['ack']) || $result['responseEnvelope']['ack']!='Success'){
			return;
		}

		$update = $this->db->prepare("UPDATE appointments SET appointment_status='Paid' WHERE appointment_id='$appt_id'");
		$update->execute();

		return true;
	}
	
	
	public function getUserRow($user_id){
		$query = $this->db->prepare("SELECT * FROM $this->db_table WHERE user_id = :user_id");
		$query->bindValue(':user_id', $user_id);
		$query->execute();
		return ($row = $query->fetch(PDO::FETCH_ASSOC)) ? (object) $row : false;
	}
	
	
	private function request($url, Array $query)
	{
		$headers = array();
		$headers[] = "X-PAYPAL-SECURITY-USERID:".PAYPAL_SECURITY_ID;
		$headers[] = "X-PAYPAL-SECURITY-PASSWORD:".PAYPAL_SECURITY_PASSWORD;
		$headers[] = "X-PAYPAL-SECURITY-SIGNATURE:".PAYPAL_SECURITY_SIGNATURE;
		$headers[] = "X-PAYPAL-REQUEST-DATA-FORMAT:NV";
		$headers[] = "X-PAYPAL-RESPONSE-DATA-FORMAT:JSON";
		$headers[] = "X-PAYPAL-APPLICATION-ID:".PAYPAL_APLICATION_ID;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  $url.'?'.http_build_query($query));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$output = curl_exec($ch);
		curl_close($ch);
		return json_decode($output, true);
	}

}



?>