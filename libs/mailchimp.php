<?php


class Mailchimp {
	

	private $api_key = '61e000293885eaa78095dcd26644d925-us10'; 
	private $api_endpoint = 'https://<dc>.api.mailchimp.com/2.0';
    private $verify_ssl   = false;
    private $db;
	
	
	/**
     * Create a new instance
     * @param object $db Database connection
     */
	public function __construct($db){
        list(, $datacentre) = explode('-', $this->api_key);
        $this->api_endpoint = str_replace('<dc>', $datacentre, $this->api_endpoint);
        $this->validateApiKey();
        $this->db = $db;
	}
	
 
    
    public function addEmailToList($user_id)
    {
		$lists = $this->call('lists/list');
		if(empty($lists->data)) return;
		
		$user = $this->db->prepare("SELECT U.user_id as id, U.first_name, U.last_name, U.email, T.type_name FROM user U RIGHT JOIN user_type T ON T.user_type_id=U.user_type WHERE user_id=:user_id");
		$user->bindValue(':user_id', $user_id);
		$user->execute();
		$user = $user->fetch(PDO::FETCH_OBJ);
		if(!$user) return;
		
		foreach($lists->data as $list){
			if(preg_match('|'.$user->type_name.'|i', $list->name)){
				$this->call('lists/subscribe', array(
	                'id'                => $list->id,
	                'email'             => array('email'=>$user->email),
	                'merge_vars'        => array('FNAME'=>$user->first_name, 'LNAME'=>$user->last_name, 'USERID' => $user->id),
	                'double_optin'      => false,
	                'update_existing'   => true,
	                'replace_interests' => false,
	                'send_welcome'      => false,
            	));
				return true;
			}
		}
		return;
	}
    
    
    
    
    
    
    /**
     * Call an API method.
     * @param  string $method The API method to call, e.g. 'lists/list'
     * @param  array  $args   An array of arguments to pass to the method. Will be json-encoded for you.
     * @return array          Associative array of json decoded API response.
     */
    public function call($method, $args = array(), $timeout = 10)
    {
        return $this->makeRequest($method, $args, $timeout);
    }
    
    
    
    /**
     * Validates MailChimp API Key
     */
    private function validateApiKey()
    {
        $request = $this->call('helper/ping');
		if(empty($request)) exit('Mailchimp::API KEY ERROR!!!');
    }
    
    
    /**
     * Performs the underlying HTTP request.
     * @param  string $method The API method to be called
     * @param  array  $args   Assoc array of parameters to be passed
     * @return array          Assoc array of decoded result
     */
    private function makeRequest($method, $args = array(), $timeout = 10)
    {
        $args['apikey'] = $this->api_key;
        $url = $this->api_endpoint.'/'.$method.'.json';
        $json_data = json_encode($args);
        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            $result    = file_get_contents($url, null, stream_context_create(array(
                'http' => array(
                    'protocol_version' => 1.1,
                    'user_agent'       => 'PHP-MCAPI/2.0',
                    'method'           => 'POST',
                    'header'           => "Content-type: application/json\r\n".
                                          "Connection: close\r\n" .
                                          "Content-length: " . strlen($json_data) . "\r\n",
                    'content'          => $json_data,
                ),
            )));
        }
        return $result ? json_decode($result) : false;
    }
	
	
}

?>