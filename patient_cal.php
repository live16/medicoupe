<?php


session_start();
//database connection info db createAccount
//$_SESSION["user_id"] = $row['user_id'];
//$_SESSION["first_name"] = $row['first_name'];
//$_SESSION["last_name"] = $row['last_name'];


if ($_SESSION['user_type'] != 1 ){  //User_type = 1 = Patient
                    //echo htmlentities($row['user_id']);

                    header("Location:login.php");
                    exit;
                    } 







include 'connect.php';
include 'upload.php';


if(isset($_POST['submitted_event']))
{
   try {
        $stmt = $db->prepare("INSERT INTO appointments 
						        (appointment_id,
						        appointment_title,
						        scheduler_user_id,
						        transport_type,
						        destination_zip,
							    destination_mapAddress,
							    pickup_zip,
							    pickup_mapAddress,
							    appointment_date,
							    appointment_time,
							    scheduled_pickup_time,
							    destination_timeStamp,
							    pickup_timeStamp,
							    actual_trip_time,
							    trip_distance,
							    scheduled_trip_time,
							    appointment_request_timeStamp,
							    timeStamp,
							    active,
							    notes,
							    appointment_status,
							    cancelBy_user_id,
							    appointment_cancel_timeStamp,
							    device_type,
							    patient_user_id,
							    driver_user_id,
							    scheduled_destination_time,
							    trip_cost)
                                                                        
                             VALUES 
                             	(:appointment_id,
                             	:appointment_title,
                             	:scheduler_user_id,
                             	:transport_type,
                             	
                             	:destination_zip,
                             	:destination_mapAddress,
                             	:pickup_zip,
                             	:pickup_mapAddress,
                             	:appointment_date,
                             	:appointment_time,
                             	:scheduled_pickup_time,
                             	:destination_timeStamp,
                             	:pickup_timeStamp,
                             	:actual_trip_time,
                             	:trip_distance,
                             	:scheduled_trip_time,
                             	:appointment_request_timeStamp,
                             	:timeStamp,
                             	:active,
                             	:notes,
                             	:appointment_status,
                             	:cancelBy_user_id,
                             	:appointment_cancel_timeStamp,
                             	:device_type,
                             	:patient_user_id,
                             	:driver_user_id,
                             	:scheduled_destination_time,
                             	:trip_cost)");
                             	
                             	
        $id = uniqid();
        //$date->format('U = Y-m-d H:i:s');

        //$date->setTimestamp(1171502725);
        //$date->format('U = Y-m-d H:i:s');
        
        $stmt->bindValue(':appointment_id', uniqid() );
        $stmt->bindValue(':appointment_title', $_POST['appointment_title']);
        $stmt->bindValue(':scheduler_user_id', $_SESSION["user_id"] );
        $stmt->bindValue(':transport_type', $_POST['rideOption']);
    
        $stmt->bindValue(':destination_zip', $_POST['destination_zip']);
        $stmt->bindValue(':destination_mapAddress', $_POST['destination_mapAddress']);
        
        $stmt->bindValue(':pickup_zip', $_POST['pickup_zip']);
        $stmt->bindValue(':pickup_mapAddress', $_POST['pickup_mapAddress']);
        $stmt->bindValue(':appointment_date', $_POST['appointment_date']);
        $stmt->bindValue(':appointment_time', $_POST['appointment_time']);
        $stmt->bindValue(':scheduled_pickup_time', $_POST['scheduled_pickup_time']);
        $stmt->bindValue(':destination_timeStamp', NULL);
        $stmt->bindValue(':pickup_timeStamp', NULL);
        $stmt->bindValue(':actual_trip_time', NULL);
        $stmt->bindValue(':trip_distance', NULL);
        $stmt->bindValue(':scheduled_trip_time', NULL);
        $stmt->bindValue(':appointment_request_timeStamp', NULL);
        
        $stmt->bindValue(':timeStamp', NULL);
        $stmt->bindValue(':active', 1);
        $stmt->bindValue(':notes', $_POST['notes']);
        $stmt->bindValue(':appointment_status', 'Scheduled');
        $stmt->bindValue(':cancelBy_user_id', NULL);
        $stmt->bindValue(':appointment_cancel_timeStamp', NULL);
        $stmt->bindValue(':device_type', 'WEB');
        $stmt->bindValue(':patient_user_id', $_SESSION["user_id"] );
        $stmt->bindValue(':driver_user_id', NULL);
        $stmt->bindValue(':scheduled_destination_time', NULL);
        $stmt->bindValue(':trip_cost', NULL);
        $stmt->execute();
        
        
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
        
        
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}


 $userImage1 = 'user_img/' . $_SESSION["user_id"] . '.jpg';
	 $defaultImage1 = 'assets/img/default-user.png';

	 $image1 = (file_exists($userImage1)) ? $userImage1 : $defaultImage1;
   
   






?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Appointments</title>
    
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    
    
 
    
    
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    
    
    
    
        <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
     
      
    <link href="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
   
   
            <link class="" href="pages/css/themes/calendar.css" rel="stylesheet" type="text/css" />
            <link class="" href="assets/css/style.css" rel="stylesheet" type="text/css" />
         
            
    
         
         
         <link href="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
    
    <link href="assets/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
    
    
    
    
    
     <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
    
    <style>
    
    .font-raleway { 
    	font-family: 'Raleway', sans-serif; 
    }
    
     .font-raleway-bold { 
    	font-family: 'Raleway', sans-serif; 
    	font-weight: 800;
    }
    
    </style>

       

    
    

    
    
    
              
    
    
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    
    
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete_pu, autocomplete_des;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  street_number_des: 'short_name',
  route_des: 'long_name',
  locality_des: 'long_name',
  administrative_area_level_1_des: 'short_name',
  country_des: 'long_name',
  postal_code_des: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete_pu = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_pu')),
      { types: ['geocode'] });
  autocomplete_des = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_des')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete_pu, 'place_changed', function() {
    fillInAddress();
  });
    google.maps.event.addListener(autocomplete_des, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete_pu.getPlace();
  var place = autocomplete_des.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete_pu.setBounds(circle.getBounds());
      autocomplete_des.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
    
    
    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>


<style>
.datepicker{z-index:11051 !important;}

</style>


<style>
.bootstrap-timepicker-widget{z-index:11051 !important;}

</style>




</head>

  
  
  
  
  
  
  
  
  <body class="fixed-header" onload="initialize()">
  
  
  
  
  
  
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container bg-white full-height">
      <!-- START MOBILE CONTROLS -->
      <!-- LEFT SIDE -->
      <div class="pull-up overlayer visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="p-l-5 p-t-15">
          <button class="btn-link toggle-sidebar" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
          </button>
        </div>
        <!-- END ACTION BAR -->
      </div>
      <!-- RIGHT SIDE -->
      <div class="top-right overlayer visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="p-r-5 p-t-15">
          <button class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
            <span class="icon-set menu-hambuger-plus"></span>
          </button>
        </div>
        <!-- END ACTION BAR -->
      </div>
      <!-- END MOBILE CONTROLS -->
      
      
      
      
      
      
       <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger-plus"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        
        
        
        
        <div class="pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline ">
              <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/logo_2x.png" height="40">
            </div>
            
            
            
           
            <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20 ">
            
            
              <li class="p-r-15 inline">
                   <a href="patient_timeline.php"><button href="#" class="pg-indent" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Dynamic View"></button></a>
              </li>
              
             
              <li class="p-r-15 inline active-view">
                   <a href="patient_cal.php"><button href="patient_cal.php" class="pg-calender" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Calendar View"></button></a>
              </li>
              
                <li class="p-r-15 inline">
                  <a href="patient_list.php"><button href="patient_list.php" class="pg-menu_justify" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="List View"></button></a>
              </li>
            </ul>
            <!-- END NOTIFICATIONS LIST -->
            
            
            <a href="#" class="search-link hide " data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> 
         <div class="inline p-l-20">
           <button style="padding-left:15px;" class="btn btn-success btn-rounded btn-animated from-top fa fa-car" data-toggle="quickview" data-toggle-element="#calendar-event" ><span class="title"><i class="pg-plus"> New Ride</i></span></button>
         </div>
            
            
            
                  
             
            
            
            
            </div>
        </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs hide" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
        
        
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
          
          <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="bold"><?php echo($_SESSION["first_name"]);?></span> <span class="text-master"><?php echo($_SESSION["last_name"]);  ?></span>
            </div>
            
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php echo $image1; ?>" alt="" data-src="<?php echo $image1; ?>" data-src-retina="<?php echo $image1; ?>" width="32" height="32">
            </span>
              </button>
                  <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="#userProfile" data-toggle="modal"><i class="fa fa-user"></i> Profile</a>
                </li>
                <li><a href="#PayPal" data-toggle="modal"><i class="fa fa-paypal"></i> Payments</a>
                </li>
                <li ><a href="tel:+18556926873"><i class="fa fa-phone"></i> Help</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="logout.php" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->

          
          
        </div>
      </div>
      <!-- END HEADER -->
      
      
      
      
      
      
      
      
      
      <!-- MODAL STICK UP  -->
    <div class="modal fade stick-up" id="userProfile" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header clearfix text-left ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="p-t-30">
            <span class="font-raleway-bold text-medi fs-40">user <span class="text-medi-dark">profile</span></span>
            </div>
          
            <h5><span>General Information</span></h5>
          </div>
          <div class="modal-body">
         
          
          
          <script>
          
     var switchToInput = function () {
        var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
        });
        $input.addClass("loadNum");
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };
    var switchToSpan = function () {
        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
    }
    $(".loadNum").on("click", switchToInput);
          
          </script>
          
           <div>
          
         
          
          </div>
          
           <form id="form-work" class="form-horizontal" role="form" >
                     
                      
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="fname" placeholder="<?php echo($_SESSION["first_name"]);?>" name="name">
                        </div>
                        
                       
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="lname" placeholder="<?php echo($_SESSION["last_name"]);?>" name="name">
                        </div>
                      </div>


             
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="phone" placeholder="<?php echo($_SESSION["phone"]);?>" name="phone">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" placeholder="<?php echo($_SESSION["email"]);?>" name="email">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="addressname" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="address" placeholder="<?php echo($_SESSION["mapAddress"]);?>" name="mapAddress">
                        </div>
                      </div>
                       <div class="row m-t-10">
                        <div class="col-sm-3">
                          <p> </p>
                        </div>
                        <div class="col-sm-6">
                      
                        
                <button type="button" class="btn btn-success btn-block m-t-5">Update Profile</button>
                          
                        </div>
                      </div>
                    </form>

                      
                      
                                             
                     
                     
         
                                         
                    
                    
                    
                    
                    
   







            
            
            <div class="row">
            
            
            <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Picture</label>
                        
                        <div class="col-sm-9">
                        	<div class="col-sm-4 m-t-20">
                        	
	                        	<div class="thumbnail-wrapper d48 circular m-t-5 m-r-25">
								<img src="<?php echo $image1; ?>" alt="" data-src="<?php echo $image1; ?>" data-src-retina="<?php echo $image1; ?>" width="60" height="60">
								</div>
                        	</div>

							<div class="col-sm-8 m-t-20">
						
							
								<div class="p-t-10">
									<form id="upload" action="patient_cal.php" method="post" enctype="multipart/form-data">
									    Select image to upload  :<br>
										    
									    (Square images work best!)<br><br>
									    
									    <input type="file" name="fileToUpload" id="fileToUpload">
									    <div class="p-t-10">
									    <input class="btn btn-master" type="submit" value="Upload Image" name="submit">
									    </div>
									</form id="upload">
								</div>
							</div>
                        </div>
            </div>

            
            </div>
            <!-- END ROW  -->
            
            
            
            
            
            
            
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP  -->
    
    
    
    
    <!-- MODAL STICK UP  -->
    <div class="modal fade stick-up" id="payPal" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        
        <div class="p-t-10 p-r-10">
        
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
        </div>
          <div class="modal-header clearfix text-left p-t-10">
          
            
            
            	<div class="pull-right p-t-25">
	            	<img src="assets/img/paypal.png" height="40">
	            </div>
	            
	            <div class="p-t-10">
	            <h3>Payment <span class="semi-bold">Information</span></h3>
            <p>We need payment information inorder to process your order</p>
	            </div>

          </div>
          <div class="modal-body p-t-15">
            <form role="form">
             
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>PayPal Username</label>
                      <input type="email" class="form-control">
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>PayPal Password</label>
                      <input type="password" class="form-control">
                    </div>
                  </div>
                </div>
                          
            </form>
            <div class="row">
              <div class="col-sm-8">
                <div class="p-t-20 clearfix p-l-10 p-r-10">
                  <div class="pull-left">
                   
                  </div>
                  <div class="pull-right">
                   
                  </div>
                </div>
              </div>
              <div class="col-sm-4 m-t-10 sm-m-t-10">
                <button type="button" class="btn btn-complete btn-block m-t-5">Connect with PayPal</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP  -->    
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper full-height">
        <!-- START PAGE CONTENT -->
        <div class="content full-width full-height">
        
        
        
        
        
        
        
     
          <!-- START CALENDAR -->
          <div class="calendar">
            <!-- START CALENDAR HEADER-->
            <div class="calendar-header">
              <div class="drager">
                <div class="years" id="years"></div>
              </div>
            </div>
            <div class="options">
              <div class="months-drager drager">
                <div class="months" id="months"></div>
              </div>
              <h4 class="semi-bold date" id="currentDate">&amp;</h4>
              <div class="drager week-dragger">
                <div class="weeks-wrapper" id="weeks-wrapper">
                </div>
              </div>
            </div>
            <!-- START CALENDAR GRID-->
            <div id="calendar" class="calendar-container">
            </div>
            <!-- END CALENDAR GRID-->
          </div>
          <!-- END CALENDAR -->
          
          
          
        </div>
        <!-- END PAGE CONTENT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        
        <li class="hide">
          <a href="#quickview-alerts" data-toggle="tab"><i class="fa fa-warning"></i></a>
        </li>
        <li class="active">
          <a href="#quickview-chat" data-toggle="tab"><i class="fa fa-comment"></i></a>
        </li>
        <li>
          <a href="#quickview-notes" data-toggle="tab"><i class="fa fa-edit"></i></a>
        </li>
      </ul>
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
    
    
    
    
    
    
    
    
    
    
      <!-- Tab panes -->
      <div class="tab-content">
      
      
      
      
      
        <!-- BEGIN Notes !-->
        <div class="tab-pane fade  in no-padding" id="quickview-notes">
          <div class="view-port clearfix quickview-notes" id="note-views">
            <!-- BEGIN Note List !-->
            <div class="view list" id="quick-note-list">
              <div class="toolbar clearfix">
                <ul class="pull-right ">
                  <li>
                    <a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
                  </li>
                  <li>
                    <a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
                  </li>
                </ul>
                <button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
              </div>
              <ul>
                <!-- BEGIN Note Item !-->
                <li data-noteid="1" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox1" type="checkbox" value="1">
                      <label for="qncheckbox1"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox2" type="checkbox" value="1">
                      <label for="qncheckbox2"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox3" type="checkbox" value="1">
                      <label for="qncheckbox3"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="3" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox4" type="checkbox" value="1">
                      <label for="qncheckbox4"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="4" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox5" type="checkbox" value="1">
                      <label for="qncheckbox5"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
              </ul>
            </div>
            <!-- END Note List !-->
            <div class="view note" id="quick-note">
              <div>
                <ul class="toolbar">
                  <li><a href="#" class="close-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="pg-arrow_left"></i></a>
                  </li>
                  <li><a href="#" class="Bold"><i class="fa fa-bold"></i></a>
                  </li>
                  <li><a href="#" class="Italic"><i class="fa fa-italic"></i></a>
                  </li>
                  <li><a href="#" class=""><i class="fa fa-link"></i></a>
                  </li>
                </ul>
                <div class="body">
                  <div>
                    <div class="top">
                      <span>21st april 2014 2:13am</span>
                    </div>
                    <div class="content">
                      <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END Notes !-->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- BEGIN Alerts !-->
        <div class="tab-pane fade no-padding" id="quickview-alerts">
          <div class="view-port clearfix" id="alerts">
            <!-- BEGIN Alerts View !-->
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default navbar-sm">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Notications
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-search"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <!-- BEGIN Alert List !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <!-- BEGIN List Group !-->
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Calendar
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                  
                  
                  
                  
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">David Nester Birthday</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today <br></span>
                          <span class="text-master">All Day</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                      <!-- BEGIN List Group Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    <!-- BEGIN List Group Item!-->
                    
                    
                    
                    
                    
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">Meeting at 2:30</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                    
                    
                  </ul>
                </div>
                <!-- END List Group !-->
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Social
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Jame Smith commented on your status<br></span>
                          <span class="text-master">“Perfection Simplified - Company Revox"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                    
                    
                    
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Jame Smith commented on your status<br></span>
                          <span class="text-master">“Perfection Simplified - Company Revox"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Sever Status
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">12:13AM GTM, 10230, ID:WR174s<br></span>
                          <span class="text-master">Server Load Exceeted. Take action</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
              </div>
              <!-- END Alert List !-->
            </div>
            <!-- EEND Alerts View !-->
          </div>
        </div>
        <!-- END Alerts !-->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <div class="tab-pane fade in active no-padding" id="quickview-chat">
          <div class="view-port clearfix" id="chat">
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-plus"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Customer Support
                    
                  </div>
                  <!-- BEGIN Header Controler !-->
              
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    Recent</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">Old</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
               
               
               
               
              </div>
            </div>
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    MediCoupe Support
                    <div class="fs-11 hint-text">Online</div>
                  </div>
                  <a href="#" class="link text-master inline action p-r-10 pull-right ">
                    <i class="pg-more"></i>
                  </a>
                </div>
              </div>
              <!-- END Header  !-->
              <!-- BEGIN Conversation  !-->
              <div class="chat-inner" id="my-conversation">
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Hello there
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Hey
                  </div>
                </div>
                <!-- END From Them Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Did you check out Pages framework ?
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Its an awesome chat
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Yea
                  </div>
                </div>
                <!-- END From Them Message  !-->
              </div>
              <!-- BEGIN Conversation  !-->
              <!-- BEGIN Chat Input  !-->
              <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="col-xs-8 no-padding">
                    <input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
                  </div>
                  <div class="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                    <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  <!-- START Calendar Events Form -->
  <!-- START submit event Form -->
  <form id="form-event" action="patient_cal.php" method="post" onkeypress="return event.keyCode != 13;">
  
      <input type='hidden' name='submitted_event' id='submitted_event' value='1'/>
    <div class="quickview-wrapper calendar-event" id="calendar-event">
      <div class="view-port clearfix" id="eventFormController">
        <div class="view bg-white">
          <div class="scrollable">
          
            <div class="p-l-30 p-r-30 p-t-20">
            
              <!-- CLOSE SCHEDULE BUTTON -->
              <a class="pg-close text-master link pull-right" data-toggle="quickview" data-toggle-element="#calendar-event" href="#"></a>
              
              
               <h4>Schedule a Ride</h4>
              
            </div>
            
            
            <div class="p-t-15">
              <input id="eventIndex" name="eventIndex" type="hidden">
              <div class="form-group-attached">
              
              
                <div class="form-group form-group-default ">
                  <label>Title</label>
                  <input type="text" class="form-control" id="appointment_title" name="appointment_title" placeholder="event name">
                </div>
                
                
               
                      <div class="form-group form-group-default ">
                  <label>Ride Type</label>

                <div class="radio radio-success">
                      <input type="radio" checked="checked" value="Black Car" name="rideOption" id="yes">
                      <label for="yes">Car</label>
                      <input type="radio" value="Ambulette" name="rideOption" id="no">
                      <label for="no">Ambulette</label>
                    </div>
                  </div>

               
               
               
               
               
               
                
               
                
                   <div class="form-group form-group-default input-group">
                      <label>Appointment Date</label>
                      <input type="date" class="form-control" name="appointment_date" placeholder="MM/DD/YYYY" id="datepicker-component2">
                      <span class="input-group-addon">
                                                  <i class="fa fa-calendar"></i>
                                                </span>
                    </div>


              
                
                             
                <div class="form-group form-group-default input-group bootstrap-timepicker">
                <label>Arrival Time</label>
                          <input id="timepicker" type="text" name="appointment_time" placeholder="When do you need a ride?" class="form-control">
                          <span class="input-group-addon"><i class="pg-clock"></i></span>
                        </div>
                
                
                
                
                
            
    
    
    
    
                <div class="form-group form-group-default">
                  <label>Start Address</label>
                  
                  <input id="autocomplete_pu" class="form-control" name="pickup_mapAddress" placeholder="Pick Up"
             onFocus="geolocate()" type="text"></input>
                <table id="address" hidden>
                  <tr>
                    <td class="label">Street address</td>
                    <td class="slimField"><input class="field" name="pickup_address" id="street_number"
                          disabled="true"></input></td>
                    <td class="wideField" colspan="2"><input class="field" id="route"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">City</td>
                    <td class="wideField" colspan="3"><input class="field" name="pickup_city" id="locality"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field"
                          name="pickup_state" id="administrative_area_level_1" disabled="true"></input></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field" name="pickup_zip" id="postal_code"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">Country</td>
                    <td class="wideField" colspan="3"><input class="field"
                          id="country" disabled="true"></input></td>
                  </tr>
                </table>
             	
                </div>
                
                             
                

                
                
                
                
                <div class="form-group form-group-default">
                  <label>End Address</label>
                  
                  <input id="autocomplete_des" name="destination_mapAddress" class="form-control" placeholder="Destination"
             onFocus="geolocate()" type="text"></input>
                  <table id="address" hidden>
                      <tr>
                        <td class="label">Street address</td>
                        <td class="slimField"><input class="field" name="destination_address" id="street_number_des"
                              disabled="true"></input></td>
                        <td class="wideField" colspan="2"><input class="field" id="route"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">City</td>
                        <td class="wideField" colspan="3"><input class="field" name="destination_city" id="locality_des"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">State</td>
                        <td class="slimField"><input class="field"
                              name="destination_state" id="administrative_area_level_1_des" disabled="true"></input></td>
                        <td class="label">Zip code</td>
                        <td class="wideField"><input class="field" name="destination_zip" id="postal_code_des"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">Country</td>
                        <td class="wideField" colspan="3"><input class="field"
                              id="country" disabled="true"></input></td>
                      </tr>
                    </table>
             	
                </div>
                
               
                
                
                
                
                
                <div class="row clearfix">
                  <div class="form-group form-group-default">
                    <label>Note</label>
                    <textarea class="form-control" placeholder="description" name="notes" id="txtEventDesc"></textarea>
                  </div>
                </div>
                
                
                
                  
      
                <div class="row">
                
                <div class="col-sm-12">
                <!-- START PANEL -->
                <div class="panel panel-transparent p-t-10">
             
            
                   
                    <div class="alert alert-info bordered" role="alert">
                      <p class="pull-left"><strong>Would you like a ride home? Do you have multiple stops?</strong> These are separate rides. Please don't forget to schedule additional rides if you need them!</p>
                      <button class="m-t-5 close hide" data-dismiss="alert"></button>
                      <div class="clearfix"></div>
                    </div>
                 
                </div>
                <!-- END PANEL -->
              </div>

                
                
                </div>
                
              
                
                
                
              </div>
            </div>
            <div class="p-l-30 p-r-30 p-t-30">
              <button id="eventSave" class="btn btn-success btn-cons">Save Event</button>
              <button id="eventDelete" class="btn btn-white"><i class="fa fa-trash-o"></i>
              </button>

            </div>
            
            
          </div>
        </div>
        
        
       
        
        
        
        
        
        
      </div>
    </div>
  </form>
  <!--END submit event-->
  <!-- END Calendar Events Form -->  
  
  
  
  
  
  
  
  
  
  <!-- START Calendar Events Form -->
  <!-- START submit event Form -->
  <form id="form--event2" action="patient_cal.php" method="post">
  
      <input type='hidden' name='submitted_event' id='submitted_event' value='1'/>
    <div class="quickview-wrapper calendar-event" id="calendar-details">
      <div class="view-port clearfix" id="eventFormController">
        <div class="view bg-white">
          <div class="scrollable">
          
            <div class="p-l-30 p-r-30 p-t-20">
            
              <!-- CLOSE SCHEDULE BUTTON -->
              <a class="pg-close text-master link pull-right" data-toggle="quickview" data-toggle-element="#calendar-details" href="#"></a>
              
              
               <h3>Ride Details</h3>
              
            </div>
            
            
            <div class="p-t-15">
              <input id="eventIndex" name="eventIndex" type="hidden">
              <div>
              
              		<span id="appointment-title" class="p-l-10"></span>
              		<br/>
              		
              		<span id="appointment-date" class="p-l-10"></span>
              		<br/>
              		
				  	<span id="appointment-time" class="p-l-10"></span> 
				  	<br/>
				  	
				  	<span id="pickup-address" class="p-l-10"></span>
				  	<br/> 
				  	
				  	<span id="destination-address" class="p-l-10"></span>
				  	<br/>	
				  	
				  	<span id="c-patient" class="p-l-10"></span>
				  	<br/>
				  	
				  	<span id="c-driver" class="p-l-10"></span>
				  	<br/>
				  	
				  	<span id="lbltoTime"></span>
                
         
                
                
                
                
              </div>
            </div>
            <div class="p-l-30 p-r-30 p-t-30 hide">
              <button id="eventSave" class="btn btn-success btn-cons">Save Event</button>
              <button id="eventDelete" class="btn btn-white"><i class="fa fa-trash-o"></i>
              </button>

            </div>
            
            
          </div>
        </div>
        
        
       
        
        
        
        
        
        
      </div>
    </div>
  </form>
  <!--END submit event-->
  <!-- END Calendar Events Form -->    
    
    
    
    
    
    
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!-- START OVERLAY -->
    <div class="overlay hide" data-pages="search">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-20">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          <img class="overlay-brand" src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close text-black fs-16">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Controls !-->
          <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">
          <br>
          <div class="inline-block">
            <div class="checkbox right">
              <input id="checkboxn" type="checkbox" value="1" checked="checked">
              <label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
            </div>
          </div>
          <div class="inline-block m-l-10">
            <p class="fs-13">Press enter to search</p>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- BEGIN Overlay Search Results, This part is for demo purpose, you can add anything you like !-->
        <div class="container-fluid">
          <span>
                <strong>suggestions :</strong>
            </span>
          <span id="overlay-suggestions"></span>
          <br>
          <div class="search-results m-t-40">
            <p class="bold">Pages Search Results</p>
            <div class="row">
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>
                      <img width="50" height="50" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on pages</h5>
                    <p class="hint-text">via john smith</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>T</div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> related topics</h5>
                    <p class="hint-text">via pages</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div><i class="fa fa-headphones large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> music</h5>
                    <p class="hint-text">via pagesmix</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
                    <div><i class="fa fa-facebook large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on facebook</h5>
                    <p class="hint-text">via facebook</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
                    <div><i class="fa fa-twitter large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Tweats on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via twitter</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
                    <div><i class="fa fa-google-plus large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Circles on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via google plus</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
            </div>
          </div>
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>
    <!-- END OVERLAY -->
    
    
    
    
    
    <!-- BEGIN VENDOR JS -->
     <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    
    
    
    

    
    
    <script src="assets/plugins/jquery-ui-touch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/moment/moment-with-locales.min.js"></script>
    <script src="assets/plugins/hammer.min.js"></script>
  
        <!-- END VENDOR JS -->
    
    
    <!-- END VENDOR JS -->
    
    
    
    
    
    
    
    
    
    
    
    <!-- BEGIN CORE TEMPLATE JS -->
     <script src="pages/js/pages.min.js"></script>
    <script src="pages/js/pages.calendar.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    
    
        
        <!--<script src="assets/js/calendar.js" type="text/javascript"></script>-->
    <?php include 'calendar.php'; ?>
    

	
		 <script src="assets/js/form_elements.js" type="text/javascript"></script>
        <script src="assets/js/scripts.js" type="text/javascript"></script>
        
        
        
     
 
    <!-- END PAGE LEVEL JS -->
  </body>
</html>