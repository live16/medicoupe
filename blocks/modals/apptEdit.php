<div class="modal fade slide-up disable-scroll editApptModal" id="editApptModal<?=$appointment_id?>">
	<div class="modal-dialog">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<button type="button" class="p-r-15 p-t-15 close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
				<div class="modal-header clearfix text-left">
					<h4>Edit Appointment</h4>
				</div>
				
				<form class="form-horizontal">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Title</label>
							<div class="col-md-9 input-group">
								<input type="text" class="form-control" name="appointment_title" required="required" value="<?=$appointment_title?>">
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-3 control-label">Date</label>
							<div class="col-md-9 input-group date">
								<input type="text" class="form-control" name="appointment_date"  required="required" placeholder="MM/DD/YY" value="<?=date('m/d/y',strtotime($appointment_date))?>">
								<span class="input-group-addon">
                                	<i class="fa fa-calendar"></i>
                                </span>	
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-3 control-label">Time</label>
							<div class="col-md-9 input-group time">
								<input type="text" class="form-control" name="appointment_time" required="required" value="<?=date("g:i A", strtotime($appointment_time))?>">
								<span class="input-group-addon">
                                	<i class="pg-clock"></i>
                                </span>	
							</div>
						</div> 
						<br/>
						<div class="form-group">
							<label class="col-md-3 control-label">Pick Up</label>
							<div class="col-md-9  input-group">
								<input type="text" class="form-control getGeo" type="text" autocomplete="off" name="pickup_mapAddress"  required="required" value="<?=$pickup_mapAddress?>">
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-3 control-label">Destination</label>
							<div class="col-md-9 input-group">
								<input type="text" class="form-control getGeo" type="text" autocomplete="off" name="destination_mapAddress"  required="required" value="<?=$destination_mapAddress?>">
							</div>
						</div>
						<br/>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success">Save changes</button>
					</div>
					<input type="hidden" name="target" value="editAppt" />
					<input type="hidden" name="appointment_id" value="<?=$appointment_id?>" />
				</form>
				
			</div>
		</div>
	</div>
</div>
