
<script>
function reloadScreen(){
	var url = window.location.href;    
	if (url.indexOf('?') > -1) url += '&preApproval=1';
	else url += '?preApproval=1';

	$('#signupPayPal').modal('hide');
	window.location.href = url;
}

$(document).ready(function(){
	<?if($show_modal):?>
		$('#signupPayPal').modal('show');
	<?endif;?>
});
</script>


<div class="modal fade slide-up disable-scroll" id="signupPayPal" tabindex="-1" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal">
    <div class="modal-content-wrapper">
      <div class="modal-content">
        <div class="modal-body text-center m-t-20">
          <h4 class="no-margin p-b-10"><img src="assets/img/medicoupe.png" height="30px;"> <i class="fa fa-arrows-h"></i> <img src="assets/img/paypal.png" height="30px;"></h4><br>
          <p class="fs-16 font-raleway">MediCoupe has partnered with PayPal to bring you effortless ride transactions. Simply authorize once, and MediCoupe will bill you at the conclusion of each trip.</p><br>
          <button type="button" class="btn btn-success btn-cons" data-dismiss="modal" onclick="reloadScreen()">Connect with PayPal to Get Started</button>
        </div>
      </div>
    </div>
  </div>
</div>


