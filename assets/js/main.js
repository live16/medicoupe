function admin_ajax(data){
	return $.ajax({
	    url: '/admin_ajax.php',
	    type: 'POST',
	    dataType: 'json',
	    async: false,
	    data: data,
	}).responseJSON;
}


function hideAppts(ids){
	var hide_ids = [];
	$('#tableWithSearch .v-align-middle .checkbox input[type=checkbox]:checked').each(function(){
		var appt_id = $(this).attr('id').replace('checkbox_','');
		hide_ids.push(appt_id);
	});

	var data = admin_ajax({target:'hideAppts', ids:hide_ids});
	if(data.code==1){
		document.location.reload();
	}
	else alert("error");
	
	return false;
}




$(document).ready(function(){	
	
	$('#admin-hide-appt').click(function(){
		var hide_ids = $('#tableWithSearch .v-align-middle .checkbox input[type=checkbox]:checked');	
		
		$('#admin-hide-appt-modal').remove();
		var modal_html = '<div class="modal fade" id="admin-hide-appt-modal"> \
		  <div class="modal-dialog"> \
		    <div class="modal-content"> \
		      <div class="modal-header"> \
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> \
		        <h4 class="modal-title">Hide rows</h4> \
		      </div> \
		      <div class="modal-body"> \
		        <p>Pleas confirm hide <b>' + hide_ids.length + '</b> of rows</p> \
		      </div> \
		      <div class="modal-footer"> \
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> \
		        <button type="button" class="btn btn-success" id="admin-hide-appt-btn" onClick="hideAppts()">Hide</button> \
		      </div> \
		    </div> \
		  </div> \
		</div>';

		if(hide_ids.length){
			$('body').append(modal_html);
			$('#admin-hide-appt-modal').modal('show');
		}
		
		return false;
	});
	
	

	$('.editApptModal .getGeo').geocomplete();
	$('.editApptModal .time input').timepicker().on('show.timepicker', function(e) {
            var widget = $('.bootstrap-timepicker-widget');
            widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
            widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
    });
	$('.editApptModal .date').datepicker({format:'mm/dd/yy'});
	$('.editApptModal .time input, .editApptModal .date input').keypress(function(){return false});
	$('.editApptModal form').submit(function(){
		var data = admin_ajax($(this).serialize());
		if(data.code==1) document.location.reload();
		else alert('Error !!!');
		return false;
	});

});