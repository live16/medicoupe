<?php

//database connection info db createAccount
include 'connect.php';




if(isset($_POST['driver_signup']))
{
   try {
        $user = $db->prepare("INSERT INTO user (user_id,user_type,first_name,last_name,phone,gender,address1,
								city,state,zip_code,email,password,emergency_first_name,emergency_last_name,emergency_phone,timeStamp,active) 
					  VALUES (:user_id,:user_type,:first_name,:last_name,:phone,:gender,:address1,
								:city,:state,:zip_code,:email,:password,:emergency_first_name,:emergency_last_name,:emergency_phone,:timeStamp,:active)"); 
						
						$date = new DateTime();

		
								       
        
		$user->bindValue(':user_id', uniqid() );
		$user->bindValue(':user_type', 3); // USER_TYPE = 3  DRIVER 
		$user->bindValue(':first_name', $_POST['first_name']);
		$user->bindValue(':last_name', $_POST['last_name']);
		$user->bindValue(':phone', $_POST['phone']);
		$user->bindValue(':gender', NULL);
		$user->bindValue(':address1', NULL);
		$user->bindValue(':city', $_POST['city']);
		$user->bindValue(':state', NULL);
		$user->bindValue(':zip_code', NULL);
		$user->bindValue(':email', $_POST['email']);
		$user->bindValue(':password', $_POST['password']);
		$user->bindValue(':emergency_first_name', NULL);
		$user->bindValue(':emergency_last_name', NULL);
		$user->bindValue(':emergency_phone', NULL);
		$user->bindValue(':timeStamp', $date->format('Y-m-d H:i:s') );
		$user->bindValue(':active', 1);


		$user->execute();







$stmt = $db->prepare("INSERT INTO driver_details (driver_details_id, user_id, company_id, driver_account_type, hometown_zip, vehicle_type) 
					  VALUES (:driver_details_id, :user_id, :company_id, :driver_account_type, :hometown_zip, :vehicle_type)"); 
						
			
		
								       
        $driverDetailsID = uniqid();
        
        
		$stmt->bindValue(':driver_details_id', $driverDetailsID );
		$stmt->bindValue(':user_id', $_SESSION["user_id"] );
		$stmt->bindValue(':company_id', $_POST['company_id']);
		$stmt->bindValue(':driver_account_type',  $_POST['driver_account_type']);
		$stmt->bindValue(':hometown_zip', $_POST['hometown_zip']);
		$stmt->bindValue(':vehicle_type', $_POST['vehicle_type']);

		

$stmt->execute();

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}

?>




<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Driver Sign Up Form Wiz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  
  
  
  
  
  
  
  
  
  
  
  <body class="fixed-header   ">
    <!-- START PAGE-CONTAINER -->
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                  </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      
      
      
      
      
      <!------------------------------------------------------------- START Login Right Container------------------------------------------------------------------>
      <div class="login-container bg-white">
      
      
      
      
      
      
      
      
      
      <!-- START PANEL -->
            <div class="panel panel-transparent">
             
             
              <div class="panel-heading">
                
              </div>
              
              
              <div class="panel-body">
              
              
                <div class="row">
                  <div class="col-sm-12">
                
                    <div class="panel panel-transparent">
                     
                     
                     
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-tabs-linetriangle">
                        <li class="active">
                          <a data-toggle="tab" href="#fade1"><span>Sign Up</span></a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#fade2"><span>Driver Details</span></a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#fade3" class="hide"><span>Hello Three</span></a>
                        </li>
                      </ul>
                      
                      
                      
                      <!-- Tab panes -->
                      <div class="tab-content">
                      
                      
                      
                        <div class="tab-pane fade in active" id="fade1">
                          <div class="row column-seperation">
                            <div class="col-md-12">
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                              
          <!-- START SIGNUP Form -->
          <form id="form-register" class="p-t-15" role="form" action="driver_signup_wizard.php" method="post">
          	<input type="hidden" name="driver_signup" id="driver_signup" value="1"/>
              
              
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>First Name</label>
                    <input type="text" name="first_name" placeholder="John" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Last Name</label>
                    <input type="text" name="last_name" placeholder="Smith" class="form-control" required>
                  </div>
                </div>
              </div>
             
             
             
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Gender</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Male" name="gender" id="male">
                            <label for="male">Male</label>
                            <input type="radio" checked="checked" value="Female" name="gender" id="female">
                            <label for="female">Female</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder="(999) 123-4567" required>
                  </div>
                </div>
              </div>
              
            
              
              
              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Email</label>
                    <input type="email" name="email" placeholder="We will send loging details to you" class="form-control" required>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Password</label>
                    <input type="password" name="pass" placeholder="Minimum of 4 Charactors" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
                                  
             
              
              <div class="row m-t-10">
           
                
              
                 
                 <br>
                <div class="col-md-12" style="text-align: center;">
                  <p class="hide">By clicking Next, I agree that MediCoupe or its representatives may contact me by email, phone, or SMS (including by automatic telephone dialing system) at the email address or number I provide, including for marketing purposes. I also confirm that I have read and understand MediCoupe's Driver Application Privacy Statement and Terms and Conditions.</p>
                </div>
              </div>
             
          
            
        <!--END SIGNUP Form-->

                            
                             <a data-toggle="tab" href="#fade2"><button class="btn btn-success" >Continue</button></a>
                            
                            
                            
                            
                            
                            
                           
                        
                              
                              
                            </div>
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        <div class="tab-pane fade" id="fade2">
                          <div class="row">
                            <div class="col-md-12">
                             <!-- START SIGNUP Form -->
         
              
              
              
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Driver Type</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Individual" name="driver_account_type" id="individual">
                            <label for="individual">Individual</label>
                            <input type="radio" checked="checked" value="Company" name="driver_account_type" id="company">
                            <label for="company">Company</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              
              
              
              <div class="row">
              <div class="col-sm-12">
               <div class="form-group form-group-default">        
               
                 <label>What Company Do you work for?</label>
                    <select class="full-width form-control-custom" name="company_id">
                    
                    
                    <option value="NULL" name="company_id">
                    
                    <span>Select Your Parent Company from the Drop Down</span>
                    
                    </option>

                    
                    
                             <?php while ($row = $getCompanies->fetch(PDO::FETCH_ASSOC)) : ?>
                             
                             
                             
                                                  
                                                   <option value="<? echo $row['company_id'] ;  ?>" name="company_id">
                                                   
                                                   		                                                   
                                                   		<span name="company_id"><? echo $row['company_name'] ;  ?> (<em><? echo $row['company_id'] ;  ?></em>)</span>
                                                   
                                                   </option>
                     
							<?php endwhile; ?>
                      
                      
                    </select>                          
            </div>
              </div>
            </div>
            
            
            
             <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Vehicle Type</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Black Car" name="vehicle_type" id="black_car">
                            <label for="individual">Black Car</label>
                            <input type="radio" checked="checked" value="Ambulette" name="vehicle_type" id="ambulette">
                            <label for="company">Ambulette</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              

            
            
            
            
          
              
              
              
              
              
              
                  <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default" id="locationField">
                    <label>Hometown</label>
                    <input id="autocomplete" type="text" name="hometown_zip" placeholder="Enter Your Hometown City or Zip Code" class="form-control"  onFocus="geolocate()" required>
                  </div>
                </div>
              </div>
              
    

    <table id="address" hidden="">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number" name="company_address1"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality" name="company_city"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" name="company_state" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code" name="hometown_zipcode"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>
              
                 
             
              
              <div class="row m-t-10">
                <div class="col-md-12">
                
         
                      
                      
              	
                 
                </div>
                
                 <button class="btn btn-success btn-block" type="submit">Update Driver Details</button>
                 
                 <br>
               
              </div>
             
            </form>
            
        <!--END SIGNUP Form-->



                            </div>
                          </div>
                        </div>
                        
                        
                        
                        <div class="tab-pane fade" id="fade3">
                          <div class="row">
                            <div class="col-md-12">
                              <h3>Follow us &amp; get updated!</h3>
                              <p>Instantly connect to what's most important to you. Follow your friends, experts, favorite celebrities, and breaking news.</p>
                              <br>
                            </div>
                          </div>
                        </div>
                        
                        
                      </div>
                    </div>
                  </div>
                  
                  
                  
                  
         
                </div>
              </div>
            </div>
            <!-- END PANEL --> 
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      </div>
      <!--------------------------------------------------------------- END Login Right Container------------------------------------------------------------------>
      
      
      
      
      
      
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
  </body>
</html>


