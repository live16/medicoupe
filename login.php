<?php

session_start();

//database connection info db createAccount
include 'connect.php';




if(!empty($_SESSION["user_type"]) && !empty($_SESSION["user_id"]))
{
	if($_SESSION['user_type'] == 1){
		header("Location:patient_timeline.php");
		exit;
	} 
	elseif ($_SESSION['user_type'] == 2) {
		header("Location:caregiver.php");
		exit;
	}
	elseif ($_SESSION['user_type'] == 3) {
		header("Location:driver_list.php");
		exit;
	}
	elseif($_SESSION['user_type'] == 4){
		header("Location:admin.php");
		exit;
	}
	else {
	 	echo "<script type='text/javascript'>alert('Wrong Username or Password!')</script>";
	}
}



if(isset($_POST['submitted']))
{


   $stmt = $db->prepare("SELECT * FROM user WHERE email = :username AND password =:password");
   
   $username = $_POST['username'];
   $password = $_POST['password'];
   $stmt->bindParam(':username',$username);
   $stmt->bindParam(':password',$password);
   $stmt->execute();
   
   
   
   
   if ($stmt->rowCount() == 0) {
	   echo "<script type='text/javascript'>alert('Wrong Username or Password!')</script>";
	}
  	else
  	{
	    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    
		    $_SESSION["user_id"] = $row['user_id'];
		    $_SESSION["user_type"] = $row['user_type'];
		    $_SESSION["first_name"] = $row['first_name'];
		    $_SESSION["last_name"] = $row['last_name'];
		    $_SESSION["email"] = $row['email'];
		    $_SESSION["password"] = $row['password'];
		    $_SESSION["phone"] = $row['phone'];
		    $_SESSION["mapAddress"] = $row['mapAddress'];

		
			if($row['user_type'] == 1){
		    	header("Location:patient_timeline.php");
		    	exit;
		    } 
		    elseif ($row['user_type'] == 2) {
		    	header("Location:caregiver.php");
		    	exit;
		    }
		    elseif ($row['user_type'] == 3) {
		    	header("Location:driver_list.php");
		    	exit;
		    }
		    elseif($row['user_type'] == 4){
		    	header("Location:admin.php");
		    	exit;
		    }
		    else {
		     	echo "<script type='text/javascript'>alert('Wrong Username or Password!')</script>";
		    }
		}		
	}	

	



}








?>



<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>MediCoupe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header   ">
    <!-- START PAGE-CONTAINER -->
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="assets/img/bg.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          
          <p class="small">
            © 2014-2015 MediCoupe.
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <img src="assets/img/medicoupe.png" alt="logo" height="40">
          <p class="p-t-35">Sign into your account</p>
          
          
          
          
          
          <!-- START Login Form -->
          
          <form id="form-login" class="p-t-15" action="login.php" method="post">
          
              <input type='hidden' name='submitted' id='submitted' value='1'/>
              
              
              
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>E-Mail</label>
              <div class="controls">
                <input type="text" name="username" placeholder="E-Mail" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            
            
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Password</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Password" required>
              </div>
            </div>
            
            
            
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-6 no-padding">
                <div class="checkbox ">
                  <input type="checkbox" value="1" id="checkbox1">
                  <label for="checkbox1">Keep Me Signed in</label>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <span class="text-info small">Help? <a href="tel:+18556926873">Contact Support</a></span>
              </div>
            </div>
            
            
            
            <!-- END Form Control-->
            <button class="btn btn-success btn-cons m-t-10" type="submit">Sign in</button>
            
          </form>
          
          
          <!--END Login Form-->
          
          
          
          
          
          
          <div class="pull-bottom sm-pull-bottom">
            <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
              
            
               <span>Don't have an account?  &nbsp;<a class="btn btn-master" href="register.php">Sign Up for MediCoupe</a></span>
                
                
                
                
                
                
                
            </div>
          </div>
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
  </body>
</html>
