<?php

function payAppointment($preapprovalKey, $senderEmail, $amount, $apptmentID)
{
	//include 'payPalCredentials.php';
	include 'connect.php';
	
	$headers = array();
	$headers[] = "X-PAYPAL-SECURITY-USERID:".PAYPAL_SECURITY_ID;
	$headers[] = "X-PAYPAL-SECURITY-PASSWORD:".PAYPAL_SECURITY_PASSWORD;
	$headers[] = "X-PAYPAL-SECURITY-SIGNATURE:".PAYPAL_SECURITY_SIGNATURE;
	$headers[] = "X-PAYPAL-REQUEST-DATA-FORMAT:NV";
	$headers[] = "X-PAYPAL-RESPONSE-DATA-FORMAT:JSON";
	$headers[] = "X-PAYPAL-APPLICATION-ID:".PAYPAL_APLICATION_ID;	
	
	
	$urlArray = array();
	$urlArray['actionType'] = 'PAY';
	$urlArray['currencyCode'] = 'USD';
	$urlArray['feesPayer'] = 'EACHRECEIVER';
	$urlArray['preapprovalKey'] = $preapprovalKey;
	$urlArray['receiverList.receiver(0).amount'] = $amount;
	$urlArray['receiverList.receiver(0).email'] = PAYPAL_RECEIVER_EMAIL;
	$urlArray['senderEmail'] = $senderEmail;
	$urlArray['returnUrl'] = PAYPAL_PAY_RETURN_URL;
	$urlArray['cancelUrl'] = PAYPAL_PAY_CANCEL_URL;
	$urlArray['requestEnvelope.errorLanguage'] = 'en_US';
	
	$urlRequest = PAYPAL_PAY_URL.'?'.http_build_query($urlArray);
	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlRequest);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$output = curl_exec($ch);
	curl_close($ch);
	$result = json_decode($output);


	$update = $db->prepare("UPDATE appointments SET appointment_status='Paid' WHERE appointment_id=:appointment_id");
	$update->bindParam(':appointment_id', $apptmentID);
	$update->execute();

	return $result['responseEnvelope']['ack'];
}


?>