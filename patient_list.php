<?php

session_start();


if ($_SESSION['user_type'] != 1 ){  //User_type = 1 = Patient
    header("Location:login.php");
    exit;
} 



//database connection info db createAccount
include 'connect.php';
include 'upload.php';



$show_modal = $PAYPAL->getUserRow($_SESSION["user_id"]) ? false : true;


if (isset($_GET['preApproval'])) {

  $show_modal = false;
  $preapprovalKey = $PAYPAL->getPreApprovalKey();
  if($preapprovalKey) $_SESSION['preapprovalKey'] = $preapprovalKey;

  $preUrl = PAYPAL_DOMAIN."/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=".$preapprovalKey;
  echo "<script> window.location.href = '$preUrl';</script>";
  exit;
  
}












//This is the Add Appointment Form PHP 
if(isset($_POST['submitted_event']))
{
   try {
        $stmt = $db->prepare("INSERT INTO appointments 
						        (appointment_id,
						        appointment_title,
						        scheduler_user_id,
						        transport_type,
						        destination_zip,
							    destination_mapAddress,
							    pickup_zip,
							    pickup_mapAddress,
							    appointment_date,
							    appointment_time,
							    scheduled_pickup_time,
							    destination_timeStamp,
							    pickup_timeStamp,
							    actual_trip_time,
							    trip_distance,
							    scheduled_trip_time,
							    appointment_request_timeStamp,
							    timeStamp,
							    active,
							    notes,
							    appointment_status,
							    cancelBy_user_id,
							    appointment_cancel_timeStamp,
							    device_type,
							    patient_user_id,
							    driver_user_id,
							    scheduled_destination_time,
							    trip_cost)
                                                                        
                             VALUES 
                             	(:appointment_id,
                             	:appointment_title,
                             	:scheduler_user_id,
                             	:transport_type,
                             	
                             	:destination_zip,
                             	:destination_mapAddress,
                             	:pickup_zip,
                             	:pickup_mapAddress,
                             	:appointment_date,
                             	:appointment_time,
                             	:scheduled_pickup_time,
                             	:destination_timeStamp,
                             	:pickup_timeStamp,
                             	:actual_trip_time,
                             	:trip_distance,
                             	:scheduled_trip_time,
                             	:appointment_request_timeStamp,
                             	:timeStamp,
                             	:active,
                             	:notes,
                             	:appointment_status,
                             	:cancelBy_user_id,
                             	:appointment_cancel_timeStamp,
                             	:device_type,
                             	:patient_user_id,
                             	:driver_user_id,
                             	:scheduled_destination_time,
                             	:trip_cost)");
                             	
                             	
        $id = uniqid();
        //$date->format('U = Y-m-d H:i:s');

        //$date->setTimestamp(1171502725);
        //$date->format('U = Y-m-d H:i:s');
        
        if(!empty($_POST['appointment_time'])){
			$_POST['appointment_time'] = date('H:i',strtotime($_POST['appointment_time']));
		}
        
        
        
        $stmt->bindValue(':appointment_id', uniqid() );
        $stmt->bindValue(':appointment_title', $_POST['appointment_title']);
        $stmt->bindValue(':scheduler_user_id', $_SESSION["user_id"] );
        $stmt->bindValue(':transport_type', $_POST['rideOption']);
    
        $stmt->bindValue(':destination_zip', @$_POST['destination_zip']);
        $stmt->bindValue(':destination_mapAddress', $_POST['destination_mapAddress']);
        
        $stmt->bindValue(':pickup_zip', @$_POST['pickup_zip']);
        $stmt->bindValue(':pickup_mapAddress', $_POST['pickup_mapAddress']);
        $stmt->bindValue(':appointment_date', $_POST['appointment_date']);
        $stmt->bindValue(':appointment_time', $_POST['appointment_time']);
        $stmt->bindValue(':scheduled_pickup_time', NULL);
        $stmt->bindValue(':destination_timeStamp', NULL);
        $stmt->bindValue(':pickup_timeStamp', NULL);
        $stmt->bindValue(':actual_trip_time', NULL);
        $stmt->bindValue(':trip_distance', NULL);
        $stmt->bindValue(':scheduled_trip_time', NULL);
        $stmt->bindValue(':appointment_request_timeStamp', NULL);
        
        $stmt->bindValue(':timeStamp', NULL);
        $stmt->bindValue(':active', 1);
        $stmt->bindValue(':notes', $_POST['notes']);
        $stmt->bindValue(':appointment_status', 'Scheduled');
        $stmt->bindValue(':cancelBy_user_id', NULL);
        $stmt->bindValue(':appointment_cancel_timeStamp', NULL);
        $stmt->bindValue(':device_type', 'WEB');
        $stmt->bindValue(':patient_user_id', $_SESSION["user_id"] );
        $stmt->bindValue(':driver_user_id', NULL);
        $stmt->bindValue(':scheduled_destination_time', NULL);
        $stmt->bindValue(':trip_cost', NULL);
        $stmt->execute();
        
        
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
        
        
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}

   
   //CANCEL APPOINTMENT --

 if(isset($_POST['cancelAppointment']))
{
   try {
        $cancelAppt = $db->prepare("UPDATE appointments
									SET appointment_status='Cancelled', cancelBy_user_id=:cancelBy_user_id
									WHERE appointment_id = :apptment_id ;");
        
        $cancelAppt->bindValue(':apptment_id', $_POST['apptment_id']);
       
        $cancelAppt->bindValue(':cancelBy_user_id', $_SESSION['user_id']);
         //  $cancelAppt->bindValue(':appointment_cancel_timeStamp', NULL);
            
            
            
        $cancelAppt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}

   
//This is the PHP to fetch appointments for the specific Patient via User_ID  
     $getPatientsAppointments = $db->prepare("SELECT * FROM appointments WHERE patient_user_id = :patient_user_id");
     $getPatientsAppointments->bindValue(':patient_user_id', $_SESSION["user_id"] );
     $getPatientsAppointments->execute();
  
   
 //This is the PHP for the SESSION Image Icon in the Header  
     $userImage1 = 'user_img/' . $_SESSION["user_id"] . '.jpg';
	 $defaultImage1 = 'assets/img/default-user.png';
	 $image1 = (file_exists($userImage1)) ? $userImage1 : $defaultImage1;




   
if(!empty($_POST['userProfile']))
{
	
	
	//print_r($_POST);exit;
	
  try {

	        $inputs = array('first_name','email','last_name','phone','password','mapAddres');
	        foreach($inputs as $k=>$input){
			   if(empty($_POST[$input]) || !strlen(trim($_POST[$input]))){
			    	unset($inputs[$k]);
				}
	  		}
	  		
	  		if(!empty($_POST['phone'])){
				$_POST['phone'] = preg_replace('|[^\d]+|','',$_POST['phone']);
			}

	        
	        $query = 'UPDATE user SET ';
	        foreach($inputs as $input){
			   $query .= $input.' = :'.$input;
			  }
	        $query .= ' WHERE user_id = :user_id;';
	        $updateProfile = $db->prepare($query);
	        
	        $updateProfile->bindValue(':user_id',  $_SESSION['user_id']);
	        
	        foreach($inputs as $input){
	   			$updateProfile->bindValue($input, $_POST[$input]);
	  		}      
	    
	        $updateProfile->execute();
	        // This is in the PHP file and sends a Javascript alert to the client
	        //$message = $_POST[$date];
	        //echo "<script type='text/javascript'>alert($id);</script>";
	}   catch (PDOException $e) {
	    echo 'Connection failed: ' . $e->getMessage();
	    echo 'fail';
	}

}
   

?>





<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>MediCoupe - <?php echo($_SESSION["first_name"]);?>'s Rides</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    
    
    
            <link class="" href="pages/css/themes/calendar.css" rel="stylesheet" type="text/css" />
    
    
    
    
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    
     <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="assets/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
    
    <link href="assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="assets/css/style.css" rel="stylesheet" type="text/css" />
    
    
    
     <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
    
    <style>
    
    .font-raleway { 
    	font-family: 'Raleway', sans-serif; 
    }
    
     .font-raleway-bold { 
    	font-family: 'Raleway', sans-serif; 
    	font-weight: 800;
    }
    
    </style>
    
    
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    
    
    
    
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    
    
    
    

    </script>
    
    
    
    
 
    
    
    
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    
    
    <script src="assets/lib/jquery.js"></script>
	<script src="assets/dist/jquery.validate.js"></script>
 
	
    
    
      <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete_pu, autocomplete_des;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  street_number_des: 'short_name',
  route_des: 'long_name',
  locality_des: 'long_name',
  administrative_area_level_1_des: 'short_name',
  country_des: 'long_name',
  postal_code_des: 'short_name'
};

function initialize() {

  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete_pu = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_pu')),
      { types: ['geocode'] });
  autocomplete_des = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_des')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete_pu, 'place_changed', function() {
    fillInAddress();
  });
    google.maps.event.addListener(autocomplete_des, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete_pu.getPlace();
  var place = autocomplete_des.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete_pu.setBounds(circle.getBounds());
      autocomplete_des.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

  function HandlePopupResult(result) {

      if (result == "Success") {
        $("#payPal").hide();
        $("#paypal-message").text('Success');
        $("#paypal-message").css({'background-color': '#99FF99'});
      }else{
        $("#payPal").Show();
        $("#paypal-message").text('Error');
        $("#paypal-message").css({'background-color': '#FF3333'});
      }

      
    }



    </script>
    
    
    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>


<style>
.datepicker{z-index:1151 !important;}

</style>


<style>
.bootstrap-timepicker-widget{z-index:1151 !important;}




.centered{
position:relative;
top:25%;
left:25%;
transform:translate(-25%,-25%)
}


</style>




    
    
    
    
    
    
    
    

    
    
    

    
  </head>



<!--------------------------------------------------------- BODY ------------------------------------------------------------>
  <body class="fixed-header"  onload="initialize()">

  
 
  
   
  
  
  

    
    
    
    
    
    
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        
        
        
        
        
         <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
              <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/logo_2x.png" height="40">
            </div>
           
                <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20 ">
            
            
              <li class="p-r-15 inline">
                   <a href="patient_timeline.php"><button href="patient_timeline.php" class="pg-indent" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Dynamic View"></button></a>
              </li>
              
             
              <li class="p-r-15 inline active-view">
                   <a href="patient_list.php"><button href="patient_list.php" class="pg-calender" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Appointments"></button></a>
              </li>
              
                <!--<li class="p-r-15 inline">
                  <a href="patient_list.php"><button href="patient_list.php" class="pg-menu_justify" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="List View"></button></a>
              </li>-->
            </ul>
            <!-- END NOTIFICATIONS LIST -->
            <a href="#" class="search-link hide" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs hide" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
          
          <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="bold"><?php echo($_SESSION["first_name"]);?></span> <span class="text-master"><?php echo($_SESSION["last_name"]);  ?></span>
            </div>
            
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php echo $image1; ?>" alt="" data-src="<?php echo $image1; ?>" data-src-retina="<?php echo $image1; ?>" width="32" height="32">
            </span>
              </button>
                <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="#userProfile" data-toggle="modal"><i class="fa fa-user"></i> Profile</a>
                </li>
                <li><a href="#signupPayPal" data-toggle="modal"><i class="fa fa-user"></i> Payment</a>
                </li>
                <li ><a href="tel:+18556926873"><i class="fa fa-phone"></i> Help</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="logout.php" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      
      
      
      
      
      
      
      
       <!-- MODAL STICK UP  -->
    <div class="modal fade stick-up" id="userProfile" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header clearfix text-left ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="p-t-30">
            <span class="font-raleway-bold text-medi fs-40">user <span class="text-medi-dark">profile</span></span>
            </div>
          
            <h5><span>General Information</span></h5>
          </div>
          <div class="modal-body">
         
          
          
          <script>
          
     var switchToInput = function () {
        var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
        });
        $input.addClass("loadNum");
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };
    var switchToSpan = function () {
        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
    }
    $(".loadNum").on("click", switchToInput);
          
          </script>
          
           <div>
          
         
          
          </div>
          
          
           		<?	
               		$user = $db->prepare("SELECT * FROM user WHERE user_id = :user_id");
					$user->bindValue(':user_id', $_SESSION["user_id"] );
					$user->execute();
					$currentUser = (object) $user->fetch();
				?>
              
          
          
           <form id="form-work" action="" class="form-horizontal" role="form" method="post" >
                     
                      
                      <input type="hidden" name="userProfile"  value="1"/>
                      
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="fname" placeholder="<?php echo($currentUser->first_name);?>" name="first_name">
                        </div>
                        
                       
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="lname" placeholder="<?php echo($currentUser->last_name);?>" name="last_name">
                        </div>
                      </div>


             
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="phone" placeholder="<?php echo($currentUser->phone);?>" name="phone">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" placeholder="<?php echo($currentUser->email);?>" name="email">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="password" placeholder="<?php echo($currentUser->password);?>" name="password">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="addressname" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="address" placeholder="<?php echo($currentUser->mapAddress);?>" name="mapAddress">
                        </div>
                      </div>
                       <div class="row m-t-10">
                        <div class="col-sm-3">
                          <p> </p>
                        </div>
                        <div class="col-sm-6">
                      
                        
                <button type="submit" class="btn btn-success btn-block m-t-5">Update Profile</button>
                          
                        </div>
                      </div>
                    </form>

                      
                      
                                             
                     
                     
         
                                         
                    
                    
                    
                    
                    
   







            
            
            <div class="row">
            
            
            <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Picture</label>
                        
                        <div class="col-sm-9">
                        	<div class="col-sm-4 m-t-20">
                        	
	                        	<div class="thumbnail-wrapper d48 circular m-t-5 m-r-25">
								<img src="<?php echo $image1; ?>" alt="" data-src="<?php echo $image1; ?>" data-src-retina="<?php echo $image1; ?>" width="60" height="60">
								</div>
                        	</div>

							<div class="col-sm-8 m-t-20">
						
							
								<div class="p-t-10">
									<form id="upload" action="patient_list.php" method="post" enctype="multipart/form-data">
									    Select image to upload  :<br>
										    
									    (Square images work best!)<br><br>
									    
									    <input type="file" name="fileToUpload" id="fileToUpload">
									    <div class="p-t-10">
									    <input class="btn btn-master" type="submit" value="Upload Image" name="submit">
									    </div>
									</form id="upload">
								</div>
							</div>
                        </div>
            </div>

            
            </div>
            <!-- END ROW  -->
            
            
            
            
            
            
            
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP  -->
    
    
    
    <?block('modals/signupPayPal',  array('show_modal'=>$show_modal))?>
    
    
    
    
    <!-- MODAL STICK UP  
    <div class="modal fade slide-up disable-scroll" id="payPal" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        
        <div class="p-t-10 p-r-10">
        
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
        </div>
          <div class="modal-header clearfix text-left p-t-10">
          
            
            
            	<div class="pull-right p-t-25">
	            	<img src="assets/img/paypal.png" height="40">
	            </div>
	            
	            <div class="p-t-10">
	            <h3>Payment <span class="semi-bold">Information</span></h3>
            <p>We need payment information inorder to process your order</p>
	            </div>

          </div>
          <div class="modal-body p-t-15">
            <form role="form">
             
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>PayPal Username</label>
                      <input type="email" class="form-control">
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>PayPal Password</label>
                      <input type="password" class="form-control">
                    </div>
                  </div>
                </div>
                          
            </form>
            <div class="row">
              <div class="col-sm-8">
                <div class="p-t-20 clearfix p-l-10 p-r-10">
                  <div class="pull-left">
                   
                  </div>
                  <div class="pull-right">
                   
                  </div>
                </div>
              </div>
              <div class="col-sm-4 m-t-10 sm-m-t-10">
                <button type="button" class="btn btn-complete btn-block m-t-5">Connect with PayPal</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content 
      </div>
      <!-- /.modal-dialog 
    </div>
    <!-- END MODAL STICK UP  -->    

      
      

      
      
      
      
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        
        

        
        
        
        <!-- START PAGE CONTENT -->
        <div class="content">
           <div id="paypal-message" style="width:450px;margin-left:350px;height:5px;vertical-align: middle;padding-left: 30px;line-height:60px;color:white;"></div>
        
          <!-- START JUMBOTRON -->
          <div class="jumbotron " data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
               
               
               
               
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Appointments</p>
                  </li>
                  
                </ul>
                <!-- END BREADCRUMB -->
                
                
                
            
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          
          
          
                  
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              
              
              <div class="panel-heading">
                <div class="panel-title">Appointments  
                </div>
                <div class="col-md-8 pull-right">
                
                  
                
              <div style="display:inline;">
              
              <div class="col-md-10">
              <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
              </div>
              
			 		<div class="pull-right">
                    <?block('add_appt_btn', array('PAYPAL'=>$PAYPAL))?>
					</div>
            </div>
           
                </div>
                <div class="clearfix"></div>
              </div>
              
              
              <div class="panel-body">
     
                
                
                
                
                
                
                
                
                
                
                
               <table class="table table-hover demo-table-search" id="tableWithSearch"> 
                
                
                  <thead>
<tr>
                    <!-- 1 -->	<th id="appointment_id" class="hide"></th>
					<!-- 2 -->	<th id="appointment_title">Reminder Title</th>
					<!-- 3 -->	<th id="scheduler_user_id" class="hide"></th>
					<!-- 4 -->	<th id="transport_type" class="hide">Ride Type</th>
										
					<!-- 7 -->	<th id="appointment_date">Date/Time</th>
					
					
					<!-- 5 -->	<th id="pickup_mapAddress">Pickup/Destination</th>

					
					<!-- 9 -->	<th id="scheduled_pickup_time" class="hide"></th>
					<!-- 10 -->	<th id="destination_timeStamp" class="hide"></th>
					<!-- 11 -->	<th id="pickup_timeStamp" class="hide"></th>
					<!-- 12 -->	<th id="actual_trip_time" class="hide"></th>
					<!-- 13 -->	<th id="trip_distance" class="hide"></th>
					<!-- 14 -->	<th id="scheduled_trip_time" class="hide"></th>
					<!-- 15 -->	<th id="appointment_request_timeStamp" class="hide"></th>
					<!-- 16 -->	<th id="timeStamp" class="hide"></th>
					<!-- 17 -->	<th id="active" class="hide"></th>
					
					<!-- 18 -->	<th id="appointment_status">Status</th>
					
					<!-- 19 -->	<th id="cancelBy_user_id" class="hide"></th>
					<!-- 20 -->	<th id="appointment_cancel_timeStamp" class="hide"></th>
					<!-- 21 -->	<th id="device_type" class="hide"></th>
					
					<!-- 22 -->	<th id="patient_user_id" class="hide"></th>
					<!-- 23 -->	<th id="driver_user_id">Your Driver</th>
					
					<!-- 24 -->	<th id="scheduled_destination_time" class="hide"></th>
					<!-- 25 -->	<th id="trip_cost" class="hide"></th>   
					<!-- 26 -->	<th></th>
                    </tr>
                  </thead>
                  
                  
                  
                  
                  <tbody>
                  
                  <?php while ($row = $getPatientsAppointments->fetch(PDO::FETCH_ASSOC)) : 
	                  
	                  
	                  
	                  
	                          
	                // this is driver name
      $getDriverName = $db->prepare("SELECT user.user_id, user.first_name, user.last_name

								FROM user
								INNER JOIN appointments on 
								
								user.user_id = appointments.driver_user_id 
								
								
								WHERE appointments.appointment_id = :appointment_id");
	$getDriverName->bindValue(':appointment_id', $row['appointment_id']);

   
   $getDriverName->execute();
   $GDN = $getDriverName->fetch(PDO::FETCH_ASSOC);
   
   
   
   //This is the PHP for the SESSION Image Icon for the Driver
     $userImage_Driver = 'user_img/' . $row['driver_user_id'] . '.jpg';
	 $defaultImage_Driver = 'assets/img/default-user.png';
	 $image_Driver = (file_exists($userImage_Driver)) ? $userImage_Driver : $defaultImage_Driver;	                  
	                  
	                  
	                  
                  ?>
                  
                  
                    <tr>
  <!-- 1 -->                       <td name="appointment_id" class="v-align-middle hide">
                        <? echo $row['appointment_id'] ;  ?>
                      </td>


  <!-- 2 -->  				  <td name="appointment_title" class="v-align-middle">
  
     <div class="panel-group " id="accordion_id" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-transparent">
                      
                      <div class="panel-heading no-padding" role="tab" id="headingOne_id">
                        <h4 class="panel-title-timeline">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion_id" href="#collapse_id<? echo $row['appointment_id'] ;  ?>" aria-expanded="true" aria-controls="collapseOne">
                             <div class="col-sm-11 no-padding p-r-10">
                             	<h5 class='font-raleway no-margin'><? echo $row['appointment_title'] ;  ?></h5><br>
                             </div>
                         	
                            </a>
                          </h4>
                      </div>
                      
                      
                      
                      
                      
                      <div id="collapse_id<? echo $row['appointment_id'] ;  ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body panel-transparent no-padding">
                          
                        
                        
                       <div class="col-sm-6 no-padding p-r-10">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        <span class="fs-30 centered"><i class="fa fa-road"></i></span>
                        <h5 class="font-raleway no-padding no-margin"><? echo round($row['trip_distance'], 2) ;  ?> miles</h5>                     
                                             
                      </div>
                      
                         <div class="col-sm-6 no-padding p-r-10">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        <span class="fs-30 centered"><i class="fa fa-money"></i></span>
                          
                        <h5 class="font-raleway no-padding no-margin">$<? echo $row['trip_cost'] ;  ?></h5>                    
                                             
                      </div>
                      
                      
                      <div class="col-sm-12 no-padding p-r-10 p-t-15">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        <h5 class="small hint-text no-margin">NOTES</h5>
                        <? echo $row['notes'] ;  ?>                    
                                             
                      </div>

                      
                      
                      
                        
                        </div>
                      </div>
                      
                      
                    </div>
                    </div>
  
  
                        
                      </td>
                      
                      
  <!-- 3 -->                     <td class="hide"> </td>                    
                      

                     
                     
   <!-- 4 -->                      <td name="transport_type" class="v-align-middle hide">
                                  <div                                    
                                   	<?php if($row['transport_type'] == 'Ambulette')
                                                            {
                                                                echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/ambulance.jpg' alt='' width='32' height='32'>"; 

                                                                
																
                                                                
                                                            } 
                                                       
                                                                elseif ($row['transport_type'] == 'Black Car') 
                                                            {
                                                            	echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/car.jpg' alt='' width='32' height='32'>";
                                                               
																
                                                                
                                                            }
                                                            
														
														
                                                    ?>
                         </div> 
                      
                      </td>
                      
                      
                      
                      
                      
                      
                      

                 

  
  
  
  
  
  
  
                        
                      
                      
                      
                      
                      
  <!-- 7 -->                      <td name="appointment_date" class="v-align-middle">
                         <? echo date("m/d/y", strtotime($row['appointment_date']))  ;  ?>  <br>                       
                         <? echo date("g:i A", strtotime($row['appointment_time'])) ;  ?>

                      </td>     
                      
                      
                      
                      
                      
                        <!-- 5 -->          
   <td class="v-align-middle">
   
   
    <div class="col-md-12">
    
    
    <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-transparent">
                      
                      <div class="panel-heading no-padding" role="tab" id="headingOne">
                        <h4 class="panel-title-timeline">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<? echo $row['appointment_id'] ;  ?>" aria-expanded="true" aria-controls="collapseOne">
                             <div class="col-sm-11 no-padding p-r-10">
                             	<h5 class="small hint-text no-margin">PICK UP</h5><br>
                             	<span class="font-arial fs-14"><? echo $row['pickup_mapAddress'] ;  ?></span>
                             </div>
                         	
                            </a>
                          </h4>
                      </div>
                      
                      
                      
                      
                      
                      <div id="collapse<? echo $row['appointment_id'] ;  ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body panel-transparent no-padding p-t-10">
                          
                        
                        
                       <div class="col-sm-6 no-padding p-r-10">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        
                        <img class="p-b-15" src="https://maps.googleapis.com/maps/api/staticmap?center=<? echo $row['pickup_mapAddress'] ;  ?>&zoom=15&size=300x200&scale=1.5&maptype=roadmap
&markers=color:green%7Clabel:%7C<? echo $row['pickup_mapAddress'] ;  ?>">
                        
                                             
                      </div>
                      
                        
                        </div>
                      </div>
                      
                      
                    </div>
                    </div>
                
                  
                  
                  <div class="panel-group " id="accordion3" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-transparent">
                      
                      <div class="panel-heading no-padding" role="tab" id="headingOne3">
                        <h4 class="panel-title-timeline">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse<? echo $row['appointment_id'] ;  ?>3" aria-expanded="true" aria-controls="collapseOne">
                             <div class="col-sm-11 no-padding p-r-10">
                             	<h5 class="small hint-text no-margin">DESTINATION</h5><br>
                             	<span class="font-arial fs-14"><? echo $row['destination_mapAddress'] ;  ?></span>
                             </div>
                         	
                            </a>
                          </h4>
                      </div>
                      
                      
                      
                      
                      
                      <div id="collapse<? echo $row['appointment_id'] ;  ?>3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne3">
                        <div class="panel-body panel-transparent no-padding p-t-10">
                          
                        
                        
                       <div class="col-sm-6 no-padding p-r-10">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        
                        <img class="p-b-15" src="https://maps.googleapis.com/maps/api/staticmap?center=<? echo $row['destination_mapAddress'] ;  ?>&zoom=15&size=300x200&scale=1.5&maptype=roadmap
&markers=color:green%7Clabel:%7C<? echo $row['destination_mapAddress'] ;  ?>">
                        
                                             
                      </div>
                      
                        
                        </div>
                      </div>
                      
                      
                    </div>
                    </div>
               
              </div>


   
   
 
   					 <div class="col-sm-12 hide">
                        <!--<h5 class="small hint-text no-margin">PICK UP</h5>-->
                        
                        
                        <img class="p-b-15" src="https://maps.googleapis.com/maps/api/staticmap?center=<? echo $row['pickup_mapAddress'] ;  ?>&zoom=15&size=250x150&scale=1&maptype=roadmap
&markers=color:green%7Clabel:%7C<? echo $row['pickup_mapAddress'] ;  ?>"><br>

<span class=""><? echo $row['pickup_mapAddress'] ;  ?></span>
                        
                      
                     	 <!--<h5 class="small hint-text no-margin">DESTINATION</h5>-->
                     	 
                     	 
                     	 <img class="p-b-15"  src="https://maps.googleapis.com/maps/api/staticmap?center=<? echo $row['destination_mapAddress'] ;  ?>&zoom=15&size=250x150&scale=1&maptype=roadmap
&markers=color:red%7Clabel:%7C<? echo $row['destination_mapAddress'] ;  ?>"><br>

<span class=""><? echo $row['destination_mapAddress'] ;  ?></span>

					 	 					 	 
                      </div>
 

                      </td>    



<!-- 9 -->                     <td class="hide"> </td>
<!-- 10 -->                     <td class="hide"> </td>
<!-- 11 -->                     <td class="hide"> </td>
<!-- 12 -->                     <td class="hide"> </td>
<!-- 13 -->                     <td class="hide"> </td>
<!-- 14 -->                     <td class="hide"> </td>
<!-- 15 -->                     <td class="hide"> </td>
<!-- 16 -->                     <td class="hide"> </td>
<!-- 17 -->                     <td class="hide"> </td>

                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
  <!-- 18 -->                       <td name="appointment_status" class="v-align-middle">
                       		<span                                     
                                   				<?php if($row['appointment_status'] == 'Scheduled')
                                                            {
                                                                echo "class='label label-warning'>"; 

                                                                echo 'Scheduled';
																
                                                                
                                                            } 
                                                            
                                                            
                                                            
                                                            
                                                            	elseif($row['appointment_status'] == 'Assigned')
                                                            {
                                                                echo "class='label label-info'>"; 

                                                                echo 'Assigned';
																
                                                                
                                                            } 
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            	elseif($row['appointment_status'] == 'Confirmed')
                                                            {
                                                                echo "class='label label-success'>"; 

                                                                echo 'Confirmed';
																
                                                                
                                                            } 



																elseif($row['appointment_status'] == 'In Progress')
                                                            {
                                                                echo "class='label label-inverse'>"; 

                                                                echo 'In Progress';
																
                                                                
                                                            } 
                                                            
                                                            
  
                                                               
                                                                elseif ($row['appointment_status'] == 'Completed') 
                                                            {
                                                            	echo "class='label'>";
                                                                echo 'Completed';
															
                                                                
                                                            }
                                                            
														
															
													
                                                            
                                                                elseif ($row['appointment_status'] == 'Cancelled') 
                                                            {
                                                            	echo "class='label label-important'>";
                                                                echo 'Cancelled';
															
                                                                
                                                            }
                                                            
                                                            

                                                            
                                                            
                                                                else 
                                                            {
                                                            echo "class='label label-default'>";
                                                                echo 'N/A';
                                                            }
                                                    ?>
                         </span>                
                      </td>
                      
                      
                      
<!-- 19 -->                     <td class="hide"> </td> 
<!-- 20 -->                     <td class="hide"> </td>
<!-- 21 -->                     <td class="hide"> </td>                     
                      
                      
 <!-- 22 -->           <td name="patient_user_id" class="v-align-middle hide">
                        <p>
                        	<div class="thumbnail-wrapper d32 circular inline ">
								<img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" 
								data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
							</div>  <? echo $row['patient_user_id'] ;  ?> 						</p>
                      </td>
                      
                      
                      
                      
                      
                      
 <!-- 23 -->                      <td name="driver_user_id" class="v-align-middle">
                        <span class="thumbnail-wrapper d32 circular m-r-10">
                <img src="<?php echo $image_Driver; ?>"  alt="" data-src="<?php echo $image_Driver; ?>" data-src-retina="<?php echo $image_Driver; ?>" width="32" height="32">
            </span>
						
                     	<h5 class='font-raleway no-margin'><span>
                     	
                     	<? echo $GDN['first_name'] ;  ?>  <? echo $GDN['last_name'] ;  ?></span> </h5>
                      </td>
                      
                      
<!-- 24 -->                     <td class="hide"> </td>
<!-- 25 -->                     <td class="hide"> </td>
                      
                      
                      
                      
                      
                      
<!-- 26 - CANCEL BUTTON -->                       <td class="v-align-middle">
                

                      
                      
                          <div class="pull-right p-b-15">
		                      <button class="btn btn-white" href="#cancelApptModal<? echo $row['appointment_id'] ;  ?>" data-toggle="modal"><i class="fa fa-trash-o"></i>
							  </button>
	                      </div>
                           
                                                      
                           
                           
                           <!-------------------------------------------------- CANCEL APPOINTMENT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="cancelApptModal<? echo $row['appointment_id'] ;  ?>" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
            <button type="button" class="p-r-15 p-t-15 close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>

          
          
            <div class="modal-header clearfix text-left">
            
              <h4>Are you sure you want to cancel this appointment?</h4>
              <p>Warning: This can't be undone! If you cancel an appointment you will need to reschedule another appointment.</p>
              
            </div>
            
            
            <div class="modal-body">
  
  
	
               <form id="form-event" action="patient_list.php" role="form" method="post">
      <input type='hidden' name='cancelAppointment' id='cancelAppointment' value='1'/>
     
     
     
      
     
     
     <input type="hidden" class="form-control" name="apptment_id" value="<? echo $row['appointment_id'] ;  ?>" placeholder="<? echo $row['appointment_id'] ;  ?>">
    
      
                          
                          <button class="btn btn-danger m-t-25 p-t-5 p-b-5 btn-block"><i class="fa fa-trash"></i> &nbsp; Cancel Appointment</button>
                          
      </form>
                        
            
                          
            
              
              
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- CANCEL APPOINTMENT MODAL ------------------------------------------------------------->
                      

                           

               
                      
                      
                      </td>
                      
                      
                      
                      
                    </tr>



				<?php endwhile; ?>


                    
                  </tbody>
                </table>
                
                
                           </div>
            </div>
            <!-- END PANEL -->
            
          </div>
          <!-- END CONTAINER FLUID -->
        
           </div>
        <!-- END PAGE CONTENT -->
        
        
        
        
        
        
        
        
        
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2014 </span>
              <span class="font-montserrat">MediCoupe</span>.
              <span class="hint-text">All rights reserved. </span>
            </p>
            
            <p class="small no-margin pull-right sm-pull-reset">
              <span class="sm-block"><a href="terms.html" class="m-l-10 m-r-10">Terms & Conditions</a></span>
            </p>
            
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
        
        
        
        
        
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<!-- QUICKVIEW -->
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        
        <li class="hide">
          <a href="#quickview-alerts" data-toggle="tab"><i class="fa fa-warning"></i></a>
        </li>
        <li class="active">
          <a href="#quickview-chat" data-toggle="tab"><i class="fa fa-comment"></i></a>
        </li>
        <li class="hide">
          <a href="#quickview-notes" data-toggle="tab"><i class="fa fa-edit"></i></a>
        </li>
      </ul>
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- BEGIN Notes !-->
        <div class="tab-pane fade  in no-padding" id="quickview-notes">
          <div class="view-port clearfix quickview-notes" id="note-views">
            <!-- BEGIN Note List !-->
            <div class="view list" id="quick-note-list">
              <div class="toolbar clearfix">
                <ul class="pull-right ">
                  <li>
                    <a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
                  </li>
                  <li>
                    <a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
                  </li>
                </ul>
                <button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
              </div>
              <ul>
                <!-- BEGIN Note Item !-->
                <li data-noteid="1" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox1" type="checkbox" value="1">
                      <label for="qncheckbox1"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox2" type="checkbox" value="1">
                      <label for="qncheckbox2"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox3" type="checkbox" value="1">
                      <label for="qncheckbox3"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="3" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox4" type="checkbox" value="1">
                      <label for="qncheckbox4"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="4" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox5" type="checkbox" value="1">
                      <label for="qncheckbox5"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
              </ul>
            </div>
            <!-- END Note List !-->
            <div class="view note" id="quick-note">
              <div>
                <ul class="toolbar">
                  <li><a href="#" class="close-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="pg-arrow_left"></i></a>
                  </li>
                  <li><a href="#" class="Bold"><i class="fa fa-bold"></i></a>
                  </li>
                  <li><a href="#" class="Italic"><i class="fa fa-italic"></i></a>
                  </li>
                  <li><a href="#" class=""><i class="fa fa-link"></i></a>
                  </li>
                </ul>
                <div class="body">
                  <div>
                    <div class="top">
                      <span>21st april 2014 2:13am</span>
                    </div>
                    <div class="content">
                      <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END Notes !-->
        <!-- BEGIN Alerts !-->
        <div class="tab-pane fade no-padding" id="quickview-alerts">
          <div class="view-port clearfix" id="alerts">
            <!-- BEGIN Alerts View !-->
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default navbar-sm">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Notications
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-search"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              
              
              
              
              <!-- BEGIN Alert List !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <!-- BEGIN List Group !-->
                
                
                <div class="list-view-group-container">
                
                
                <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Appointments
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                  
                  
                  
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">New Ride</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today <br></span>
                          <span class="text-master">5:00 pm</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                      <!-- BEGIN List Group Item!-->
                    </li>
                    
                    
                    
                    <!-- END List Group Item!-->
                    
                    
                    
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">Ride <span class="label label-success">Confirmed</span></span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">12/15/14</span>
                          <span class="text-master">5:00 pm</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
                <!-- END List Group !-->
                
                
                
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                   Group
                  </div>
                  <!-- END List Group Header!-->
                  
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">“Sub Title"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                    
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">“Sub-Title"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Group
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">Sub-Title</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
              </div>
              <!-- END Alert List !-->
            </div>
            <!-- EEND Alerts View !-->
          </div>
        </div>
        <!-- END Alerts !-->
        <div class="tab-pane fade in active no-padding" id="quickview-chat">
          <div class="view-port clearfix" id="chat">
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-plus"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Customer Support
                    <div class="fs-11">Show All</div>
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    a</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">Ava Flores</span>
                            <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">b</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">Bella McCoy</span>
                             <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">I have a bunch of ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">bob stephens</span>
                             <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Can someone please ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">c</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">carole roberts</span>
                             <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                           <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">christopher perez</span>
                             <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">d</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">danielle fletcher</span>
                             <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">david sutton</span>
                             <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">e</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">earl hamilton</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">elaine lawrence</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">ellen grant</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">erik taylor</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">everett wagner</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">f</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">freddie gomez</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">g</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">glen jensen</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">gwendolyn walker</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">j</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">janet romero</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">k</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">kim martinez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">l</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lawrence white</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">leroy bell</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">letitia carr</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lucy castro</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">m</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mae hayes</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marilyn owens</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marlene cole</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha warren</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha dean</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mia diaz</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">n</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">noah elliott</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">p</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">phyllis hamilton</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">r</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">raul rodriquez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">rhonda barnett</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">roberta king</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">s</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">scott armstrong</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sebastian austin</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sofia davis</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">t</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">terrance young</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">theodore woods</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">todd wood</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">tommy jenkins</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">w</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">wilma hicks</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
              </div>
            </div>
            
            
            
            
            
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    John Smith
                    <div class="fs-11 hint-text">Online</div>
                  </div>
                  <a href="#" class="link text-master inline action p-r-10 pull-right hide">
                    <i class="pg-more"></i>
                  </a>
                </div>
              </div>
              <!-- END Header  !-->
              
              
              <!-- BEGIN Conversation  !-->
              <div class="chat-inner" id="my-conversation">
              
              
              
              
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix hide">
                  <div class="chat-bubble from-me hide">
                    Hello there
                  </div>
                </div>
                <!-- END From Me Message  !-->
                
                
                
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    I have a bunch of questions about how to sign up patients in my care at the hospital. Can someone please follow up with me??
                  </div>
                </div>
                <!-- END From Them Message  !-->
                
                
                
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Did you check out the FAQ section ?
                  </div>
                </div>
                <!-- END From Me Message  !-->
                
                  <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    No, where can I access that??

                  </div>
                </div>
                <!-- END From Them Message  !-->
                
                
                
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                   
                Here is the link!
                    </div>
                </div>
                <!-- END From Me Message  !-->
                
             
                
                
             
                
                
              </div>
              <!-- BEGIN Conversation  !-->
              <!-- BEGIN Chat Input  !-->
              <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="col-xs-8 no-padding">
                    <input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
                  </div>
                  <div class="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top hide">
                    <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    
    
<!-- END QUICKVIEW -->
   
   
   
   
   
  
    
    
    
    
    
    
    

    
    
    
    
    
    
      <!-- MODAL STICK UP  -->
        <div class="modal fade slide-right" id="addNewAppModal" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
            
            

             
          

  
              
             

               
  <form id="thisForm" action="patient_list.php" role="form" method="post" onkeypress="return event.keyCode != 13;">
      <input type='hidden' name='submitted_event' id='submitted_event' value='1'/>
 
 
 <!--   <div class="quickview-wrapper calendar-event" id="calendar-event">
      <div class="view-port clearfix" id="eventFormController"> -->
 
 
        <div class="view bg-white">
          <div class="scrollable">
          
     
          
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
          
     
          
            <div class="p-l-30 p-r-30 p-t-20">
            
         
                         
               <h4>Schedule a Ride</h4>
              
            </div>
            
            
            
            
            
            
            
                      
            
            
            
            <div class="p-t-15">
              <!--<input id="eventIndex" name="eventIndex" type="hidden">-->
              
              
              
              
              
              
              
              
                          
              
              
                               
                        
 <div class="form-group-attached">
                 
                        

                           
              
                <div class="form-group form-group-default required">
                  <label>Title</label>
                  <input type="text" id="appt_title" class="form-control "  name="appointment_title" placeholder="event name" >
                </div>



                
                
                 <div class="form-group form-group-default ">
                  <label>Ride Type</label>

                <div class="radio radio-success">
                      <input type="radio" checked="checked" value="Black Car" name="rideOption" id="yes">
                      <label for="yes">Car</label>
                      <input type="radio"  value="Ambulette" name="rideOption" id="no">
                      <label for="no">Ambulette</label>
                    </div>
                  </div>
                
               
                  <div class="form-group form-group-default input-group">
                      <label>Appointment Date</label>
                      <input type="date" class="form-control" name="appointment_date" placeholder="MM/DD/YYYY" id="datepicker-component2">
                      <span class="input-group-addon">
                                                  <i class="fa fa-calendar"></i>
                                                </span>
                    </div>


              
                
                
                <div class="form-group form-group-default input-group bootstrap-timepicker">
                <label>Arrival Time</label>
                          <input id="timepicker" type="text" name="appointment_time" placeholder="When do you need a ride?" class="form-control">
                          <span class="input-group-addon"><i class="pg-clock"></i></span>
                        </div>
                
                

                
            
    
    
    
    
                <div class="form-group form-group-default">
                  <label>Pick Up Address</label>
                  
                  <input id="autocomplete_pu" class="form-control" name="pickup_mapAddress" placeholder="Pick Up"
             onFocus="geolocate()" type="text"></input>
                <table id="address" hidden>
                 
                  <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field"
                          name="pickup_state" id="administrative_area_level_1" disabled="true"></input></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field" name="pickup_zip" id="postal_code"
                          disabled="true"></input></td>
                  </tr>
                 
                </table>
             	
                </div>
                
                
                   <div class="form-group form-group-default input-group bootstrap-timepicker hide">
                <label> Appointment Time</label>
                          <input id="timepicker_appt" type="text" name="scheduled_pickup_time" placeholder="When do you need to arrive?" class="form-control">
                          <span class="input-group-addon"><i class="pg-clock"></i></span>
                        </div>
                

                
               
               
               
               
                <div class="form-group form-group-default">
                  <label>End Address</label>
                  
                  <input id="autocomplete_des" name="destination_mapAddress" class="form-control" placeholder="Destination"
             onFocus="geolocate()" type="text"></input>
                  
                  <table id="address" hidden>
                                            <tr>
                        <td class="label">State</td>
                        <td class="slimField"><input class="field"
                              name="destination_state" id="administrative_area_level_1_des" disabled="true"></input></td>
                        <td class="label">Zip code</td>
                        <td class="wideField"><input class="field" name="destination_zip" id="postal_code_des"
                              disabled="true"></input></td>
                      </tr>
                     
                    </table>
             	
                </div>
                
               
                
                
                <div class="row clearfix">
                  <div class="form-group form-group-default">
                    <label>Note</label>
                    <textarea class="form-control" placeholder="description" id="txtEventDesc" name="notes"></textarea>
                  </div>
                </div>
                
                
                
                
                 <div class="col-sm-12">
                <!-- START PANEL -->
                <div class="panel panel-transparent p-t-10">
             
            
                   
                    <div class="alert alert-info bordered" role="alert">
                      <p class="pull-left"><strong>Would you like a ride home? Do you have multiple stops?</strong> These are separate rides. Please don't forget to schedule additional rides if you need them!</p>
                      <button class="m-t-5 close hide" data-dismiss="alert"></button>
                      <div class="clearfix"></div>
                    </div>
                 
                </div>
                <!-- END PANEL -->
              </div>
                
                
                
              
            </div>
            
            
            
            <div class="p-l-15 p-r-15 p-t-30">
              <button id="eventSave" class="btn btn-success btn-block btn-cons">Save Event</button>
              <button id="eventDelete" class="btn btn-white hide"><i class="fa fa-trash-o"></i>
              </button>

            </div>
            
            
          </div>
        </div>
        
        
                
        </div>
        
        
        
        
    
      </form>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- END MODAL STICK UP  -->

    
    
    
    
    
    
    
    
    
    
    
    
    
           <!-- BEGIN VENDOR JS -->
           
           
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>


    <script src="assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>

    <script type="text/javascript" src="assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    
    
        
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/form_elements.js" type="text/javascript"></script>
    <script src="assets/js/datatables.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  </body>
</html>