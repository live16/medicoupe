<?php
session_start();
//database connection info db createAccount
//$_SESSION["user_id"] = $row['user_id'];
//$_SESSION["first_name"] = $row['first_name'];
//$_SESSION["last_name"] = $row['last_name'];


if ($_SESSION['user_type'] != 4 ){  //User_type = 4 --- Admin 
                    //echo htmlentities($row['user_id']);

                    header("Location:login.php");
                    exit;
                    } 


//database connection info db createAccount
include 'connect.php';




   
    if(isset($_POST['assignDriver']))
{
   try {
        $update = $db->prepare("UPDATE appointments 
        						SET driver_user_id = :driver_id, appointment_status='Confirmed' 
								WHERE appointment_id = :appt_id");
        
         $update->bindValue(':appt_id', $_POST['appt_id']);
         $update->bindValue(':driver_id', $_POST['driver_id']);
            
            
            
         $update->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}





//CANCEL APPOINTMENT --

 if(isset($_POST['cancelAppointment']))
{
   try {
        $cancelAppt = $db->prepare("UPDATE appointments
									SET appointment_status='Cancelled', cancelBy_user_id=:cancelBy_user_id
									WHERE appointment_id = :apptment_id ;");
        
         $cancelAppt->bindValue(':apptment_id', $_POST['apptment_id']);
       
          $cancelAppt->bindValue(':cancelBy_user_id', $_SESSION['user_id']);
         //  $cancelAppt->bindValue(':appointment_cancel_timeStamp', NULL);
            
            
            
         $cancelAppt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}


   
   




if(isset($_POST['submitted_event']))
{
   try {
        $stmt = $db->prepare("INSERT INTO appointments 
						        (appointment_id,
						        appointment_title,
						        scheduler_user_id,
						        transport_type,
						        destination_address,
						        destination_city,
						        destination_state,
						        destination_zip,
						        destination_mapAddress,
						        pickup_address,
						        pickup_city,
						        pickup_state,
						        pickup_zip,
						        pickup_mapAddress,
						        appointment_date,
						        appointment_time,
						        scheduled_pickup_time,
						        destination_timeStamp,
						        pickup_timeStamp,
						        actual_trip_time,
						        trip_distance,
						        scheduled_trip_time,
						        appointment_request_timeStamp,
						        timeStamp,
						        active,
						        appointment_status,
						        cancelBy_user_id,
						        appointment_cancel_timeStamp,
						        device_type,
						        patient_user_id,
						        driver_user_id,
						        scheduled_destination_time,
						        trip_cost)
                                                                        
                             VALUES 
                             	(:appointment_id,
                             	:appointment_title,
                             	:scheduler_user_id,
                             	:transport_type,
                             	:destination_address,
                             	:destination_city,                                       
                                :destination_state,
                                :destination_zip,
                                :destination_mapAddress,
                                :pickup_address,
                                :pickup_city,
                                :pickup_state,
                                :pickup_zip,
                                :pickup_mapAddress,                                 
								:appointment_date,
                                :appointment_time,
                                :scheduled_pickup_time,
                                :destination_timeStamp,
                                :pickup_timeStamp,
                                :actual_trip_time,
                                :trip_distance,
                                :scheduled_trip_time,
                                :appointment_request_timeStamp,
                                :timeStamp,
                                :active,
                                :appointment_status,
                                :cancelBy_user_id,
                                :appointment_cancel_timeStamp,
                                :device_type,
                                :patient_user_id,
                                :driver_user_id,
                                :scheduled_destination_time,
                                :trip_cost)");
                                
                                
        $id = uniqid();
        //$date->format('U = Y-m-d H:i:s');

        //$date->setTimestamp(1171502725);
        //$date->format('U = Y-m-d H:i:s');
        
        $stmt->bindValue(':appointment_id', uniqid() );
        $stmt->bindValue(':appointment_title', $_POST['appointment_title']);
        $stmt->bindValue(':scheduler_user_id', $_SESSION["user_id"] );
        $stmt->bindValue(':transport_type', $_POST['rideOption']);
        $stmt->bindValue(':destination_address', $_POST['destination_address']);
        $stmt->bindValue(':destination_city', $_POST['destination_city']);
        $stmt->bindValue(':destination_state', $_POST['destination_state']);
        $stmt->bindValue(':destination_zip', $_POST['destination_zip']);
        $stmt->bindValue(':destination_mapAddress', $_POST['destination_mapAddress']);
        $stmt->bindValue(':pickup_address', $_POST['pickup_address']);
        $stmt->bindValue(':pickup_city', $_POST['pickup_city']);
        $stmt->bindValue(':pickup_state', $_POST['pickup_state']);
        $stmt->bindValue(':pickup_zip', $_POST['pickup_zip']);
        $stmt->bindValue(':pickup_mapAddress', $_POST['pickup_mapAddress']);
        $stmt->bindValue(':appointment_date', $_POST['appointment_date']);
        $stmt->bindValue(':appointment_time', $_POST['appointment_time']);
        $stmt->bindValue(':scheduled_pickup_time', NULL);
        $stmt->bindValue(':destination_timeStamp', NULL);
        $stmt->bindValue(':pickup_timeStamp', NULL);
        $stmt->bindValue(':actual_trip_time', NULL);
        $stmt->bindValue(':trip_distance', NULL);
        $stmt->bindValue(':scheduled_trip_time', NULL);
        $stmt->bindValue(':appointment_request_timeStamp', NULL);
        
        $stmt->bindValue(':timeStamp', NULL);
        $stmt->bindValue(':active', 1);
        $stmt->bindValue(':appointment_status', 'Scheduled');
        $stmt->bindValue(':cancelBy_user_id', NULL);
        $stmt->bindValue(':appointment_cancel_timeStamp', NULL);
        $stmt->bindValue(':device_type', 'WEB');
        $stmt->bindValue(':patient_user_id', $_POST['patient_user_id']);
        $stmt->bindValue(':driver_user_id', NULL);
        $stmt->bindValue(':scheduled_destination_time', NULL);
        $stmt->bindValue(':trip_cost', NULL);
        $stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
        //echo "<script type='text/javascript'>alert($id);</script>";
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}

   
   
     
   
   
   //echo 'POST' . "<br>";
   //echo $_POST['username'];
   $getAppointments = $db->prepare("SELECT * FROM appointments");
   $getAppointments->execute();




   
   //echo 'POST' . "<br>";
   //echo $_POST['username'];
   $getPatients = $db->prepare("SELECT * FROM user WHERE user_type = 1 ");
   $getPatients->execute();

   
   
   
   
  	 
	 
	 
	 
	 $driverName = $db->prepare("SELECT u.first_name, u.last_name, a.driver_user_id, a.appointment_id FROM user AS u, appointments AS a
  WHERE u.user_id = a.driver_user_id AND a.driver_user_id = driver_user_id");
   $driverName->execute();
   
   
   
   
   
   
   
   
   
   
   
   
   
   

   
   
   
   
      
   
   

?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>MediCoupe - All Appointments</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    
    
    
            <link class="" href="pages/css/themes/calendar.css" rel="stylesheet" type="text/css" />
    
    
    
    
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    
    
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    
    
    
    <link href="assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="assets/css/custom.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    
    
    
    
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    
    
    
    
    
    
    
      <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete_pu, autocomplete_des;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  street_number_des: 'short_name',
  route_des: 'long_name',
  locality_des: 'long_name',
  administrative_area_level_1_des: 'short_name',
  country_des: 'long_name',
  postal_code_des: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete_pu = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_pu')),
      { types: ['geocode'] });
  autocomplete_des = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_des')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete_pu, 'place_changed', function() {
    fillInAddress();
  });
    google.maps.event.addListener(autocomplete_des, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete_pu.getPlace();
  var place = autocomplete_des.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete_pu.setBounds(circle.getBounds());
      autocomplete_des.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]




    </script>
    
    
    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  </head>
  <body class="fixed-header"  onload="initialize()">
  
  
  
  
 <!-- <script type='text/javascript'>alert('Hi');</script> -->
  
  
  
  
  
  
  
    <!-- USER INFO  -->
  
    <!-- MODAL STICK UP  -->
    <div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        
        
          <div class="modal-header clearfix text-left">
          
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            
          </div>
          
          
          <div class="modal-body">
          
          
          
          
          
           <div class="panel panel-transparent ">
           
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs nav-tabs-simple">
                        <li class="active">
                          <a data-toggle="tab" href="#slide1"><span>Profile</span></a>
                        </li>
                        <li>
                          <a data-toggle="tab" href="#slide2"><span>Payment</span></a>
                        </li>
                       
                      </ul>
                      
                      
                      
                      <!-- Tab panes -->
                      <div class="tab-content">
                        
                        
                        <div class="tab-pane slide-left active" id="slide1">
                          
                    
                             <div class="row">
                             <div class="col-md-12">
            
                          <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                 
                      
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                           <div class="row">
                            <div class="col-sm-5">
                              <input type="text" placeholder="<?php echo($_SESSION["first_name"]);?>" class="form-control" required>
                            </div>
                            <div class="col-sm-7 sm-m-t-10">
                              <input type="text" placeholder="<?php echo($_SESSION["last_name"]);?>" class="form-control">
                            </div>
                          </div>

                        </div>
                      </div>
                      
                          <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" placeholder="<?php echo($_SESSION["email"]);?>" required>
                        </div>
                      </div>
                      
                      
                        <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="password" placeholder="**********" required>
                        </div>
                      </div>
                      
                            <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="phone" placeholder="<?php echo($_SESSION["phone"]);?>" required>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Your Ride</label>
                        <div class="col-sm-9">
                          <div class="radio radio-success">
                            <input type="radio" value="blackcar" name="optionyes" id="male">
                            <label for="male">Black Car</label>
                            <input type="radio" checked="checked" value="ambulette" name="optionyes" id="female">
                            <label for="female">Ambulette</label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Profile Photo</label>
                        <div class="col-sm-9">
                          <div class="thumbnail-wrapper d48 circular bordered b-white m-r-15">
                                    <img alt="Avatar" width="55" height="55" data-src-retina="assets/img/profiles/avatar_small2x.jpg" data-src="assets/img/profiles/avatar.jpg" src="assets/img/profiles/avatar.jpg"> <input type="file" id="fileInput" name="fileInput" />
                                  </div>  
                                  <div class="m-t-10">
                                  
                                  <input type="file" id="fileInput" name="fileInput" />
                                                     
                      
						                
<script>
   function chooseFile() {
      $("#fileInput").click();
   }
</script>

						
                                  <button onclick="chooseFile();" type="button" class="btn btn-success">Set Photo</button>
                              
                                  



                        </div>
                                  
                        </div>
                      </div>
                      
                      <br>
                      
                      
                      
                      
                      
                      
                      
                      <div class="row">
                      
                        <div class="col-sm-12">
                      
                          <button class="btn btn-block btn-success" type="submit">Save</button>
                          
                        </div>
                        
                        
                      </div>
                    </form>
                  </div>
                          </div>
                                    </div>
              
          
                        
                        
                        <div class="tab-pane slide-left" id="slide2">
                        
                        
                        
                        
                          <div class="row">
                            <div class="col-md-12">
                              
                              
                              
                              
                              
                              
                              
                              
                              
                  
               
                        <form role="form">
                          <div class="bg-master-light padding-30 b-rad-lg">
                            <h2 class="pull-left m-t-15">Pay for a Ride</h2>
                             
                            <ul class="list-unstyled pull-right list-inline no-margin">
                              <li>
                                <a href="#">
                                  <img height="32" data-src-retina="assets/img/form-wizard/paypal.png" data-src="assets/img/form-wizard/paypal.png" class="brand" alt="logo" src="assets/img/form-wizard/paypal.png">
                                </a>
                              </li>
                             </ul>
                            <div class="clearfix"></div>
                            <div class="form-group form-group-default required m-t-25">
                              <label>PayPal User</label>
                              <input type="text" class="form-control" placeholder="PayPal E-Mail" required>
                            </div>
                            <div class="form-group form-group-default required">
                              <label>PayPal Password</label>
                              <input type="password" class="form-control" placeholder="PayPal Password" required>
                            </div>
                            <div class="row hide">
                              <div class="col-md-6">
                                <label>Expiration</label>
                                <br>
                                <select class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                  <option selected>Jan (01)</option>
                                  <option>Feb (02)</option>
                                  <option>Mar (03)</option>
                                  <option>Apr (04)</option>
                                  <option>May (05)</option>
                                  <option>Jun (06)</option>
                                  <option>Jul (07)</option>
                                  <option>Aug (08)</option>
                                  <option>Sep (09)</option>
                                  <option>Oct (10)</option>
                                  <option>Nov (11)</option>
                                  <option>Dec (12)</option>
                                </select>
                                <select class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                  <option value="2014">2014</option>
                                  <option value="2015">2015</option>
                                  <option value="2016">2016</option>
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                  <option value="2026">2026</option>
                                  <option value="2027">2027</option>
                                  <option value="2028">2028</option>
                                  <option value="2029">2029</option>
                                  <option value="2030">2030</option>
                                </select>
                              </div>
                              <div class="col-md-2 col-md-offset-4">
                                <div class="form-group required">
                                  <label>CVC Code</label>
                                  <input type="text" class="form-control" placeholder="000" required>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                    
               

                              
                              
                              
                              
                              
                              
                              
                              
                              </div>
                          </div>
                          
                          
                          
                        </div>
                        
                        
                        <div class="tab-pane slide-left" id="slide3">
                        
                        
                          <div class="row">
                             <div class="col-md-12">
            
                          <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                 
                      
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                           <div class="row">
                            <div class="col-sm-5">
                              <input type="text" placeholder="<?php echo($_SESSION["first_name"]);?>" class="form-control" required>
                            </div>
                            <div class="col-sm-7 sm-m-t-10">
                              <input type="text" placeholder="<?php echo($_SESSION["last_name"]);?>" class="form-control">
                            </div>
                          </div>

                        </div>
                      </div>
                      
                          <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" placeholder=" <?php echo($_SESSION["email"]);  ?>" required>
                        </div>
                      </div>
                      
                      
                        <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="password" placeholder="**********" required>
                        </div>
                      </div>
                      
                            <div class="form-group">
                        <label for="position" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="phone" placeholder="(949) 680-0173" required>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Your Ride</label>
                        <div class="col-sm-9">
                          <div class="radio radio-success">
                            <input type="radio" value="blackcar" name="optionyes" id="male">
                            <label for="male">Black Car</label>
                            <input type="radio" checked="checked" value="ambulette" name="optionyes" id="female">
                            <label for="female">Ambulette</label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Profile Photo</label>
                        <div class="col-sm-9">
                          <div class="thumbnail-wrapper d48 circular bordered b-white m-r-15">
                                    <img alt="Avatar" width="55" height="55" data-src-retina="assets/img/profiles/avatar_small2x.jpg" data-src="assets/img/profiles/avatar.jpg" src="assets/img/profiles/avatar.jpg">
                                  </div>  
                                  <div class="m-t-10">
                                  <button class="btn btn-success">Set Photo</button>
                        </div>
                                  
                        </div>
                      </div>
                      
                      <br>
                      
                      
                      
                      
                      
                      
                      
                      <div class="row">
                      
                        <div class="col-sm-12">
                      
                          <button class="btn btn-block btn-success" type="submit">Save</button>
                          
                        </div>
                        
                        
                      </div>
                    </form>
                  </div>
                          </div>
            
                          
                          
                        </div>
                      </div>
           </div>
              
            
        
          </div>
          
          
          
          
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP  -->

<!-- END USER INFO  -->
  
  
  

    
    
    
    
    
    
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        
        
        
        
        
         <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
              <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/logo_2x.png" height="40">
            </div>
            <!-- START NOTIFICATION LIST -->
             <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20 ">
            
            <li class="p-r-15 inline">
                 <a href="admin.php"><button href="#" class="fa fa-dashboard" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Dashboard"></button></a>
              </li>
            
         
            
            
            
            
            
              <li class="p-r-15 inline">
                   <a href="users.php"><button href="users.php" class="fa fa-users" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Users"></button></a>
              </li>
              
              <li class="p-r-15 inline">
                  <a href="#"><button href="appointments_list.php" class="pg-menu_justify" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="List View"></button></a>
              </li>
              <li class="p-r-15 inline active-view">
                   <a href="appointments_cal.php"><button href="appointments_cal.html" class="pg-calender" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Calendar View"></button></a>
              </li>
            </ul>
            <!-- END NOTIFICATIONS LIST -->
            <a href="#" class="search-link hide " data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> 
         <div class="inline p-l-20">
           <button style="padding-left:15px;" class="btn btn-success btn-rounded btn-animated from-top fa fa-car hide" data-target="#modalSlideLeft" data-toggle="modal" ><span class="title"><i class="pg-plus"> New Ride</i></span></button>
         </div>
            
            </div>
            
            
            
            
            
            
            
        </div>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set grid-box m-l-20 sm-no-margin hidden-sm hidden-xs hide" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        
        
        
        
        
        
        
        <div class=" pull-right">

          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            

          <!-- START NOTIFICATION LIST -->
            <ul class="notification-list">
              <li class="inline">
                <div class="dropdown ">
                
                
                
                
                  <a href="javascript:;" id="notification-center" data-toggle="dropdown">
                  
                  
                  <div class="thumbnail-wrapper d32 circular inline m-t-5">
              <img src="assets/img/profiles/1.jpg" alt="" data-src="assets/img/profiles/1.jpg" data-src-retina="assets/img/profiles/1_small2x.jpg" width="32" height="32">
            </div>
            
            
            <div class="pull-left p-l-10 p-r-10 p-t-10 fs-16 font-heading">
             <span class="semi-bold text-master" data-target="#myModal" data-toggle="modal"><a><?php echo($_SESSION["first_name"]);?> <?php echo($_SESSION["last_name"]);  ?></span></a>
            </div>

                   
                  </a>
                  
                  
                  
                                </li>
           
            </ul>
            <!-- END NOTIFICATIONS LIST -->
          
        </div>

  
          </div>
          <!-- END User Info-->
          
          
          
          
          
          
                     
                  
        
        
        
        
        
        
        
      </div>
      <!-- END HEADER -->
      
      
      
      
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        
        

        
        
        
        <!-- START PAGE CONTENT -->
        <div class="content">
        
        
          <!-- START JUMBOTRON -->
          <div class="jumbotron " data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
               
               
               
               
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Appointments</p>
                  </li>
                 
                </ul>
                <!-- END BREADCRUMB -->
                
                
                
            
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          
          
          
                  
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              
              
              <div class="panel-heading">
                <div class="panel-title">Appointments  
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <button id="show-modal" class="btn btn-success btn-cons"><i class="fa fa-plus"></i> New Ride</button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              
              
              <div class="panel-body">
              <div class="table-scrollable">
                
                
                
                
                
                
                
                
                
                
                
                <table class="table table-hover demo-table-dynamic" id="tableWithExportOptions">   
                
                
                  <thead>
<tr>
                    <!-- 1 -->	<th id="appointment_id">appointment_id</th>
					<!-- 2 -->	<th id="appointment_title">appointment_title</th>
					<!-- 3 -->	<th id="scheduler_user_id" class="hide"></th>
					<!-- 4 -->	<th id="transport_type">transport_type</th>
					<!-- 5 -->	<th id="pickup_mapAddress">pickup_mapAddress</th>
					<!-- 6 -->	<th id="destination_mapAddress">destination_mapAddress</th>
					<!-- 7 -->	<th id="appointment_date">appointment_date</th>
					<!-- 8 -->	<th id="appointment_time">appointment_time</th>
					
					<!-- 9 -->	<th id="scheduled_pickup_time" class="hide"></th>
					<!-- 10 -->	<th id="destination_timeStamp" class="hide"></th>
					<!-- 11 -->	<th id="pickup_timeStamp" class="hide"></th>
					<!-- 12 -->	<th id="actual_trip_time" class="hide"></th>
					<!-- 13 -->	<th id="trip_distance" class="hide"></th>
					<!-- 14 -->	<th id="scheduled_trip_time" class="hide"></th>
					<!-- 15 -->	<th id="appointment_request_timeStamp" class="hide"></th>
					<!-- 16 -->	<th id="timeStamp" class="hide"></th>
					<!-- 17 -->	<th id="active" class="hide"></th>
					
					<!-- 18 -->	<th id="appointment_status">appointment_status</th>
					
					<!-- 19 -->	<th id="cancelBy_user_id" class="hide"></th>
					<!-- 20 -->	<th id="appointment_cancel_timeStamp" class="hide"></th>
					<!-- 21 -->	<th id="device_type" class="hide"></th>
					
					<!-- 22 -->	<th id="patient_user_id" class="hide"></th>
					<!-- 23 -->	<th id="driver_user_id">driver_user_id</th>
					
					<!-- 24 -->	<th id="scheduled_destination_time" class="hide"></th>
					<!-- 25 -->	<th id="trip_cost" class="hide"></th>   
					<!-- 26 -->	<th></th>
                    </tr>
                  </thead>
                  
                  
                  
                  
                                   <tbody>
                  
<?php while ($row = $getAppointments->fetch(PDO::FETCH_ASSOC)) : 




   
   $getPatientName = $db->prepare("SELECT user.user_id, user.first_name, user.last_name

								FROM `user`
								INNER JOIN `appointments` on 
								
								user.user_id = appointments.patient_user_id 
								
								
								WHERE appointments.appointment_id = :appointment_id");
	$getPatientName->bindValue(':appointment_id', $row['appointment_id']);

   
   $getPatientName->execute();




$PatientName = $getPatientName->fetchAll();















//driver loop
	                  
	                  
	$apptmentID = $row['appointment_id'] ; 
	                  
	                  
	//NESTED WHILE LOOP QUERY FOR ASSIGN DRIVERS MODAL                  
   $getDrivers = $db->prepare("SELECT * FROM user WHERE user_type = 3");
   $getDrivers->execute();
   
	                  
	                  
	                  
	                  
                  ?>
                  
                  
                    <tr>
  <!-- 1 -->                       <td name="appointment_id" class="v-align-middle">
                        <? echo $row['appointment_id'] ;  ?>
                      </td>


  <!-- 2 -->  				  <td name="appointment_title" class="v-align-middle">
                        <? echo $row['appointment_title'] ;  ?>
                      </td>
                      
                      
  <!-- 3 -->                     <td class="hide"> </td>                    
                      

                     
                     
   <!-- 4 -->                      <td name="transport_type" class="v-align-middle">
                                  <div                                    
                                   	<?php if($row['transport_type'] == 'Ambulette')
                                                            {
                                                                echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/ambulance.jpg' alt='' width='32' height='32'>"; 

                                                                
																
                                                                
                                                            } 
                                                       
                                                                elseif ($row['transport_type'] == 'Black Car') 
                                                            {
                                                            	echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/car.jpg' alt='' width='32' height='32'>";
                                                               
																
                                                                
                                                            }
                                                            
														
														
                                                    ?>
                         </div> 
                      
                      </td>
                      
                      
                      
                      
                      
                      
                      
  <!-- 5 -->                      <td name="pickup_address" class="v-align-middle"><button style="border: none; background-color: transparent;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="<? echo $row['pickup_mapAddress'] ;  ?>">
                       <? echo $row['pickup_mapAddress'] ;  ?>
                      </td>
                      
                      
                      
                      
                      
                      
                      
  <!-- 6 -->                      <td name="destination_address" class="v-align-middle">
  
  <button style="border: none; background-color: transparent;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="<? echo $row['destination_mapAddress'] ;  ?>">
  				
                         <? echo $row['destination_mapAddress'] ;  ?></button>
                      </td>
                      
                      
                       
                      
                      
                      
                      
                      
                      
  <!-- 7 -->                      <td name="appointment_date" class="v-align-middle">
                         <? echo $row['appointment_date'] ;  ?>
                      </td>
                      
                      
                      
                      
                      
                      
   <!-- 8 -->                     <td name="appointment_time" class="v-align-middle">
                         <? echo $row['appointment_time'] ;  ?>
                      </td>




<!-- 9 -->                     <td class="hide"> </td>
<!-- 10 -->                     <td class="hide"> </td>
<!-- 11 -->                     <td class="hide"> </td>
<!-- 12 -->                     <td class="hide"> </td>
<!-- 13 -->                     <td class="hide"> </td>
<!-- 14 -->                     <td class="hide"> </td>
<!-- 15 -->                     <td class="hide"> </td>
<!-- 16 -->                     <td class="hide"> </td>
<!-- 17 -->                     <td class="hide"> </td>

                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
  <!-- 18 -->                       <td name="appointment_status" class="v-align-middle">
                       		<span                                     
                                   				<?php if($row['appointment_status'] == 'Confirmed')
                                                            {
                                                                echo "class='label label-success'>"; 

                                                                echo 'Confirmed';
																
                                                                
                                                            } 
                                                       
                                                                elseif ($row['appointment_status'] == 'Scheduled') 
                                                            {
                                                            	echo "class='label label-warning'>";
                                                                echo 'Scheduled';
																
                                                                
                                                            }
                                                            
														
															
													
                                                            
                                                                elseif ($row['appointment_status'] == 'Cancelled') 
                                                            {
                                                            	echo "class='label label-important'>";
                                                                echo 'Cancelled';
															
                                                                
                                                            }
                                                            
                                                            

                                                            
                                                            
                                                                else 
                                                            {
                                                            echo "class='label label-default'>";
                                                                echo 'N/A';
                                                            }
                                                    ?>
                         </span>                
                      </td>
                      
                      
                      
<!-- 19 -->                     <td class="hide"> </td> 
<!-- 20 -->                     <td class="hide"> </td>
<!-- 21 -->                     <td class="hide"> </td>                     
                      
                      
 <!-- 22 -->           <td name="patient_user_id" class="v-align-middle hide">
                        <p>
                        	<div class="thumbnail-wrapper d32 circular inline ">
								<img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" 
								data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
							</div>  <? echo $row['patient_user_id'] ;  ?> 						</p>
                      </td>
                      
                      
                      
                      
                      
                      
 <!-- 23 -->                      <td name="driver_user_id" class="v-align-middle">
                        <p>
                        	<div class="thumbnail-wrapper d32 circular inline ">
								<img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" 
								data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
							</div>  
						<span class="p-l-10 inline-block"><? echo $row['driver_user_id'] ;  ?></span> 
						</p>
                      </td>
                      
                      
<!-- 24 -->                     <td class="hide"> </td>
<!-- 25 -->                     <td class="hide"> </td>
                      
                      
                      
                      
                      
                      
<!-- 26 -->                      <td class="v-align-middle">
                                           		
                        
                           <button class="btn-list btn-success-list" href="#modalSlideUp<? echo $row['appointment_id'] ;  ?>" data-toggle="modal"><span>Assign Driver</span></button><br><br>
                           
                           
                           
                           
                           <span><? echo $row['appointment_id'] ;  ?></span>
                           
                           
                           
                          <button class="btn-list btn-danger-list" href="#cancelApptModal<? echo $row['appointment_id'] ;  ?>" data-toggle="modal"><span>Cancel Ride</span></button>
                           
                                                      
                           
                           
<!-------------------------------------------------- CANCEL APPOINTMENT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="cancelApptModal<? echo $row['appointment_id'] ;  ?>" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Are you sure you want to cancel this appointment?</h2>
              
            </div>
            
            
            <div class="modal-body">
  
  
	
               <form id="form-event" action="appt_list_1.php" role="form" method="post">
      <input type='hidden' name='cancelAppointment' id='cancelAppointment' value='1'/>
     
     
     
      
     
     
     <input type="hidden" class="form-control" name="apptment_id" value="<? echo $row['appointment_id'] ;  ?>" placeholder="<? echo $row['appointment_id'] ;  ?>">
    
      
                          
                          <button class="btn btn-success"><i class="fa fa-trash"></i> &nbsp; Cancel Appointment</button>
                          
      </form>
                        
            
                          
            
              
              
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- CANCEL APPOINTMENT MODAL ------------------------------------------------------------->
                      

                           
                           
                           
                           
                           
                           
       
      
                           
                           
                           
                           
                           
                             
                    
                    
                    
                    
                    
                    
                      </td>
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                                 
                      
                      
                    
 <!-------------------------------------------------- PAIR DRIVER MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp<? echo $row['appointment_id'] ;  ?>" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Assign a Driver</h2>
              <p class="p-b-10">Select a Driver from the List of Available Drivers Below..</p>
            </div>
            
            
            <div class="modal-body">
  
  
	
            
            
            <form id="assign_Driver" action="appt_list_1.php" role="form" method="post">
      <input type='hidden' name='assignDriver' id='assignDriver' value='1'/>
      
                     
                     
                 <input type="hidden" name="appt_id" value="<? echo $row['appointment_id'] ;  ?>">
                     
                     
                     
			
        
                               

                         
                         
                    <select class="full-width form-control-default" name="driver_id">
                             
                             
                             
                         <?php while ($row2 = $getDrivers->fetch(PDO::FETCH_ASSOC)) : ?>
                                                  
                               <option value="<? echo $row2['user_id'] ;  ?>">
                               
                               		<span><? echo $row2['user_id'] ;  ?> <? echo $row2['first_name'] ;  ?> <? echo $row2['last_name'] ;  ?></span>
                               
                               </option>
                     
						 <?php endwhile; ?>
                     
                      
                      
                    </select>  
                                    
                                                  	 
                                         
                    
                      
                    <input type="hidden" class="form-control" name="apptment_id" value="<? echo $row['appointment_id'] ;  ?>" placeholder="<? echo $row['appointment_id'] ;  ?>">             
                    
                    
                    
              <div class="row">
              
            
                
                <div class="col-sm-4 m-t-10 sm-m-t-10">
                  <button id="assignDriverBtn" class="btn btn-success btn-block m-t-5">Save</button>
                </div>
                
                
                
                
                
                </form>

                
            
              
              
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END PAIR DRIVER MODAL ------------------------------------------------------------->
                      
                      

                      
                      
                      
                      
                      
                       
                      
                    </tr>
                    
                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    	<?php endwhile; ?>
                    
                    
                    
                    
                    





                    
                  </tbody>
                </table>

                
                
              </div>
              </div>
            </div>
            <!-- END PANEL -->
            
          </div>
          <!-- END CONTAINER FLUID -->
        
           </div>
        <!-- END PAGE CONTENT -->
        
        
        
        
        
        
        
        
        
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2014 </span>
              <span class="font-montserrat">MediCoupe</span>.
              <span class="hint-text">All rights reserved. </span>
            </p>
            
            <p class="small no-margin pull-right sm-pull-reset">
              <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of Use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
            </p>
            
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
        
        
        
        
        
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        
        <li>
          <a href="#quickview-alerts" data-toggle="tab"><i class="fa fa-warning"></i></a>
        </li>
        <li class="active">
          <a href="#quickview-chat" data-toggle="tab"><i class="fa fa-comment"></i></a>
        </li>
        <li>
          <a href="#quickview-notes" data-toggle="tab"><i class="fa fa-edit"></i></a>
        </li>
      </ul>
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- BEGIN Notes !-->
        <div class="tab-pane fade  in no-padding" id="quickview-notes">
          <div class="view-port clearfix quickview-notes" id="note-views">
            <!-- BEGIN Note List !-->
            <div class="view list" id="quick-note-list">
              <div class="toolbar clearfix">
                <ul class="pull-right ">
                  <li>
                    <a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
                  </li>
                  <li>
                    <a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
                  </li>
                </ul>
                <button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
              </div>
              <ul>
                <!-- BEGIN Note Item !-->
                <li data-noteid="1" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox1" type="checkbox" value="1">
                      <label for="qncheckbox1"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox2" type="checkbox" value="1">
                      <label for="qncheckbox2"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox3" type="checkbox" value="1">
                      <label for="qncheckbox3"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="3" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox4" type="checkbox" value="1">
                      <label for="qncheckbox4"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="4" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox5" type="checkbox" value="1">
                      <label for="qncheckbox5"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
              </ul>
            </div>
            <!-- END Note List !-->
            <div class="view note" id="quick-note">
              <div>
                <ul class="toolbar">
                  <li><a href="#" class="close-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="pg-arrow_left"></i></a>
                  </li>
                  <li><a href="#" class="Bold"><i class="fa fa-bold"></i></a>
                  </li>
                  <li><a href="#" class="Italic"><i class="fa fa-italic"></i></a>
                  </li>
                  <li><a href="#" class=""><i class="fa fa-link"></i></a>
                  </li>
                </ul>
                <div class="body">
                  <div>
                    <div class="top">
                      <span>21st april 2014 2:13am</span>
                    </div>
                    <div class="content">
                      <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END Notes !-->
        <!-- BEGIN Alerts !-->
        <div class="tab-pane fade no-padding" id="quickview-alerts">
          <div class="view-port clearfix" id="alerts">
            <!-- BEGIN Alerts View !-->
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default navbar-sm">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Notications
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-search"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              
              
              
              
              <!-- BEGIN Alert List !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <!-- BEGIN List Group !-->
                
                
                <div class="list-view-group-container">
                
                
                <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Appointments
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                  
                  
                  
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">New Ride</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today <br></span>
                          <span class="text-master">5:00 pm</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                      <!-- BEGIN List Group Item!-->
                    </li>
                    
                    
                    
                    <!-- END List Group Item!-->
                    
                    
                    
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">Ride <span class="label label-success">Confirmed</span></span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">12/15/14</span>
                          <span class="text-master">5:00 pm</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
                <!-- END List Group !-->
                
                
                
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                   Group
                  </div>
                  <!-- END List Group Header!-->
                  
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">“Sub Title"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                    
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">“Sub-Title"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    
                    
                    
                    
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Group
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Title<br></span>
                          <span class="text-master">Sub-Title</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
              </div>
              <!-- END Alert List !-->
            </div>
            <!-- EEND Alerts View !-->
          </div>
        </div>
        <!-- END Alerts !-->
        <div class="tab-pane fade in active no-padding" id="quickview-chat">
          <div class="view-port clearfix" id="chat">
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-plus"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Customer Support
                    <div class="fs-11">Show All</div>
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    a</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">Ava Flores</span>
                            <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">b</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">Bella McCoy</span>
                             <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">I have a bunch of ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">bob stephens</span>
                             <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Can someone please ...</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">c</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">carole roberts</span>
                             <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                           <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">christopher perez</span>
                             <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">d</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">danielle fletcher</span>
                             <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">david sutton</span>
                             <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">e</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">earl hamilton</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">elaine lawrence</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">ellen grant</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">erik taylor</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">everett wagner</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">f</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">freddie gomez</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">g</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">glen jensen</span>
                           <span class="pull-right"><span class="badge badge-empty badge-warning"> </span> Pending</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">gwendolyn walker</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">j</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">janet romero</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">k</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">kim martinez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">l</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lawrence white</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">leroy bell</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">letitia carr</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lucy castro</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">m</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mae hayes</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marilyn owens</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marlene cole</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha warren</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha dean</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mia diaz</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">n</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">noah elliott</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">p</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">phyllis hamilton</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">r</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">raul rodriquez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">rhonda barnett</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">roberta king</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">s</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">scott armstrong</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sebastian austin</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sofia davis</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">t</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">terrance young</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">theodore woods</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">todd wood</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">tommy jenkins</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">w</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">wilma hicks</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
              </div>
            </div>
            
            
            
            
            
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    John Smith
                    <div class="fs-11 hint-text">Online</div>
                  </div>
                  <a href="#" class="link text-master inline action p-r-10 pull-right hide">
                    <i class="pg-more"></i>
                  </a>
                </div>
              </div>
              <!-- END Header  !-->
              
              
              <!-- BEGIN Conversation  !-->
              <div class="chat-inner" id="my-conversation">
              
              
              
              
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix hide">
                  <div class="chat-bubble from-me hide">
                    Hello there
                  </div>
                </div>
                <!-- END From Me Message  !-->
                
                
                
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    I have a bunch of questions about how to sign up patients in my care at the hospital. Can someone please follow up with me??
                  </div>
                </div>
                <!-- END From Them Message  !-->
                
                
                
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Did you check out the FAQ section ?
                  </div>
                </div>
                <!-- END From Me Message  !-->
                
                  <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    No, where can I access that??

                  </div>
                </div>
                <!-- END From Them Message  !-->
                
                
                
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                   
                Here is the link!
                    </div>
                </div>
                <!-- END From Me Message  !-->
                
             
                
                
             
                
                
              </div>
              <!-- BEGIN Conversation  !-->
              <!-- BEGIN Chat Input  !-->
              <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="col-xs-8 no-padding">
                    <input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
                  </div>
                  <div class="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top hide">
                    <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
          
                  
                  
          
    
    
    
    
    
    
    
    
    
    
    
      <!-- MODAL STICK UP  -->
        <div class="modal fade slide-right" id="addNewAppModal" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
             
             
             
                
       
              
             

               
  <form id="form-event" action="appt_list_1.php" role="form" method="post">
      <input type='hidden' name='submitted_event' id='submitted_event' value='1'/>
 
 
 <!--   <div class="quickview-wrapper calendar-event" id="calendar-event">
      <div class="view-port clearfix" id="eventFormController"> -->
 
 
        <div class="view bg-white">
          <div class="scrollable">
          
     
          
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
          
     
          
            <div class="p-l-30 p-r-30 p-t-20">
            
         
                         
               <h4>Schedule a Ride</h4>
              
            </div>
            
            
            <div class="p-t-15">
              <input id="eventIndex" name="eventIndex" type="hidden">
              
              
              
              
              
              <div class="form-group-attached">
              
              
                <div class="form-group form-group-default ">
                  <label>Title</label>
                  <input type="text" class="form-control" id="appointment_title" name="appointment_title" placeholder="event name">
                </div>



				<div class="form-group form-group-default ">
                  <label>Patient</label>
                  
                  
                 
                     <select class="full-width" data-init-plugin="select2" name="patient_user_id">
                  <?php while ($row = $getPatients->fetch(PDO::FETCH_ASSOC)) : ?>
                               
                  	<option value="<? echo $row['user_id'] ;  ?>"><span><? echo $row['first_name'] ;  ?> <span class="bold"><? echo $row['last_name'] ;  ?></span></option>
                  	
                  	  <?php endwhile; ?>
          
                  
                  
                 
                          </select>
                  
                  
                  
                  
                </div>

                
                
                 <div class="form-group form-group-default ">
                  <label>Ride Type</label>

                <div class="radio radio-success">
                      <input type="radio" value="Black Car" name="rideOption" id="yes">
                      <label for="yes">Black Car</label>
                      <input type="radio" checked="checked" value="Ambulette" name="rideOption" id="no">
                      <label for="no">Ambulette</label>
                    </div>
                  </div>
                
               
                
                  <div class="form-group form-group-default input-group">
                      <label>Appointment Date</label>
                     <span id="event-date" name="appointment_date" class="p-l-10 hide"></span>
                      <input type="date" class="form-control" name="appointment_date" id="datepicker-component2">
                      <span class="input-group-addon">
                                                  <i class="fa fa-calendar"></i>
                                                </span>
                    </div>
                
                
                
                
                
                
                
                
                
                
                
                 <div class="form-group form-group-default input-group">
                  <label>Time</label>
                  <span id="lblfromTime" class="p-l-10 hide"></span>
                  <input type="date" class="form-control time" name="appointment_time" id="basicExample">
                <span class="input-group-addon">
                                                  <i class="fa fa-clock-o"></i>
                                                </span>

                </div>
                
                
                
            
    
    
    
    
                <div class="form-group form-group-default">
                  <label>Start Address</label>
                  
                  <input id="autocomplete_pu" class="form-control" name="pickup_mapAddress" placeholder="Pick Up"
             onFocus="geolocate()" type="text"></input>
                <table id="address" hidden>
                  <tr>
                    <td class="label">Street address</td>
                    <td class="slimField"><input class="field" name="pickup_address" id="street_number"
                          disabled="true"></input></td>
                    <td class="wideField" colspan="2"><input class="field" id="route"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">City</td>
                    <td class="wideField" colspan="3"><input class="field" name="pickup_city" id="locality"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field"
                          name="pickup_state" id="administrative_area_level_1" disabled="true"></input></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field" name="pickup_zip" id="postal_code"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">Country</td>
                    <td class="wideField" colspan="3"><input class="field"
                          id="country" disabled="true"></input></td>
                  </tr>
                </table>
             	
                </div>
                
               
               
               
               
                <div class="form-group form-group-default">
                  <label>End Address</label>
                  
                  <input id="autocomplete_des" name="destination_mapAddress" class="form-control" placeholder="Destination"
             onFocus="geolocate()" type="text"></input>
                  
                  <table id="address" hidden>
                      <tr>
                        <td class="label">Street address</td>
                        <td class="slimField"><input class="field" name="destination_address" id="street_number_des"
                              disabled="true"></input></td>
                        <td class="wideField" colspan="2"><input class="field" id="route"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">City</td>
                        <td class="wideField" colspan="3"><input class="field" name="destination_city" id="locality_des"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">State</td>
                        <td class="slimField"><input class="field"
                              name="destination_state" id="administrative_area_level_1_des" disabled="true"></input></td>
                        <td class="label">Zip code</td>
                        <td class="wideField"><input class="field" name="destination_zip" id="postal_code_des"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="label">Country</td>
                        <td class="wideField" colspan="3"><input class="field"
                              id="country" disabled="true"></input></td>
                      </tr>
                    </table>
             	
                </div>
                
               
                
                
                <div class="row clearfix">
                  <div class="form-group form-group-default">
                    <label>Note</label>
                    <textarea class="form-control" placeholder="description" id="txtEventDesc"></textarea>
                  </div>
                </div>
                
                
                
              </div>
            </div>
            
            
            
            <div class="p-l-30 p-r-30 p-t-30">
              <button id="eventSave" class="btn btn-success btn-cons">Save Event</button>
              <button id="eventDelete" class="btn btn-white"><i class="fa fa-trash-o"></i>
              </button>

            </div>
            
            
          </div>
        </div>
        
        
                
        
        
        
        
        
    
      </form>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- END MODAL STICK UP  -->

    
    
    
    
    
    
    
    
    
    
    
    
    
           <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>

    <script type="text/javascript" src="assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    
    
    
    
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>

    
    
    
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/form_elements.js" type="text/javascript"></script>
    <script src="assets/js/datatables.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  </body>
</html>