<?php

session_start();
if ($_SESSION['user_type'] != 4 ){  //User_type = 4 = Admin
	header("Location:login.php");
	exit;
} 

//database connection
include 'connect.php';

$ajax = new Ajax($_POST, $db);
echo $ajax->json();







class Ajax {
	
	private $code = 0;
	private $body = '';
	private $post = array();
	private $target = null;
	private $db = array();
	
	
	public function __construct($post, $db){
		$this->db = $db;
		$this->post = (object) $post;
		if(empty($this->post->target)){
			exit('Target empty....');
		}
		$this->target = $this->post->target;
		unset($this->post->target);
		$this->processing();
	}
	
	public function json(){
		return json_encode(array('code'=>$this->code,'body'=>$this->body));
	}

	private function processing(){
		$this->{$this->target}();
	}
	
	
	
	
	private function hideAppts(){
		foreach($this->post->ids as $id){
			$updateAppt = $this->db->prepare("UPDATE appointments SET list_visible=0 WHERE appointment_id = '$id'");
			$updateAppt->execute();
			$this->body .= ' + '.$id;
		}
		$this->code = 1;
		$this->body .= ' Success';
	}
	
	private function editAppt(){
		$data=array();
		foreach($this->post as $k=>&$v){
			if($k=='appointment_date') $v = date('Y-m-d',strtotime($v));
			else if($k=='appointment_time')  $v = date('H:i',strtotime($v));
			else if($k=='appointment_title')  $v = trim(preg_replace('|\s+|',' ',$v));
			//---
			$data[] = "$k='$v'";
		}
		$updateAppt = $this->db->prepare("UPDATE appointments SET ".implode(', ',$data)." WHERE appointment_id = '".$this->post->appointment_id."'");
		if($updateAppt->execute()){
			$this->code = 1;
			$this->body = implode(', ',$data);
		}

		
	}
	
	
}


?>