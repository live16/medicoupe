/* ============================================================
 * Google Map
 * Render maps using Google Maps JS API
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
(function($) {

    'use strict';

    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    var map;
    var zoomLevel = 13;

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: zoomLevel,
            disableDefaultUI: true,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(40.7300, -74.0710), // Jersey City, NJ
            }
       

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        
        
        var mapElement = document.getElementById('google-map');

        // Create the Google Map using out element and options defined above
        map = new google.maps.Map(mapElement, mapOptions);
    }

    $(document).ready(function() {
        $('#map-zoom-in').click(function() {
            map.setZoom(++zoomLevel);
        });
        $('#map-zoom-out').click(function() {
            map.setZoom(--zoomLevel);
        });
    });

})(window.jQuery);