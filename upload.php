<?php

if(empty($_SESSION)) session_start();


if(!empty($_FILES["fileToUpload"]["tmp_name"]) && !empty($_SESSION["user_id"]))
{

	$userID = $_SESSION["user_id"];
	$target_dir = "user_img/";
	$target_file = $target_dir . $userID . ".jpg";
	$tmp_file = $_FILES["fileToUpload"]["tmp_name"];	


	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($_FILES["fileToUpload"]['name'], PATHINFO_EXTENSION));

	// Check if image file is a actual image or fake image
    $check = getimagesize($tmp_file);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        //echo "File is not an image.";
        $uploadOk = 0;
    }



	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 500000) {
	    //echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	
	// Allow certain file formats
	$allowedExt =  array('jpg','jpeg','png','gif');
	if(!in_array($imageFileType, $allowedExt)){
	    //echo "Sorry, only ".implode(', ',$allowedExt)." files are allowed... Current:'$imageFileType'";
	    $uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    //echo "Sorry, your file was not uploaded.";
	
	// if everything is ok, try to upload file
	} else {
	    
			switch($imageFileType) {
			    case 'jpg':
			    case 'jpeg':
			       $image = imagecreatefromjpeg($tmp_file);
			    break;
			    case 'gif':
			       $image = imagecreatefromgif($tmp_file);
			       break;
			    case 'png':
			       $image = imagecreatefrompng($tmp_file);
			       break;
			 }
			 
			 
			 //$thumb = imagecreatetruecolor( $check[0], $check[0]);
			 // Resize and crop
			 //imagecopyresampled($thumb, $image, 0, 0, 0, 0, $check[0], $check[1], $check[0], $check[1]);
			 
			 imagejpeg($image, $target_file, 100);
	}	
		
	
	
}



?>