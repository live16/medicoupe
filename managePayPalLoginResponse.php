<?php
	session_start();

	include 'connect.php';

	if(!$PAYPAL->savePreApprovalDetails($_SESSION['user_id'], $_SESSION['preapprovalKey'])){
		exit('An error occurred, Please contact the administrator...');
	}
	
	header('Location: '.MEDI_URL.'/login.php');
	exit;
?>
