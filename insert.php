<?php

include 'connect.php';

try {
$stmt = $db->prepare("INSERT INTO user (user_id,user_type,first_name,last_name,phone,address1,
								city,state,email,password,emergency_name,emergency_phone,timeStamp,active) 
					  VALUES (:user_id,:user_type,:first_name,:last_name,:phone,:address1,
								:city,:state,:email,:password,:emergency_name,:emergency_phone,:timeStamp,:active)");

$stmt->bindValue(':user_id', uniqid());
$stmt->bindValue(':user_type', 2);
$stmt->bindValue(':first_name', 'Derek');
$stmt->bindValue(':last_name', 'Stock');
$stmt->bindValue(':phone', '2345678910');
$stmt->bindValue(':address1', '123 Street');
$stmt->bindValue(':city', 'Aliso Viejo');
$stmt->bindValue(':state', 'CA');
$stmt->bindValue(':email', 'derek@ninthcoastss2.com');
$stmt->bindValue(':password', 'myPass');
$stmt->bindValue(':emergency_name', '');
$stmt->bindValue(':emergency_phone', 123456789);
$stmt->bindValue(':timeStamp', '92837393');
$stmt->bindValue(':active', 1);
$stmt->execute();
echo 'execute' . "<br>";
printf("uniqid(): %s\r\n", uniqid());
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

?>