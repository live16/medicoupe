
<?php


if(empty($_SESSION)) session_start();

include 'connect.php';


function getUserName($user_id, $db){
	$user = $db->prepare("SELECT * FROM user WHERE user_id = '$user_id'");
	$user->execute();
	$row = $user->fetch();
	return empty($row['first_name']) ? null : $row['first_name'].' '.$row['last_name'];
}



$calendarStmt = $db->prepare("SELECT * FROM appointments WHERE patient_user_id = :patient_user_id");
$calendarStmt->bindValue(':patient_user_id', $_SESSION["user_id"] );
$calendarStmt->execute();


$events = '';
 
 while ($row = $calendarStmt->fetch(PDO::FETCH_ASSOC))
 {
 	$start_end = $row['appointment_date'].'T'.$row['appointment_time'];
	$patient = getUserName($row['patient_user_id'], $db);
	$driver = getUserName($row['driver_user_id'],$db);
	
	
	$events .= '{';
	
	$events .= 'title: "'.str_replace("'","\'",$row['appointment_title']).'",';
	$events .= 'class: "bg-success-lighter",';
	$events .= 'start: "'.$start_end.'",';
	$events .= 'end: "'.$start_end.'",';
	
	$events .= 'other: ';
		$events .= '{';
			$events .= 'transport_type : "'.$row['transport_type'].'",';
			$events .= 'destination_address : "'.$row['destination_mapAddress'].'",';
			$events .= 'pickup_address : "'.$row['pickup_mapAddress'].'",';
			$events .= 'patient: "'.$patient.'",';
			$events .= 'driver: "'.$driver.'",';
		$events .= '}';
	
	$events .= '},';
	
 }


?>

<script type="text/javascript">
(function($) {

    'use strict';

     $(document).ready(function() {

        //Multiselect - Select2 plug-in
        $("#multi").val(["Jim", "Lucy"]).select2();

        //Date Pickers
        $('#datepicker-range, #datepicker-component, #datepicker-component2').datepicker();

        $('#datepicker-embeded').datepicker({
            daysOfWeekDisabled: "0,1"
        });

        var selectedEvent;
        $('body').pagescalendar({
            //Loading Dummy EVENTS for demo Purposes, you can feed the events attribute from 
            //Web Service
            events: [ <?=$events?> ],

            
            onViewRenderComplete: function() 
            {
                //You can Do a Simple AJAX here and update 
               $('.grid').scrollTop(460);
            },
            
            
            onEventClick: function(event) {
                //Open Pages Custom Quick View
                if (!$('#calendar-details').hasClass('open'))
                    $('#calendar-details').addClass('open');

				console.log(event);
				
                selectedEvent = event;
                setEventDetailsToForm(selectedEvent);
            },
            
            onEventDragComplete: function(event) {
                selectedEvent = event;
                setEventDetailsToForm(selectedEvent);
            },
            
            onEventResizeComplete: function(event) {
                selectedEvent = event;
                setEventDetailsToForm(selectedEvent);
            },
            
            //onTimeSlotDblClick: function(timeSlot) {
                //Adding a new Event on Slot Double Click
              //  var newEvent = {
             //       title: 'New Ride',
               //     class: 'bg-complete',
                 //   start: timeSlot.date,
                   // end: moment(timeSlot.date).add(1, 'hour').format(),
                 //   allDay: false,
                   // other: {
                        //You can have your custom list of attributes here
                     //   note: 'test'
                  //  }
              //  }; 
               // selectedEvent = newEvent;
              //  $('body').pagescalendar('addEvent', newEvent);

                //Open Pages Custom Quick View
             //   if (!$('#calendar-event').hasClass('open'))
               //     $('#calendar-event').addClass('open');
             //   setEventDetailsToForm(selectedEvent);
         //   }
        
        });
        
        //After the settings Render you Calendar
        $('body').pagescalendar('render');

        // Some Other Public Methods That can be Use are below \
        //console.log($('body').pagescalendar('getEvents'))
        //get the value of a property
        //console.log($('body').pagescalendar('getDate','MMMM'));

        function setEventDetailsToForm(event) {
            $('#eventIndex').val('');
            $('#txtEventName').val('');
            $('#txtEventCode').val('');
            $('#txtEventLocation').val('');
            
            //Show Event date
            $('#appointment-title').html('<b style="font-size:22px">' + event.title + '</b>');
            $('#appointment-date').html(moment(event.start).format('dddd, MMM D, YYYY'));
            $('#appointment-time').html(moment(event.start).format('h:mm A'));
            
            $('#pickup-address').html('<b>PickUp:</b> ' + event.other.pickup_address);
            $('#destination-address').html('<b>Destination:</b> ' + event.other.destination_address);
            
            $('#c-patient').html('<b>Patient:</b> ' + event.other.patient);
            $('#c-driver').html('<b>Driver:</b> ' + event.other.driver);
            
            
            //$('#lbltoTime').text(moment(event.end).format('H:mm A'));

            //Load Event Data To Text Field
            $('#eventIndex').val(event.index);
            $('#txtEventName').val(event.title);
            $('#txtEventCode').val(event.other.code);
            $('#txtEventLocation').val(event.other.location);
        }

        $('#eventSave').on('click', function() {
            selectedEvent.title = $('#txtEventName').val();

            //You can add Any thing inside \"other\" object and it will get save inside the plugin.
            //Refer it back using the same name other.your_custom_attribute

            selectedEvent.other.code = $('#txtEventCode').val();
            selectedEvent.other.location = $('#txtEventLocation').val();

            $('body').pagescalendar('updateEvent', $('#eventIndex').val(), selectedEvent);

            $('#calendar-event').removeClass('open');
        });
        $('#eventDelete').on('click', function() {
            $('body').pagescalendar('removeEvent', $('#eventIndex').val());
            selectedEvent.other.code = $('#txtEventCode').val();
            selectedEvent.other.location = $('#txtEventLocation').val();

            $('#element').pagescalendar('updateEvent', $('#eventIndex').val(), selectedEvent);
            $('#calendar-event').removeClass('open');

        });
        $('#eventDelete').on('click', function() {
            $('#element').pagescalendar('removeEvent', $('#eventIndex').val());
            $('#calendar-event').removeClass('close');
        });
    });

})(window.jQuery);


</script>
