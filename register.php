<?php

//database connection info db createAccount
include 'connect.php';


//Add Patient Form - Sign Up
if(isset($_POST['patient_signup']))
{
   try {
        $stmt = $db->prepare("INSERT INTO user (user_id,
        user_type,
        first_name,
        last_name,
        phone,
        mapAddress,
        state,
        zip_code,email,password,emergency_first_name,emergency_last_name,emergency_phone,timeStamp,active) 
					  VALUES (:user_id,:user_type,:first_name,:last_name,:phone,:mapAddress,:state,:zip_code,:email,:password,:emergency_first_name,:emergency_last_name,:emergency_phone,:timeStamp,:active)"); 
						
		$date = new DateTime();

		$u_id = uniqid();
								       
        
		$stmt->bindValue(':user_id', $u_id ); 
		$stmt->bindValue(':user_type', 1); //USER_TYPE = PATIENT
		$stmt->bindValue(':first_name', $_POST['first_name']);
		$stmt->bindValue(':last_name', $_POST['last_name']);
		$stmt->bindValue(':phone', preg_replace("|[^\d]+|","",$_POST['phone']));
		
		$stmt->bindValue(':mapAddress', $_POST['mapAddress']);
		$stmt->bindValue(':state', $_POST['state']);
		$stmt->bindValue(':zip_code', $_POST['zip_code']);
		$stmt->bindValue(':email', $_POST['email']);
		$stmt->bindValue(':password', $_POST['password']);
		$stmt->bindValue(':emergency_first_name', $_POST['emergency_first_name']);
		$stmt->bindValue(':emergency_last_name', $_POST['emergency_last_name']);
		$stmt->bindValue(':emergency_phone',$_POST['emergency_phone']);
		$stmt->bindValue(':timeStamp', $date->format('Y-m-d H:i:s') );
		$stmt->bindValue(':active', 1);


if($stmt->execute()){
	$MAILCHIMP->addEmailToList($u_id);
}


//Add User Details to PayPal
//Disabled - Fetch Details on patient_timeline.php or patient_list.php

//  	$paypalStmt = $db->prepare("INSERT INTO paypal (paypal_id,
//								        user_id,
//								        paypal_email,
//								        paypal_password) 
//								
//								  VALUES (:paypal_id,
//								        :user_id,
//								        :paypal_email,
//								        :paypal_password)  "); 					
    
//		$paypalStmt->bindValue(':paypal_id', uniqid() );
//		$paypalStmt->bindValue(':user_id', $u_id ); 
//		$paypalStmt->bindValue(':paypal_email', $_POST['paypal_email']);
//		$paypalStmt->bindValue(':paypal_password', NULL);

//		$paypalStmt->execute();


// Second Execute if PayPal Enabled
// $stmt->execute();





header("Location:thankyou.html");
                    exit;

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}


   
   


?>




<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Sign Up to Ride With MediCoupe Today | MediCoupe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    <script src="assets/lib/jquery.js"></script>
	<script src="assets/dist/jquery.validate.js"></script>
    
    
    
    
    
    
    <script>
	$.validator.setDefaults({
		submitHandler: function() {
			alert("submitted!");
		}
	});

	$().ready(function() {
		// validate the comment form when it is submitted
		$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#patientSignUp").validate({
			rules: {
				first_name: "required",
				last_name: "required",
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				topic: {
					required: "#newsletter:checked",
					minlength: 2
				},
				agree: "required"
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			}
		});

		
	});
	</script>
        
    

 <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
    
    
    
    
        
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>
    
    

    <style>
    
/* here you can put your own css to customize and override the theme */


.cta {
  z-index: 9999;
  background-color: white;
  position: absolute;
  width: 100%;
  height: 60px;
  top:0px;
}


.cta a {
  text-shadow: none !important;
  color: #bcbcbc;
  transition: color 0.1s linear 0s, background-color 0.1s linear 0s, opacity 0.2s linear 0s !important;
}

.cta a:focus,
.cta a:hover,
.cta a:active {
  color: #28a2ae;
}


.cta a,
.cta a:focus,
.cta a:hover,
.cta a:active {
  outline: 0 !important;
  text-decoration: none;
}

    </style>
    
    
    
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
    
    <style>
    
    .font-raleway { 
    	font-family: 'Raleway', sans-serif; 
    }
    
     .font-raleway-bold { 
    	font-family: 'Raleway', sans-serif; 
    	font-weight: 800;
    }

    </style>
    
    
    
  </head>
  
  
  
  
  
  
  <body class="fixed-header   "  onload="initialize()">
  
  
  <!-- Header BEGIN -->
  <div class="cta">
    <div class="container">
      <div class="row m-t-10">
      
    <a href="index.html">
<img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="40" alt="MediCoupe"> &nbsp; <img style="position: inline-block;" src="assets/img/beta.png" height="30px">
    </a>
      
      
              
      </div>
    </div>
  </div>
  <!-- Header END -->
  
  <div class="row m-t-20 p-t-50">
  
    
            <div class="col-sm-2">
            </div>
            <div class="col-sm-4 m-t-50">
            <img src="assets/img/DriverMap.png" alt="logo" data-src="assets/img/DriverMap.png" data-src-retina="assets/img/DriverMap.png" min-width="100" width="400">
            </div>

			<div class="col-sm-5 m-t-50">
			
			 <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="50">
            <h2>Sign Up to Ride with MediCoupe</h2>
            
            <p class="font-raleway fs-16">Schedule rides same-day or up to a year in advance with MediCoupe. To get started fill out the form below or call us at 855-MY-COUPE.</p>
          </div>
         
	
   </div>
  
    <div class="register-container full-height sm-p-t-30 p-t-10">
      <div class="container-sm-height full-height">
        <div class="row row-sm-height">
          <div class="col-sm-12 col-sm-height col-middle p-t-10">
         
            
            
            <br>
            
            <form id="patientSignUp" class="p-t-15" role="form" action="register.php" method="post">
                  	<input type="hidden" name="patient_signup" id="patient_signup" value="1"/>



				  	<div class="row m-t-10">
			<h3>Account</h3>
				  	</div>
			
			
			   
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default required">
                    <label>E-Mail</label>
                    <input type="email" id="email" name="email" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default required">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default required">
                    <label for="password">Confirm Password</label>
                    <input type="password" id="confirm_password" name="confirm_password" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>

              
              
             <div class="row m-t-10"> 
              <h3>Profile</h3>
             </div>
              
              
              
                 <div class="row">
                               
                               
                               
                <div class="col-sm-6">
                  <div class="form-group form-group-default required">
                    <label>First Name</label>
                    <input type="text" id="first_name" name="first_name" placeholder="" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default required">
                    <label>Last Name</label>
                    <input type="text" id="last_name" name="last_name" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
             
             
             
               <div class="row hide">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Gender</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Male" name="gender" id="male">
                            <label for="male">Male</label>
                            <input type="radio" checked="checked" value="Female" name="gender" id="female">
                            <label for="female">Female</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default required">
                      <label>Phone</label>
                      <input type="text" id="phone" name="phone" class="form-control" required>
                    </div>
                
             
                </div>
              </div>
              
   

              
                  <div class="row">
                <div class="col-sm-12">
                  
                  
                  <div class="form-group form-group-default required" id="locationField">
                  <label>Address</label>
      <input class="form-control" id="autocomplete" name="mapAddress" placeholder=""
             onFocus="geolocate()" type="text"></input>
    </div>

    <table id="address" class="hide">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality" 
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" name="state" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" name="zip_code" id="postal_code"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>

                  
                  
                </div>
              </div>
              
    

              
              
              <div class="row m-t-10"> 
              <h3>Emergency</h3>
              </div>
              
              
              <div class="row">
                 <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Emergency First Name</label>
                    <input type="text" name="emergency_first_name" placeholder="" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Emergency Last Name</label>
                    <input type="text" name="emergency_last_name" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
              
         <div class="row">
                <div class="col-sm-12">
                  
                    <div class="form-group form-group-default">
                      <label>Emergency Phone</label>
                      <input type="text" id="emergency_phone" name="emergency_phone" class="form-control">
                    </div>
                </div>
              </div>
              
 
 
 
 
  <div class="row m-t-10 hide"> 
              <h3>Payment</h3>
              </div>
              
              
              <div class="row hide">
                 <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>PayPal E-Mail Address</label>
                    <input type="text" name="paypal_email" placeholder="" class="form-control">
                  </div>
                </div>
                
               
              </div>
              
              
              
         <div class="row hide">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>PayPal Password</label>
                    <input type="text" name="" class="form-control" placeholder="">
                  </div>
                </div>
              </div>
              

 
 
 
 
 
 
 
 
 
              
            <div class="row m-t-30">
              <button class="btn btn-success btn-block m-t-30 m-b-30" type="submit">Create Account</button>
            </div>
              
                <div class="row m-t-10">
                <div class="col-md-12">
                  <p>By clicking "Create Account", you agree to MediCoupe's <a href="terms.html" class="text-success small">Terms and Conditions</a>.</p>
                </div>
               
              </div>
              
                         <div class="col-md-12 text-right">
                  Help? <a data-toggle="modal" href="#modalSlideUpSmall" class="text-success small">Contact Support</a>
                </div>
            </form>
            
            
            
            
            
             <!-- MODAL SLIDE UP SMALL  -->
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="modalSlideUpSmall" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-body text-center m-t-20">
              <h4 class="no-margin p-b-10">Contact MediCoupe Support</h4>
              <p>Call to start or finish your registration, or ask a question.</p>
              
              <a href="tel:+18556926873" class="btn btn-info btn-block m-t-25 bold">1-855-MYCOUPE | 1-855-692-6873</a>
                          <div class="p-t-5"><small><em>Available 24 hours a day, 7 days a week.</em></small></div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
            
            
            
            
            
            
            
            
          </div>
        </div>
      </div>
      
      
      
          
    <div class="">
      <div class="register-container m-b-10 clearfix">
        <div class="inline pull-left">
          
        </div>
        <div class="col-md-10 m-t-15">
          <p class="hinted-text small inline "></p>
        </div>
      </div>
    </div>
      
      
    </div>
    

    
    
        <!-- BEGIN VENDOR JS -->
   <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
       <script src="assets/js/form_elements.js" type="text/javascript"></script>
    <script src="assets/js/form_layouts.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <!--<script>
    $(function()
    {
      $('#form-register').validate()
    })
    </script>-->
  </body>
</html>


