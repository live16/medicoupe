<?php

//database connection info db createAccount
include 'connect.php';

if(isset($_POST['driver_signup']))
{
   try {
        $stmt = $db->prepare("INSERT INTO user (user_id,
								        user_type,
								        first_name,
								        last_name,
								        phone,
								        mapAddress,
								        state,
										zip_code,
										email,
										password,
										emergency_first_name,
										emergency_last_name,
										emergency_phone,
										timeStamp,
										active) 
								
								  VALUES (:user_id,
										  :user_type,
										  :first_name,
										  :last_name,
										  :phone,
										  :mapAddress,
										  :state,
										  :zip_code,
										  :email,
										  :password,
										  :emergency_first_name,
										  :emergency_last_name,
										  :emergency_phone,
										  :timeStamp,
										  :active)"); 
						
		$date = new DateTime();
		$u_id = uniqid();

		
								       
        
		$stmt->bindValue(':user_id', $u_id );
		$stmt->bindValue(':user_type', 3); //USER_TYPE = DRIVER
		$stmt->bindValue(':first_name', $_POST['first_name']);
		$stmt->bindValue(':last_name', $_POST['last_name']);
		$stmt->bindValue(':phone', preg_replace("|[^\d]+|","",$_POST['phone']));
	
		$stmt->bindValue(':mapAddress', $_POST['mapAddress']);
	
		$stmt->bindValue(':state', $_POST['state']);
		$stmt->bindValue(':zip_code', $_POST['zip_code']);
		$stmt->bindValue(':email', $_POST['email']);
		$stmt->bindValue(':password', $_POST['password']);
		$stmt->bindValue(':emergency_first_name', $_POST['emergency_first_name']);
		$stmt->bindValue(':emergency_last_name', $_POST['emergency_last_name']);
		$stmt->bindValue(':emergency_phone', preg_replace("|[^\d]+|","",$_POST['emergency_phone']));
		$stmt->bindValue(':timeStamp', $date->format('Y-m-d H:i:s') );
		$stmt->bindValue(':active', 1);


if($stmt->execute()){
	$MAILCHIMP->addEmailToList($u_id);
}








        $driverStmt = $db->prepare("INSERT INTO driver_details (driver_details_id,
								        user_id,
								        company_id,
								        driver_account_type,
								        hometown_zip,
								        vehicle_type) 
								
								  VALUES (:driver_details_id,
								        :user_id,
								        :company_id,
								        :driver_account_type,
								        :hometown_zip,
								        :vehicle_type) "); 
						
			
								       
        
		$driverStmt->bindValue(':driver_details_id', uniqid() );
		$driverStmt->bindValue(':user_id', $u_id ); 
		$driverStmt->bindValue(':company_id', $_POST['company_id']);
		$driverStmt->bindValue(':driver_account_type', $_POST['driver_account_type']);
		$driverStmt->bindValue(':hometown_zip', NULL );
		$driverStmt->bindValue(':vehicle_type', $_POST['vehicle_type']);
	

$driverStmt->execute();

$stmt->execute();

header("Location:login.php");
                    exit;

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}




}






   $getCompanies = $db->prepare("SELECT * FROM companies");
   $getCompanies->execute();
   
   


?>




<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Sign Up to Drive With MediCoupe Today | MediCoupe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    <script src="assets/lib/jquery.js"></script>
	<script src="assets/dist/jquery.validate.js"></script>
    
    
    
    
    
    
    <script>
	$.validator.setDefaults({
		submitHandler: function() {
			alert("submitted!");
		}
	});

	$().ready(function() {
		// validate the comment form when it is submitted
		$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#driverSignUp").validate({
			rules: {
				first_name: "required",
				last_name: "required",
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				topic: {
					required: "#newsletter:checked",
					minlength: 2
				},
				agree: "required"
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			}
		});

		
	});
	</script>
    
    
    
          <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete_pu, autocomplete_des;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  street_number_des: 'short_name',
  route_des: 'long_name',
  locality_des: 'long_name',
  administrative_area_level_1_des: 'short_name',
  country_des: 'long_name',
  postal_code_des: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete_pu = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_pu')),
      { types: ['geocode'] });
  autocomplete_des = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete_des')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete_pu, 'place_changed', function() {
    fillInAddress();
  });
    google.maps.event.addListener(autocomplete_des, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete_pu.getPlace();
  var place = autocomplete_des.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete_pu.setBounds(circle.getBounds());
      autocomplete_des.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
    
    
    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>

    
    

    <style>
    
/* here you can put your own css to customize and override the theme */


.cta {
  z-index: 9999;
  background-color: white;
  position: absolute;
  width: 100%;
  height: 60px;
  top:0px;
}


.cta a {
  text-shadow: none !important;
  color: #bcbcbc;
  transition: color 0.1s linear 0s, background-color 0.1s linear 0s, opacity 0.2s linear 0s !important;
}

.cta a:focus,
.cta a:hover,
.cta a:active {
  color: #28a2ae;
}


.cta a,
.cta a:focus,
.cta a:hover,
.cta a:active {
  outline: 0 !important;
  text-decoration: none;
}

    </style>
    
    
    
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
    
    <style>
    
    .font-raleway { 
    	font-family: 'Raleway', sans-serif; 
    }
    
     .font-raleway-bold { 
    	font-family: 'Raleway', sans-serif; 
    	font-weight: 800;
    }

    </style>
    
    
    
    
    
    
    
  </head>
  
  
  
  
  
  
  <body class="fixed-header   "  onload="initialize()">
  
  
  <!-- Header BEGIN -->
  <div class="cta">
    <div class="container">
      <div class="row m-t-10">
      
    <a href="index.html">
<img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="40">
    </a>
      
      
              
      </div>
    </div>
  </div>
  <!-- Header END -->
  
  <div class="row m-t-20 p-t-50">
  
    
            <div class="col-sm-2">
            </div>
            <div class="col-sm-4 m-t-50">
            <img src="assets/img/DriverMap.png" alt="logo" data-src="assets/img/DriverMap.png" data-src-retina="assets/img/DriverMap.png" min-width="100" width="400">
            </div>

			<div class="col-sm-5 m-t-50">
			
			 <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="50">
            <h2>Sign Up to Drive with MediCoupe</h2>
            
            <p class="font-raleway fs-16">
              
        Not just a job, a career path. Enjoy flexible hours, mentorship programs and opportunities for career advancement. Fill out the form below to start the application process!
            </p>
          </div>
         
	
   </div>
  
    <div class="register-container full-height sm-p-t-30 p-t-10">
      <div class="container-sm-height full-height">
        <div class="row row-sm-height">
          <div class="col-sm-12 col-sm-height col-middle p-t-10">
         
            
            
            <br>
            
            <form id="driverSignUp" class="p-t-15" role="form" action="driver_register.php" method="post">
                  	<input type="hidden" name="driver_signup" id="driver_signup" value="1"/>
                  	
                  	
                  	
                  	<div class="row m-t-30">
              <a class="btn btn-info btn-block m-t-30 m-b-30" href="login.php">Already Have An Account? <span class="bold">Login</span></a>
            </div>





				  	<div class="row m-t-10">
			<h3>Account</h3>
				  	</div>
			
			
			   
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>E-Mail</label>
                    <input type="email" id="email" name="email" placeholder="" class="form-control" >
                  </div>
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" placeholder="" class="form-control">
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label for="password">Confirm Password</label>
                    <input type="password" id="confirm_password" name="confirm_password" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>

              
              
             <div class="row m-t-10"> 
              <h3>Profile</h3>
             </div>
              
              
              
                 <div class="row">
                               
                               
                               
                <div class="col-sm-6">
                  <div class="form-group form-group-default required">
                    <label>First Name</label>
                    <input type="text" id="first_name" name="first_name" placeholder="" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Last Name</label>
                    <input type="text" id="last_name" name="last_name" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
             
             
             
               <div class="row hide">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Gender</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" value="Male" name="gender" id="male">
                            <label for="male">Male</label>
                            <input type="radio" checked="checked" value="Female" name="gender" id="female">
                            <label for="female">Female</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>

              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                      <label>Phone</label>
                      
                      <input type="text" name="phone" id="phone" class="form-control">
                    </div>
                </div>
              </div>
              
              <div class="row">
              <div class="col-sm-12">
                <div class="form-group form-group-default">
                  <label>Address</label>
                  
                  <input id="autocomplete_pu" class="form-control" name="mapAddress" placeholder="Start typing your address.. "
             onFocus="geolocate()" type="text"></input>
                <table id="address" hidden>
                  <tr>
                    <td class="label">Street address</td>
                    <td class="slimField"><input class="field" name="address1" id="street_number"
                          disabled="true"></input></td>
                    <td class="wideField" colspan="2"><input class="field" id="route"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">City</td>
                    <td class="wideField" colspan="3"><input class="field" name="city" id="city"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field"
                          name="state" id="state" disabled="true"></input></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field" name="zip_code" id="postal_code"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">Country</td>
                    <td class="wideField" colspan="3"><input class="field"
                          id="country" disabled="true"></input></td>
                  </tr>
                </table>
             	
                </div>
                </div>
                </div>

              
              
              
              <div class="row m-t-10"> 
              <h3>Emergency</h3>
              </div>
              
              
              <div class="row">
                 <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Emergency First Name</label>
                    <input type="text" name="emergency_first_name" placeholder="" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Emergency Last Name</label>
                    <input type="text" name="emergency_last_name" placeholder="" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
              
         <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                     <label>Emergency Phone</label>
                      
                      <input type="text" name="emergency_phone" id="emergency_phone" class="form-control">
                  </div>
                </div>
              </div>
              
 
 
 
 
  <div class="row m-t-10 hide"> 
              <h3>Payment</h3>
              </div>
              
              
              <div class="row hide">
                 <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>PayPal Username</label>
                    <input type="text" name="" placeholder="" class="form-control">
                  </div>
                </div>
                
               
              </div>
              
              
              
         <div class="row hide">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>PayPal Password</label>
                    <input type="text" name="" class="form-control" placeholder="">
                  </div>
                </div>
              </div>
              

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  <div class="row m-t-10"> 
              <h3>Driver Details</h3>
              </div>
              
              
                <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Driver Type</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" checked="checked" value="Individual" name="driver_account_type" id="individual">
                            <label for="individual">Individual</label>
                            <input type="radio" value="Company" name="driver_account_type" id="company">
                            <label for="company">Company</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              
              
              
              
              
              
              <div class="row">
              <div class="col-sm-12">
               <div class="form-group form-group-default p-b-10">        
               
                 <label class="p-b-10">What Company Do you work for?</label>
                 
                 
                    <select class="cs-select cs-skin-slide p-l-25 padding-10" data-init-plugin="cs-select" name="company_id" >
                    
                    
                   <option value="NONE" name="company_id">
                    
                    <span>I am a solo driver.</span>
                    
                    </option>

                    
                    
                             <?php while ($row = $getCompanies->fetch(PDO::FETCH_ASSOC)) : ?>
                             
                             
                             
                                                  
                                                   <option value="<? echo $row['company_id'] ;  ?>" name="company_id">
                                                   
                                                   		                                                   
                                                   		<span name="company_id"><? echo $row['company_name'] ;  ?> (<em><? echo $row['company_id'] ;  ?></em>)</span>
                                                   
                                                   </option>
                     
							<?php endwhile; ?>
							
							
							
                      
                      
                    </select>                          
            </div>
              </div>
            </div>
            


		
		
		
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                  <label>Vehicle Type</label>
                       <div>
                          <div class="radio radio-success">
                            <input type="radio" checked="checked" value="Black Car" name="vehicle_type" id="black_car">
                            <label for="black_car">Car</label>
                            <input type="radio"  value="Ambulette" name="vehicle_type" id="ambulette">
                            <label for="ambulette">Ambulette</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>

		
		
		
		
		
		<div class="row">
		  <div class="col-md-12">
		              <div class="form-group form-group-default" id="locationField">
		               <label>Hometown</label>
      
					   <input class="form-control" id="autocomplete_des" placeholder="Enter your address"
					   onFocus="geolocate()" type="text" name="hometown_zip"></input>
					   </div>

						    <table id="address" class="hide">
						      <tr>
						        <td class="label">Street address</td>
						        <td class="slimField"><input class="field" id="street_number"
						              disabled="true"></input></td>
						        <td class="wideField" colspan="2"><input class="field" id="route"
						              disabled="true"></input></td>
						      </tr>
						      <tr>
						        <td class="label">City</td>
						        <td class="wideField" colspan="3"><input class="field" id="locality"
						              disabled="true"></input></td>
						      </tr>
						      <tr>
						        <td class="label">State</td>
						        <td class="slimField"><input class="field"
						              id="administrative_area_level_1" disabled="true" name="state"></input></td>
						        <td class="label">Zip code</td>
						        <td class="wideField"><input class="field" id="postal_code"
						              disabled="true" name="zip_code"></input></td>
						      </tr>
						      <tr>
						        <td class="label">Country</td>
						        <td class="wideField" colspan="3"><input class="field"
						              id="country" disabled="true"></input></td>
						      </tr>
						    </table>
   </div>
</div>
		
		            
		            
		            
		            
		          
		              
		              
		              
		              
		              
		                           
             
             

 
 
 
 
 
 
 
 
 
 
 
 
 
 
              
            <div class="row m-t-30">
              <button class="btn btn-success btn-block m-t-30 m-b-30" type="submit">Create Account</button>
            </div>
              
                <div class="row m-t-10">
                <div class="col-md-12">
                  <p>By clicking "Create Account", you agree to MediCoupe's <a href="terms.html" class="text-success small">Terms and Conditions</a>.</p>
                </div>
               
              </div>
              
                         <div class="col-md-12 text-right">
                  Help? <a data-toggle="modal" href="#modalSlideUpSmall" class="text-success small">Contact Support</a>
                </div>
            </form>
            
            
            
            
            
             <!-- MODAL SLIDE UP SMALL  -->
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="modalSlideUpSmall" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-body text-center m-t-20">
              <h4 class="no-margin p-b-10">Contact MediCoupe Support</h4>
              <p>Call to start or finish your registration, or ask a question.</p>
              
              <a href="tel:+18556926873" class="btn btn-info btn-block m-t-25 bold">1-855-MYCOUPE | 1-855-692-6873</a>
                          <div class="p-t-5"><small><em>Available 24 hours a day, 7 days a week.</em></small></div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
            
            
            
            
            
            
            
            
          </div>
        </div>
      </div>
      
      
      
          
    <div class="">
      <div class="register-container m-b-10 clearfix">
        <div class="inline pull-left">
          
        </div>
        <div class="col-md-10 m-t-15">
          <p class="hinted-text small inline "></p>
        </div>
      </div>
    </div>
      
      
    </div>
    

    
    
        <!-- BEGIN VENDOR JS -->
   <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
     <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
     <script src="assets/js/form_elements.js" type="text/javascript"></script>
    <script src="assets/js/form_layouts.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <!--<script>
    $(function()
    {
      $('#form-register').validate()
    })
    </script>-->
  </body>
</html>


