<?php
session_start();

//$_SESSION["user_id"] = $row['user_id'];
//$_SESSION["first_name"] = $row['first_name'];
//$_SESSION["last_name"] = $row['last_name'];


//database connection info db createAccount
include 'connect.php';

if(isset($_POST['addCompany']))
{
   try {
        $stmt = $db->prepare("INSERT INTO companies (company_id,company_type,company_name,company_phone,company_address1,company_address2,
								company_city,company_state,company_zip) 
					  VALUES (:company_id,:company_type,:company_name,:company_phone,:company_address1,:company_address2,
								:company_city,:company_state,:company_zip)"); 
						
			
		
								       
        $companyID = uniqid();
        
        
		$stmt->bindValue(':company_id', $companyID );
		$stmt->bindValue(':company_type', 'Driver'); //USER_TYPE = DRIVER
		$stmt->bindValue(':company_name', $_POST['company_name']);
		$stmt->bindValue(':company_phone', $_POST['company_phone']);
		$stmt->bindValue(':company_address1', NULL);
		$stmt->bindValue(':company_address2', NULL);
		$stmt->bindValue(':company_city', $_POST['city']);
		$stmt->bindValue(':company_state', NULL);
		$stmt->bindValue(':company_zip', NULL);
		

$stmt->execute();

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}
   
   
   
// Select Everything From the Users Table
	   $getUsers = $db->prepare("SELECT * FROM user");
	   $getUsers->execute();
   
  
//Appointments – UNCONFIRMED (), ordered by date/time, soonest to furthest
//
//
   
   
//Appointments – UNASSIGNED DRIVER, Build into UI ABILITY TO ordered by date/time, soonest to furthest
	   $getUnassignedAppointments = $db->prepare("SELECT * FROM appointments WHERE driver_user_id is NULL");
	   $getUnassignedAppointments->execute();
   
   
	   $getDrivers = $db->prepare("SELECT * FROM user WHERE user_type = 3");
	   $getDrivers->execute();
	   
	   
	   
	   
	   
	 	   

?>













<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    
    <title>Admin Page</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    
    
    
    
    
    
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    
    
    
    
    
    
    
    
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if lt IE 9]>
            <link href="assets/plugins/mapplic/css/mapplic-ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    
    
    
    
    
    
    
    
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
    
    
    
    
    
    
    
    
     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>

    
<style>
    div.pac-container {
   z-index: 1050 !important;
}
</style>

    
    
    
    
  </head>
  
  
  
  
  <body class="fixed-header  dashboard " onload="initialize()">
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  
   <!-------------------------------------------------- CANCELLED APPT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up full  disable-scroll" id="cancelledApptModal" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Cancelled Appointments</h2>
              <p class="p-b-10">Cancellations Listed Below</p>
            </div>
            
            
            <div class="modal-body">
  
	
            
          
           <div id="rootwizard" class="m-t-50">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
                <li class="active">
                  <a data-toggle="tab" href="#tab1"><i class="fa fa-shopping-cart tab-icon"></i> <span>Your cart</span></a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#tab2"><i class="fa fa-truck tab-icon"></i> <span>Shipping information</span></a>
                </li>
                <li class="hide">
                  <a data-toggle="tab" href="#tab3"><i class="fa fa-credit-card tab-icon"></i> <span>Payment details</span></a>
                </li>
                <li class="hide">
                  <a data-toggle="tab" href="#tab4"><i class="fa fa-check tab-icon"></i> <span>Summary</span></a>
                </li>
              </ul>
              
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane padding-20 active slide-left" id="tab1">
                  <div class="row row-same-height">
                    <div class="col-md-5 b-r b-dashed b-grey sm-b-b">
                      <div class="padding-30 m-t-50">
                        <i class="fa fa-shopping-cart fa-2x hint-text"></i>
                        <h2>Your Bags are ready to check out!</h2>
                        <p>Discover goods you'll love from brands that inspire. The easiest way to open your own online store. Discover amazing stuff or open your own store for free!</p>
                        <p class="small hint-text">Below is a sample page for your cart , Created using pages design UI Elementes</p>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="padding-30">
                        
                        <table class="table table-condensed">
                          <tr>
                            <td class="col-lg-8 col-md-6 col-sm-7 ">
                              <a href="#" class="remove-item"><i class="pg-close"></i></a>
                              <span class="m-l-10 font-montserrat fs-18 all-caps">Webarch UI Framework</span>
                              <span class="m-l-10 ">Dashboard UI Pack</span>
                            </td>
                            <td class=" col-lg-2 col-md-3 col-sm-3 text-right">
                              <span>Qty 1</span>
                            </td>
                            <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
                              <h4 class="text-primary no-margin font-montserrat">$27</h4>
                            </td>
                          </tr>
                          <tr>
                            <td class="col-lg-8 col-md-6 col-sm-7">
                              <a href="#" class="remove-item"><i class="pg-close"></i></a>
                              <span class="m-l-10 font-montserrat fs-18 all-caps">Pages UI Framework</span>
                              <span class="m-l-10 ">Next Gen UI Pack</span>
                            </td>
                            <td class="col-lg-2 col-md-3 col-sm-3 text-right">
                              <span>Qty 1</span>
                            </td>
                            <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
                              <h4 class="text-primary no-margin font-montserrat">$27</h4>
                            </td>
                          </tr>
                        </table>
                        
                        <h5>Donation</h5>
                        <div class="row">
                          <div class="col-lg-7 col-md-6">
                            <p class="no-margin">Donate now and give clean, safe water to those in need. </p>
                            <p class="small hint-text">
                              100% of your donation goes to the field, and you can track the progress of every dollar spent. <a href="#">Click Here</a>
                            </p>
                          </div>
                          <div class="col-lg-5 col-md-6">
                            <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-default active">
                                <input type="radio" name="options" id="option1" checked> <span class="fs-16">$0</span>
                              </label>
                              <label class="btn btn-default">
                                <input type="radio" name="options" id="option2"> <span class="fs-16">$10</span>
                              </label>
                              <label class="btn btn-default">
                                <input type="radio" name="options" id="option3"> <span class="fs-16">$20</span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <br>
                        <div class="container-sm-height">
                          <div class="row row-sm-height b-a b-grey">
                            <div class="col-sm-3 col-sm-height col-middle p-l-10 sm-padding-15">
                              <h5 class="font-montserrat all-caps small no-margin hint-text bold">Discount (10%)</h5>
                              <p class="no-margin">$10</p>
                            </div>
                            <div class="col-sm-7 col-sm-height col-middle sm-padding-15 ">
                              <h5 class="font-montserrat all-caps small no-margin hint-text bold">Donations</h5>
                              <p class="no-margin">$0</p>
                            </div>
                            <div class="col-sm-2 text-right bg-primary col-sm-height col-middle padding-10">
                              <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
                              <h4 class="no-margin text-white">$44</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
               
               
                <div class="tab-pane slide-left padding-20" id="tab2">
                  <div class="row row-same-height">
                    <div class="col-md-5 b-r b-dashed b-grey ">
                      <div class="padding-30 m-t-50">
                        <h2>Your Information is safe with us!</h2>
                        <p>We respect your privacy and protect it with strong encryption, plus strict policies . Two-step verification, which we encourage all our customers to use.</p>
                        <p class="small hint-text">Below is a sample page for your cart , Created using pages design UI Elementes</p>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="padding-30">
                        
                        <form role="form">
                          <p>Name and Email Address</p>
                          <div class="form-group-attached">
                            <div class="row clearfix">
                              <div class="col-sm-6">
                                <div class="form-group form-group-default required">
                                  <label>First name</label>
                                  <input type="text" class="form-control" required>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                  <label>Last name</label>
                                  <input type="text" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="form-group form-group-default required">
                              <label>Email</label>
                              <input type="text" class="form-control" required>
                            </div>
                          </div>
                          <br>
                          <p>Billing Address</p>
                          <div class="form-group-attached">
                            <div class="form-group form-group-default required">
                              <label>Address</label>
                              <input type="text" class="form-control" placeholder="Current address" required>
                            </div>
                            <div class="row clearfix">
                              <div class="col-sm-6">
                                <div class="form-group form-group-default required form-group-default-selectFx">
                                  <label>Country</label>
                                  <select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select">
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Åland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia, Plurinational State of</option>
                                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                    <option value="BA">Bosnia and Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, the Democratic Republic of the</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">Côte d'Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">Curaçao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard Island and McDonald Islands</option>
                                    <option value="VA">Holy See (Vatican City State)</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran, Islamic Republic of</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                    <option value="KR">Korea, Republic of</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macao</option>
                                    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States of</option>
                                    <option value="MD">Moldova, Republic of</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territory, Occupied</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Réunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="BL">Saint Barthélemy</option>
                                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                    <option value="KN">Saint Kitts and Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="MF">Saint Martin (French part)</option>
                                    <option value="PM">Saint Pierre and Miquelon</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome and Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SX">Sint Maarten (Dutch part)</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan, Province of China</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania, United Republic of</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Minor Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands, British</option>
                                    <option value="VI">Virgin Islands, U.S.</option>
                                    <option value="WF">Wallis and Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                  <label>City</label>
                                  <input type="text" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="row clearfix">
                              <div class="col-sm-9">
                                <div class="form-group form-group-default required">
                                  <label>State/Province</label>
                                  <input type="text" class="form-control" placeholder="Outside US/Canada" required>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                  <label>Zip code</label>
                                  <input type="text" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="form-group form-group-default input-group">
                              <span class="input-group-addon">
                                            <select class="cs-select cs-skin-slide cs-transparent" data-init-plugin="cs-select">
                                            <option data-countryCode="GB" value="44" Selected>UK (+44)</option>
                                            <option data-countryCode="US" value="1">USA (+1)</option>
                                            <option data-countryCode="DZ" value="213">Algeria (+213)</option>
                                            <option data-countryCode="AD" value="376">Andorra (+376)</option>
                                            <option data-countryCode="AO" value="244">Angola (+244)</option>
                                            <option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
                                            <option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
                                            <option data-countryCode="AR" value="54">Argentina (+54)</option>
                                            <option data-countryCode="AM" value="374">Armenia (+374)</option>
                                            <option data-countryCode="AW" value="297">Aruba (+297)</option>
                                            <option data-countryCode="AU" value="61">Australia (+61)</option>
                                            <option data-countryCode="AT" value="43">Austria (+43)</option>
                                            <option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
                                            <option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
                                            <option data-countryCode="BH" value="973">Bahrain (+973)</option>
                                            <option data-countryCode="BD" value="880">Bangladesh (+880)</option>
                                            <option data-countryCode="BB" value="1246">Barbados (+1246)</option>
                                            <option data-countryCode="BY" value="375">Belarus (+375)</option>
                                            <option data-countryCode="BE" value="32">Belgium (+32)</option>
                                            <option data-countryCode="BZ" value="501">Belize (+501)</option>
                                            <option data-countryCode="BJ" value="229">Benin (+229)</option>
                                            <option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
                                            <option data-countryCode="BT" value="975">Bhutan (+975)</option>
                                            <option data-countryCode="BO" value="591">Bolivia (+591)</option>
                                            <option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
                                            <option data-countryCode="BW" value="267">Botswana (+267)</option>
                                            <option data-countryCode="BR" value="55">Brazil (+55)</option>
                                            <option data-countryCode="BN" value="673">Brunei (+673)</option>
                                            <option data-countryCode="BG" value="359">Bulgaria (+359)</option>
                                            <option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
                                            <option data-countryCode="BI" value="257">Burundi (+257)</option>
                                            <option data-countryCode="KH" value="855">Cambodia (+855)</option>
                                            <option data-countryCode="CM" value="237">Cameroon (+237)</option>
                                            <option data-countryCode="CA" value="1">Canada (+1)</option>
                                            <option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
                                            <option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
                                            <option data-countryCode="CF" value="236">Central African Republic (+236)</option>
                                            <option data-countryCode="CL" value="56">Chile (+56)</option>
                                            <option data-countryCode="CN" value="86">China (+86)</option>
                                            <option data-countryCode="CO" value="57">Colombia (+57)</option>
                                            <option data-countryCode="KM" value="269">Comoros (+269)</option>
                                            <option data-countryCode="CG" value="242">Congo (+242)</option>
                                            <option data-countryCode="CK" value="682">Cook Islands (+682)</option>
                                            <option data-countryCode="CR" value="506">Costa Rica (+506)</option>
                                            <option data-countryCode="HR" value="385">Croatia (+385)</option>
                                            <option data-countryCode="CU" value="53">Cuba (+53)</option>
                                            <option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
                                            <option data-countryCode="CY" value="357">Cyprus South (+357)</option>
                                            <option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
                                            <option data-countryCode="DK" value="45">Denmark (+45)</option>
                                            <option data-countryCode="DJ" value="253">Djibouti (+253)</option>
                                            <option data-countryCode="DM" value="1809">Dominica (+1809)</option>
                                            <option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
                                            <option data-countryCode="EC" value="593">Ecuador (+593)</option>
                                            <option data-countryCode="EG" value="20">Egypt (+20)</option>
                                            <option data-countryCode="SV" value="503">El Salvador (+503)</option>
                                            <option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
                                            <option data-countryCode="ER" value="291">Eritrea (+291)</option>
                                            <option data-countryCode="EE" value="372">Estonia (+372)</option>
                                            <option data-countryCode="ET" value="251">Ethiopia (+251)</option>
                                            <option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
                                            <option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
                                            <option data-countryCode="FJ" value="679">Fiji (+679)</option>
                                            <option data-countryCode="FI" value="358">Finland (+358)</option>
                                            <option data-countryCode="FR" value="33">France (+33)</option>
                                            <option data-countryCode="GF" value="594">French Guiana (+594)</option>
                                            <option data-countryCode="PF" value="689">French Polynesia (+689)</option>
                                            <option data-countryCode="GA" value="241">Gabon (+241)</option>
                                            <option data-countryCode="GM" value="220">Gambia (+220)</option>
                                            <option data-countryCode="GE" value="7880">Georgia (+7880)</option>
                                            <option data-countryCode="DE" value="49">Germany (+49)</option>
                                            <option data-countryCode="GH" value="233">Ghana (+233)</option>
                                            <option data-countryCode="GI" value="350">Gibraltar (+350)</option>
                                            <option data-countryCode="GR" value="30">Greece (+30)</option>
                                            <option data-countryCode="GL" value="299">Greenland (+299)</option>
                                            <option data-countryCode="GD" value="1473">Grenada (+1473)</option>
                                            <option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
                                            <option data-countryCode="GU" value="671">Guam (+671)</option>
                                            <option data-countryCode="GT" value="502">Guatemala (+502)</option>
                                            <option data-countryCode="GN" value="224">Guinea (+224)</option>
                                            <option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
                                            <option data-countryCode="GY" value="592">Guyana (+592)</option>
                                            <option data-countryCode="HT" value="509">Haiti (+509)</option>
                                            <option data-countryCode="HN" value="504">Honduras (+504)</option>
                                            <option data-countryCode="HK" value="852">Hong Kong (+852)</option>
                                            <option data-countryCode="HU" value="36">Hungary (+36)</option>
                                            <option data-countryCode="IS" value="354">Iceland (+354)</option>
                                            <option data-countryCode="IN" value="91">India (+91)</option>
                                            <option data-countryCode="ID" value="62">Indonesia (+62)</option>
                                            <option data-countryCode="IR" value="98">Iran (+98)</option>
                                            <option data-countryCode="IQ" value="964">Iraq (+964)</option>
                                            <option data-countryCode="IE" value="353">Ireland (+353)</option>
                                            <option data-countryCode="IL" value="972">Israel (+972)</option>
                                            <option data-countryCode="IT" value="39">Italy (+39)</option>
                                            <option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
                                            <option data-countryCode="JP" value="81">Japan (+81)</option>
                                            <option data-countryCode="JO" value="962">Jordan (+962)</option>
                                            <option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
                                            <option data-countryCode="KE" value="254">Kenya (+254)</option>
                                            <option data-countryCode="KI" value="686">Kiribati (+686)</option>
                                            <option data-countryCode="KP" value="850">Korea North (+850)</option>
                                            <option data-countryCode="KR" value="82">Korea South (+82)</option>
                                            <option data-countryCode="KW" value="965">Kuwait (+965)</option>
                                            <option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
                                            <option data-countryCode="LA" value="856">Laos (+856)</option>
                                            <option data-countryCode="LV" value="371">Latvia (+371)</option>
                                            <option data-countryCode="LB" value="961">Lebanon (+961)</option>
                                            <option data-countryCode="LS" value="266">Lesotho (+266)</option>
                                            <option data-countryCode="LR" value="231">Liberia (+231)</option>
                                            <option data-countryCode="LY" value="218">Libya (+218)</option>
                                            <option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
                                            <option data-countryCode="LT" value="370">Lithuania (+370)</option>
                                            <option data-countryCode="LU" value="352">Luxembourg (+352)</option>
                                            <option data-countryCode="MO" value="853">Macao (+853)</option>
                                            <option data-countryCode="MK" value="389">Macedonia (+389)</option>
                                            <option data-countryCode="MG" value="261">Madagascar (+261)</option>
                                            <option data-countryCode="MW" value="265">Malawi (+265)</option>
                                            <option data-countryCode="MY" value="60">Malaysia (+60)</option>
                                            <option data-countryCode="MV" value="960">Maldives (+960)</option>
                                            <option data-countryCode="ML" value="223">Mali (+223)</option>
                                            <option data-countryCode="MT" value="356">Malta (+356)</option>
                                            <option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
                                            <option data-countryCode="MQ" value="596">Martinique (+596)</option>
                                            <option data-countryCode="MR" value="222">Mauritania (+222)</option>
                                            <option data-countryCode="YT" value="269">Mayotte (+269)</option>
                                            <option data-countryCode="MX" value="52">Mexico (+52)</option>
                                            <option data-countryCode="FM" value="691">Micronesia (+691)</option>
                                            <option data-countryCode="MD" value="373">Moldova (+373)</option>
                                            <option data-countryCode="MC" value="377">Monaco (+377)</option>
                                            <option data-countryCode="MN" value="976">Mongolia (+976)</option>
                                            <option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
                                            <option data-countryCode="MA" value="212">Morocco (+212)</option>
                                            <option data-countryCode="MZ" value="258">Mozambique (+258)</option>
                                            <option data-countryCode="MN" value="95">Myanmar (+95)</option>
                                            <option data-countryCode="NA" value="264">Namibia (+264)</option>
                                            <option data-countryCode="NR" value="674">Nauru (+674)</option>
                                            <option data-countryCode="NP" value="977">Nepal (+977)</option>
                                            <option data-countryCode="NL" value="31">Netherlands (+31)</option>
                                            <option data-countryCode="NC" value="687">New Caledonia (+687)</option>
                                            <option data-countryCode="NZ" value="64">New Zealand (+64)</option>
                                            <option data-countryCode="NI" value="505">Nicaragua (+505)</option>
                                            <option data-countryCode="NE" value="227">Niger (+227)</option>
                                            <option data-countryCode="NG" value="234">Nigeria (+234)</option>
                                            <option data-countryCode="NU" value="683">Niue (+683)</option>
                                            <option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
                                            <option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
                                            <option data-countryCode="NO" value="47">Norway (+47)</option>
                                            <option data-countryCode="OM" value="968">Oman (+968)</option>
                                            <option data-countryCode="PW" value="680">Palau (+680)</option>
                                            <option data-countryCode="PA" value="507">Panama (+507)</option>
                                            <option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
                                            <option data-countryCode="PY" value="595">Paraguay (+595)</option>
                                            <option data-countryCode="PE" value="51">Peru (+51)</option>
                                            <option data-countryCode="PH" value="63">Philippines (+63)</option>
                                            <option data-countryCode="PL" value="48">Poland (+48)</option>
                                            <option data-countryCode="PT" value="351">Portugal (+351)</option>
                                            <option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
                                            <option data-countryCode="QA" value="974">Qatar (+974)</option>
                                            <option data-countryCode="RE" value="262">Reunion (+262)</option>
                                            <option data-countryCode="RO" value="40">Romania (+40)</option>
                                            <option data-countryCode="RU" value="7">Russia (+7)</option>
                                            <option data-countryCode="RW" value="250">Rwanda (+250)</option>
                                            <option data-countryCode="SM" value="378">San Marino (+378)</option>
                                            <option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
                                            <option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
                                            <option data-countryCode="SN" value="221">Senegal (+221)</option>
                                            <option data-countryCode="CS" value="381">Serbia (+381)</option>
                                            <option data-countryCode="SC" value="248">Seychelles (+248)</option>
                                            <option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
                                            <option data-countryCode="SG" value="65">Singapore (+65)</option>
                                            <option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
                                            <option data-countryCode="SI" value="386">Slovenia (+386)</option>
                                            <option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
                                            <option data-countryCode="SO" value="252">Somalia (+252)</option>
                                            <option data-countryCode="ZA" value="27">South Africa (+27)</option>
                                            <option data-countryCode="ES" value="34">Spain (+34)</option>
                                            <option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
                                            <option data-countryCode="SH" value="290">St. Helena (+290)</option>
                                            <option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
                                            <option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
                                            <option data-countryCode="SD" value="249">Sudan (+249)</option>
                                            <option data-countryCode="SR" value="597">Suriname (+597)</option>
                                            <option data-countryCode="SZ" value="268">Swaziland (+268)</option>
                                            <option data-countryCode="SE" value="46">Sweden (+46)</option>
                                            <option data-countryCode="CH" value="41">Switzerland (+41)</option>
                                            <option data-countryCode="SI" value="963">Syria (+963)</option>
                                            <option data-countryCode="TW" value="886">Taiwan (+886)</option>
                                            <option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
                                            <option data-countryCode="TH" value="66">Thailand (+66)</option>
                                            <option data-countryCode="TG" value="228">Togo (+228)</option>
                                            <option data-countryCode="TO" value="676">Tonga (+676)</option>
                                            <option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
                                            <option data-countryCode="TN" value="216">Tunisia (+216)</option>
                                            <option data-countryCode="TR" value="90">Turkey (+90)</option>
                                            <option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
                                            <option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
                                            <option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
                                            <option data-countryCode="TV" value="688">Tuvalu (+688)</option>
                                            <option data-countryCode="UG" value="256">Uganda (+256)</option>
                                            <!-- <option data-countryCode="GB" value="44">UK (+44)</option> -->
                                            <option data-countryCode="UA" value="380">Ukraine (+380)</option>
                                            <option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
                                            <option data-countryCode="UY" value="598">Uruguay (+598)</option>
                                            <!-- <option data-countryCode="US" value="1">USA (+1)</option> -->
                                            <option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
                                            <option data-countryCode="VU" value="678">Vanuatu (+678)</option>
                                            <option data-countryCode="VA" value="379">Vatican City (+379)</option>
                                            <option data-countryCode="VE" value="58">Venezuela (+58)</option>
                                            <option data-countryCode="VN" value="84">Vietnam (+84)</option>
                                            <option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
                                            <option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
                                            <option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
                                            <option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
                                            <option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
                                            <option data-countryCode="ZM" value="260">Zambia (+260)</option>
                                            <option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
                                        </select>
                                        </span>
                              <label>Phone number</label>
                              <input type="text" class="form-control" placeholder="For verification purpose">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
                
                
                <div class="tab-pane slide-left padding-20" id="tab3">
                  <div class="row row-same-height">
                    <div class="col-md-5 b-r b-dashed b-grey ">
                      <div class="padding-30 m-t-50">
                        <h2>We Secured Your Line</h2>
                        <p>Below is a sample page for your cart , Created using pages design UI Elementes</p>
                        <p class="small hint-text">Below is a sample page for your cart , Created using pages design UI Elementes</p>
                        <table class="table table-condensed">
                          <tr>
                            <td class=" col-md-9">
                              <span class="m-l-10 font-montserrat fs-18 all-caps">Webarch UI Framework</span>
                              <span class="m-l-10 ">Dashboard UI Pack</span>
                            </td>
                            <td class=" col-md-3 text-right">
                              <span>Qty 1</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-9">
                              <span class="m-l-10 font-montserrat fs-18 all-caps">Pages UI Framework</span>
                              <span class="m-l-10 ">Next Gen UI Pack</span>
                            </td>
                            <td class=" col-md-3 text-right">
                              <span>Qty 1</span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class=" col-md-3 text-right">
                              <h4 class="text-primary no-margin font-montserrat">$27</h4>
                            </td>
                          </tr>
                        </table>
                        <p class="small">Invoice are issued on the date of despatch. Payment terms: Pre-orders: within 10 days of invoice date with 4% discount, from the 11th to the 30th day net. Re-orders: non-reduced stock items are payable net after 20 days. </p>
                        <p class="small">By pressing Pay Now You will Agree to the Payment <a href="#">Terms &amp; Conditions</a>
                        </p>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="padding-30">
                        <ul class="list-unstyled list-inline m-l-30">
                          <li><a href="#" class="p-r-30 text-black">Credit card</a>
                          </li>
                          <li><a href="#" class="p-r-30 text-black  hint-text">PayPal</a>
                          </li>
                          <li><a href="#" class="p-r-30 text-black  hint-text">Wire transfer</a>
                          </li>
                        </ul>
                        <form role="form">
                          <div class="bg-master-light padding-30 b-rad-lg">
                            <h2 class="pull-left no-margin">Credit Card</h2>
                            <ul class="list-unstyled pull-right list-inline no-margin">
                              <li>
                                <a href="#">
                                  <img width="51" height="32" data-src-retina="assets/img/form-wizard/visa2x.png" data-src="assets/img/form-wizard/visa.png" class="brand" alt="logo" src="assets/img/form-wizard/visa.png">
                                </a>
                              </li>
                              <li>
                                <a href="#" class="hint-text">
                                  <img width="51" height="32" data-src-retina="assets/img/form-wizard/amex2x.png" data-src="assets/img/form-wizard/amex.png" class="brand" alt="logo" src="assets/img/form-wizard/amex.png">
                                </a>
                              </li>
                              <li>
                                <a href="#" class="hint-text">
                                  <img width="51" height="32" data-src-retina="assets/img/form-wizard/mastercard2x.png" data-src="assets/img/form-wizard/mastercard.png" class="brand" alt="logo" src="assets/img/form-wizard/mastercard.png">
                                </a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div class="form-group form-group-default required m-t-25">
                              <label>Card holder's name</label>
                              <input type="text" class="form-control" placeholder="Name on the card" required>
                            </div>
                            <div class="form-group form-group-default required">
                              <label>Card number</label>
                              <input type="text" class="form-control" placeholder="8888-8888-8888-8888" required>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <label>Expiration</label>
                                <br>
                                <select class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                  <option selected>Jan (01)</option>
                                  <option>Feb (02)</option>
                                  <option>Mar (03)</option>
                                  <option>Apr (04)</option>
                                  <option>May (05)</option>
                                  <option>Jun (06)</option>
                                  <option>Jul (07)</option>
                                  <option>Aug (08)</option>
                                  <option>Sep (09)</option>
                                  <option>Oct (10)</option>
                                  <option>Nov (11)</option>
                                  <option>Dec (12)</option>
                                </select>
                                <select class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                  <option value="2014">2014</option>
                                  <option value="2015">2015</option>
                                  <option value="2016">2016</option>
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                  <option value="2026">2026</option>
                                  <option value="2027">2027</option>
                                  <option value="2028">2028</option>
                                  <option value="2029">2029</option>
                                  <option value="2030">2030</option>
                                </select>
                              </div>
                              <div class="col-md-2 col-md-offset-4">
                                <div class="form-group required">
                                  <label>CVC Code</label>
                                  <input type="text" class="form-control" placeholder="000" required>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
 </div>
 
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END CANCELLED APPT MODAL ------------------------------------------------------------->
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  




  
  
  
  
  
  
  
  
  <!-------------------------------------------------- ADD COMPANY MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="addCompanyModal" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Add a Company Yo</h2>
              <p class="p-b-10">Hop on this Company Addin'</p>
            </div>
            
            
            <div class="modal-body">
  
	
            
          
          <!-- START SIGNUP Form -->
          <form id="form-register" class="p-t-15" role="form" action="admin.php" method="post">
          	<input type="hidden" name="addCompany" id="addCompany" value="1"/>
              
              
              
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Company Name</label>
                    <input type="text" name="company_name" class="form-control" required>
                  </div>
                </div>
                
            
              </div>
              
              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Company Phone</label>
                    <input type="text" name="company_phone" class="form-control" required>
                  </div>
                </div>
              </div>
              
            
              
              
              
              
              
              
                  <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default" id="locationField">
                    <label>City</label>
                    <input id="autocomplete" type="text" name="location" placeholder="Enter Your Location" class="form-control"  onFocus="geolocate()" required>
                  </div>
                </div>
              </div>
              
    

    <table id="address" hidden="">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number" name="company_address1"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality" name="company_city"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" name="company_state" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code" name="company_zip"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>
              
                 
             
              
              <div class="row m-t-10">
                <div class="col-md-12">
                
         
                      
                      
              	
                 
                </div>
                
                 <button class="btn btn-success btn-block" type="submit">Add a Company</button>
                 
                 <br>
               
              </div>
             
            </form>
            
        <!--END SIGNUP Form-->
        
                    
                    

            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END CANCELLED APPT MODAL ------------------------------------------------------------->







  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <!-------------------------------------------------- CANCELLED APPT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="cancelledApptModal" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Cancelled Appointments</h2>
              <p class="p-b-10">Cancellations Listed Below</p>
            </div>
            
            
            <div class="modal-body">
  
	
            
          
            <table class="table table-condensed table-hover ">
                        <tbody>
                        
                        
                        
                       
                        
                        
                        
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                       
                       
                       
                        </tbody>
                      </table>  
                          
                    
                    

            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END CANCELLED APPT MODAL ------------------------------------------------------------->













            
               
  
  
  
  
  
  
  




























<!-------------------------------------------------- UNASSIGNED DRIVER TO APPT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="unassignedApptModal" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Unassigned Appointments</h2>
              <p class="p-b-10">Unassigned Drivers to Appointments Listed Below</p>
            </div>
            
            
            <div class="modal-body">
  
	
            
          
            <table class="table table-condensed table-hover ">
                        <tbody>
                        
                        
                        
                        
                        
                         <?php while ($row = $getUnassignedAppointments->fetch(PDO::FETCH_ASSOC)) : ?>
                 
                         <tr>
                          
                            <td name="transport_type" width="10%">
                            
                            <div                                    
                                   	<?php if($row['transport_type'] == 'Ambulette')
                                       {
                                echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/ambulance.jpg' alt='' width='32' height='32'>";
										} 
                                                       
                                elseif ($row['transport_type'] == 'Black Car') 
                                       {
                                echo "class='thumbnail-wrapper d32 circular'>
								<img src='assets/img/car.jpg' alt='' width='32' height='32'>";
                                }
                             ?>
							</div> 
							
							</td>
							
							
							
							
							
							
                            <td width="40%" class="b-r b-dashed b-grey">
                              <span class="hint-text bold"><? echo $row['appointment_title'] ;  ?></span>  <em class="small"><? echo $row['patient_user_id'] ;  ?></em><br>
                              <span class="hint-text bold"><? echo $row['appointment_date'] ;  ?></span>  <span><? echo $row['appointment_time'] ;  ?></span>
                            </td>
                            
                                                        
                            
                            <td width="25%">
                              <span class="font-montserrat fs-18"><button type="button" href="#assignDriverModal"  data-toggle="modal" data-dismiss="modal" aria-hidden="true" class="btn"  ><i class="fa fa-car"></i> &nbsp; Assign Driver</button></span>
                            </td>
                          </tr>
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
  
  
  
  <!-------------------------------------------------- ASSIGN DRIVER TO APPT MODAL ---------------------------------------------------------------->
   
    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="assignDriverModal" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
          
          
            <div class="modal-header clearfix text-left">
            
              <h2>Pair Driver</h2>
              <p class="p-b-10">Select a Driver from the List of Available Drivers Below..</p>
            </div>
            
            
            <div class="modal-body">
  
	
			<script>
                function clickLink(a) {
       var myLink = a.getAttribute('value');
       window.open(myLink,'Rename','height=150px','width=250px');
       return false;
    }
            </script>
            
            
            <form id="form-event" action="appointments_all.php" role="form" method="post">
      <input type='hidden' name='pairDriverEvent' id='submitted_event' value='1'/>
      
                      <div class="form-group">
                      
                                             
                    <select class="full-width form-control-custom">
                    
                    
                             <?php while ($row = $getDrivers->fetch(PDO::FETCH_ASSOC)) : ?>
                                                  
                                                   <option value="<? echo $row['user_id'] ;  ?>" name="driver_user_id">
                                                   
                                                   		<span><? echo $row['first_name'] ;  ?> <? echo $row['last_name'] ;  ?></span>
                                                   
                                                   </option>
                     
							<?php endwhile; ?>
                      
                      
                    </select>                          
                         
                          
                                                  	 
                      </div>
             </form>
                    
                    
                    
                    
              <div class="row">
              
                <div class="col-sm-8">
                  <div class="p-t-20 clearfix p-l-10 p-r-10 hide">
                    <div class="pull-left">
                      <p class="bold font-montserrat text-uppercase"></p>
                    </div>
                    <div class="pull-right">
                      <p class="bold font-montserrat text-uppercase"></p>
                    </div>
                  </div>
                </div>
                
                <div class="col-sm-4 m-t-10 sm-m-t-10">
                  <button type="button" class="btn btn-success btn-block m-t-5">Save</button>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END ASSIGN DRIVER TO APPT MODAL ------------------------------------------------------------->
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          <?php endwhile; ?>
                          
                          
                          
                          
                          
                          
                        </tbody>
                      </table>  
                          
                    
                    

            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- /.modal-dialog -->
    
    
<!----------------------------------------------------- END PAIR DRIVER W/ APPT MODAL ------------------------------------------------------------->
    










     

  

  
  
  
  
  
  
  
  
  
  
  
  
  
      
    
    
    
    
    
    
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        
        
        
        
        
        <div class="pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
              <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="35">
            </div>
             <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20 ">
            
            <li class="p-r-15 inline">
                 <a href="#"><button href="#" class="fa fa-dashboard" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Dashboard"></button></a>
              </li>
            
         
            
            
            
            
            
              <li class="p-r-15 inline">
                   <a href="users.php"><button href="users.php" class="fa fa-users" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Users"></button></a>
              </li>
              
              <li class="p-r-15 inline">
                  <a href="appointments_list.php"><button href="appointments_list.php" class="pg-menu_justify" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="List View"></button></a>
              </li>
              <li class="p-r-15 inline active-view">
                   <a href="appointments_cal.html"><button href="appointments_cal.html" class="pg-calender" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Calendar View"></button></a>
              </li>
            </ul>
            <!-- END NOTIFICATIONS LIST -->
            
            
            <a href="#addCompanyModal"  data-toggle="modal" >Add Company</a>

            </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        
        
        
        
        
        
        
        <div class=" pull-right">
         <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            
            <div class="thumbnail-wrapper d32 circular inline m-t-5">
              <img src="assets/img/profiles/1.jpg" alt="" data-src="assets/img/profiles/1.jpg" data-src-retina="assets/img/profiles/1_small2x.jpg" width="32" height="32">
            </div>
            
            
            <div class="pull-left p-l-10 p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold text-master"><?php echo($_SESSION["first_name"]);?> <?php echo($_SESSION["last_name"]);  ?></span>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper m-t-50">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
        
        
        
        
        
        
        
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
          
          
            <!-- START ROW -->
            <div class="row">
              <div class="col-md-6 col-xlg-5">
               




<!---------------------------------------------------------- row ------------------------------------------------------>
     
               
                <div class="row">
                  <div class="col-md-12 m-b-10">
                    <div class="ar-3-2 widget-1-wrapper">
                    
                    
                    
                    
                    
                    
                    
                      <!-- START WIDGET -->
                      <div class="widget-1 panel no-border bg-master no-margin widget-loader-circle-lg">
                        <div class="panel-heading top-right ">
                          <div class="panel-controls">
                            <ul>
                              <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh-lg-master"></i></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="pull-bottom bottom-left bottom-right ">
                            <span class="label font-montserrat fs-11">DRIVER MAP VIEW</span>
                            <br>
                            <a href="drivers.php"><h2 class="text-white">View All <span class="bold">Drivers</span></h2></a>
                            <p class="text-white hint-text">View All ></p>
                            
                            
                            
                            <div class="row stock-rates m-t-15 hide">
                              <div class="company col-xs-4">
                                <div>
                                  <p class="font-montserrat text-success no-margin fs-16">
                                    <i class="fa fa-caret-up"></i> +0.95%
                                    <span class="font-arial text-white fs-12 hint-text m-l-5">546.45</span>
                                  </p>
                                  <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                                    AAPL
                                  </p>
                                </div>
                              </div>
                              <div class="company col-xs-4">
                                <div>
                                  <p class="font-montserrat text-danger no-margin fs-16">
                                    <i class="fa fa-caret-up"></i> -0.34%
                                    <span class="font-arial text-white fs-12 hint-text m-l-5">345.34</span>
                                  </p>
                                  <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                                    YAHW
                                  </p>
                                </div>
                              </div>
                              <div class="company col-xs-4">
                                <div class="pull-right">
                                  <p class="font-montserrat text-success no-margin fs-16">
                                    <i class="fa fa-caret-up"></i> +0.95%
                                    <span class="font-arial text-white fs-12 hint-text m-l-5">278.87</span>
                                  </p>
                                  <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                                    PAGES
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- END WIDGET -->
                      
                      
                      
                    </div>
                  </div>
                </div>
                
                
                
                
<!----------------------------------------------------- end  row ------------------------------------------------------>                
                
                
                
                
                
                <div class="row">
                
                
                
                  <div class="col-sm-6 m-b-10">
                    <div class="ar-2-1 hide">
                    
                    
                    
                                         
                      
                      
                    </div>
                  </div>
                  
                  
                  
                  
                  
                  
                  
                  <div class="col-sm-6 m-b-10">
                    <div class="ar-2-1 hide">
                    
                    
                      <!-- START WIDGET -->
                      <div class="widget-5 panel no-border  widget-loader-bar">
                        <div class="panel-heading pull-top top-right">
                          <div class="panel-controls">
                            <ul>
                              <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="container-xs-height full-height">
                          <div class="row row-xs-height">
                            <div class="col-xs-5 col-xs-height col-middle relative">
                              <div class="padding-15 top-left bottom-left">
                                <h5 class="hint-text no-margin p-l-10">Italy, Florence</h5>
                                <p class=" bold font-montserrat p-l-10">2,345,789
                                  <br>USD</p>
                                <p class=" hint-text visible-xlg p-l-10">Today's sales</p>
                              </div>
                            </div>
                            <div class="col-xs-7 col-xs-height col-bottom relative widget-5-chart-container">
                              <div class="widget-5-chart"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- END WIDGET -->
                      
                      
                    </div>
                  </div>
                  
                  
                </div>
                
                
                
                
                
                
              </div>
              
<!--------------------------------------------------- END TOP LEFT QUAD ----------------------------------------------->


















<!------------------------------------------------ START TOP RIGHT QUAD ----------------------------------------------->
             
              

              <div class="col-md-6 col-xlg-4">
              
                <div class="row">
                
                
                  <div class="col-sm-6 m-b-10">
                    <div class="ar-1-1">
                    
                    
                    
                      <!-- START WIDGET -->
                      <div class="widget-2 panel no-border bg-master widget widget-loader-circle-lg no-margin">
                        <div class="panel-heading">
                          <div class="panel-controls">
                            <ul>
                              <li><a href="#" class="portlet-refresh" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh-lg-white"></i></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="pull-bottom bottom-left bottom-right padding-25">
                            <a href="users.php"><span class="label font-montserrat fs-11">USERS</span></a>
                            <br>
                            <a href="users.php"><h3 class="text-white">View All <span class="bold">Users</span></h3></a>
                            <p class="text-white hint-text">View All ></p>
                          </div>
                        </div>
                      </div>
                      <!-- END WIDGET -->
                      
                      
                    </div>
                    
                    
                    
                    <div class="ar-2-1 m-t-10">
                    
                    
                    
                      <!-- START WIDGET -->
                      <div class="widget-4 panel no-border no-margin widget-loader-bar">
                        <div class="container-sm-height full-height">
                          <div class="row-sm-height">
                            <div class="col-sm-height col-top">
                              <div class="panel-heading ">
                                <div class="panel-title text-black hint-text">
                                  <span class="font-montserrat fs-11 all-caps">Companies <i class="fa fa-chevron-right"></i>
                                                        </span>
                                </div>
                                
                                
                                <div class="panel-controls">
                                  <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                  </ul>
                                </div>
                                
                                
                                
                              </div>
                            </div>
                          </div>
                        
                          
                             
                              <div class="col-sm-6 no-padding">
                               <div class="bg-white">
                                <span class="p-l-50 p-t-10 fs-80"><a href="#" class="text-master"><i class="fa fa-user-md"></a></i></span>
                               </div>
                              </div>
                              
                              
                              <div class="col-sm-6 no-padding">
                                <div class="bg-white">
                                <a href="#"><span class="p-l-25 p-t-10 fs-80"><i class="fa fa-ambulance"></i> </span></a>
                                </div>
                              </div>
                                
                        
                      
                       
                        </div>
                      </div>
                      <!-- END WIDGET -->
                      
                      
                      
                    </div>

                    
                    
                    
                  </div>
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  <div class="col-sm-6  m-b-10">
              
                    
                    <!-- START WIDGET -->
                <div class="widget-16 panel no-border bg-white no-margin widget-loader-circle">
                  <div class="panel-heading">
                    <div class="panel-title">Admin Panel 
                    </div>
                    <div class="panel-controls">
                      <ul>
                        <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="widget-16-header padding-20">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">ca</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Christine Admin</p>
                      <h5 class="no-margin overflow-ellipsis ">Super User</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="p-l-25 p-r-45 p-t-25 p-b-25 ">
                    <div class="row">
                      <div class="col-md-4 ">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Views</p>
                        <p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
                      </div>
                      <div class="col-md-4 text-center">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Today</p>
                        <p class="all-caps font-montserrat  no-margin text-warning ">24</p>
                      </div>
                      <div class="col-md-4 text-right">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Week</p>
                        <p class="all-caps font-montserrat  no-margin text-success ">56</p>
                      </div>
                    </div>
                  </div>
                  <div class="relative no-overflow hide">
                    <div class="widget-16-chart line-chart" data-line-color="success" data-points="true" data-point-color="white" data-stroke-width="2">
                      <svg></svg>
                    </div>
                  </div>
                  <div class="b-b b-t b-grey p-l-20 p-r-20 p-b-10 p-t-10">
                    <p class="pull-left">Toggle 1</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" checked="checked" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                   <div class="b-b b-t b-grey p-l-20 p-r-20 p-b-10 p-t-10">
                    <p class="pull-left">Toggle 2</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" checked="checked" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                   <div class="b-b b-t b-grey p-l-20 p-r-20 p-b-10 p-t-10">
                    <p class="pull-left">Toggle 3</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" checked="checked" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="b-b b-grey p-l-20 p-r-20 p-b-15 p-t-15">
                    <p class="pull-left">Toggle 4</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" checked="checked" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="p-l-20 p-r-20 p-t-10 p-b-10 hide ">
                    <p class="pull-left no-margin hint-text">More Fun Stuff</p>
                    <a href="#" class="pull-right"><i class="fa fa-arrow-circle-o-down text-success fs-16"></i></a>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>

                      
                      
                 
               
                  
                </div>
                
                
                
                
                
                
                
                
                
                <div class="row">
                
                  <div class="col-sm-6 m-b-10">
                    <div class="ar-1-1 hide">
                    
                      <!-- START WIDGET -->
                      <div class="panel no-border bg-master widget widget-6 widget-loader-circle-lg no-margin">
                        <div class="panel-heading">
                          <div class="panel-controls">
                            <ul>
                              <li><a data-toggle="refresh" class="portlet-refresh" href="#"><i class="portlet-icon portlet-icon-refresh-lg-white"></i></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="pull-bottom bottom-left bottom-right padding-25">
                            <h1 class="text-white semi-bold">30&#176;</h1>
                            <span class="label font-montserrat fs-11">TODAY</span>
                            <p class="text-white m-t-20">Feels like rainy</p>
                            <p class="text-white hint-text m-t-30">November 2014, 5 Thusday </p>
                          </div>
                        </div>
                      </div>
                      <!-- END WIDGET -->
                      
                    </div>
                  </div>
                  
                  
                  
                  
                  <div class="col-sm-6 m-b-10">
                    <div class="ar-1-1 hide">
                      <!-- START WIDGET -->
                      <div class="widget-7 panel no-border bg-success no-margin">
                        <div class="panel-body no-padding">
                          <div class="metro live-tile " data-delay="3500" data-mode="carousel">
                            <div class="slide-front tiles slide active">
                              <div class="padding-30">
                                <div class="pull-bottom p-b-30 bottom-right bottom-left p-l-30 p-r-30">
                                  <h5 class="no-margin semi-bold p-b-5">Apple Inc.</h5>
                                  <p class="no-margin text-black hint-text">NASDAQ : AAPL - NOV 01 8:40 AM ET</p>
                                  <h3 class="semi-bold text-white"><i class="fa fa-sort-up small text-white"></i> 0.15 (0.13%)</h3>
                                  <p><span class="text-black">Open</span> <span class="m-l-20 hint-text">117.52</span>
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div class="slide-back tiles">
                              <div class="container-sm-height full-height">
                                <div class="row-sm-height">
                                  <div class="col-sm-height padding-25">
                                    <p class="hint-text text-black">Pre-market: 116.850.50 (0.43%)</p>
                                    <p class="p-t-10 text-black">AAPL - Apple inc.</p>
                                    <div class="p-t-10">
                                      <p class="hint-text inline">+0.42% <span class="m-l-20">217.51</span>
                                      </p>
                                      <div class="inline"><i class="fa fa-sort-up small text-white fs-16 col-bottom"></i>
                                      </div>
                                    </div>
                                    <p class="p-t-10 text-black">GOOG - Google inc.</p>
                                    <div class="p-t-10">
                                      <p class="hint-text inline">+0.22% <span class="m-l-20">-2.28</span>
                                      </p>
                                      <div class="inline"><i class="fa fa-sort-down small text-white fs-16 col-top"></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row-sm-height">
                                  <div class="col-sm-height relative">
                                    <div class='widget-7-chart line-chart' data-line-color="white" data-points="true" data-point-color="white" data-stroke-width="3">
                                      <svg></svg>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- END WIDGET -->
                    </div>
                  </div>
                  
                  
                  
                  
                  
                </div>
              </div>
              
              
              
              
              
              
              
<!--------------------------------------------------- END TOP RIGHT QUAD ---------------------------------------------->              
              
              
              
              
              
              
              
              <!-- Filler -->
              <div class="visible-xlg col-xlg-3 hide">
                <div class="ar-2-3 hide">
                 
                 
                 
                 
                 
                  <!-- START WIDGET -->
                  <div class="widget-11 panel no-border  no-margin widget-loader-bar hide">
                    <div class="panel-heading ">
                      <div class="panel-title">Today's sales
                      </div>
                      <div class="panel-controls">
                        <ul>
                          <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="p-l-25 p-r-25 p-b-20">
                      <div class="pull-left">
                        <h2 class="text-success no-margin">webarch</h2>
                      </div>
                      <h3 class="pull-right semi-bold"><sup><small class="semi-bold">$</small></sup> 102,967</h3>
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-11-table auto-overflow">
                      <table class="table table-condensed table-hover">
                        <tbody>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">janedrooler</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="padding-25">
                      <p class="small no-margin">
                        <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                        <span class="hint-text ">Show more details of APPLE . INC</span>
                      </p>
                    </div>
                  </div>
                  <!-- END WIDGET -->
                  
                  
                  
                  
                </div>
              </div>
              
              
              
              
            </div>
            <!-- END ROW -->
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <div class="row">
            
            
            
            
            
              <div class="col-md-4 col-lg-3 col-xlg-2 ">
                
                
                <div class="row">
                  <div class="col-md-12 m-b-10">
                  
                  
                  
                    <!-- START WIDGET -->
                    <div class="widget-8 panel no-border bg-success no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading top-left top-right">
                              <div class="panel-title text-black hint-text">
                               <span class="font-montserrat fs-11 all-caps"><a href="#unassignedApptModal"  data-toggle="modal" >
Unassigned Appointments <i class="fa fa-chevron-right"></i></a>
                                                    </span>
                              </div>
                              <div class="panel-controls">
                                <ul>
                                  <li>
                                    <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height ">
                          <div class="col-xs-height col-top relative">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="p-l-20">
                                  <h3 class="no-margin p-b-5 text-white bold">14</h3>
                                  <p class="small hint-text m-t-5">
                                    <span class="label  font-montserrat m-r-5">60%</span>Higher
                                  </p>
                                </div>
                              </div>
                              <div class="col-sm-6">
                              </div>
                            </div>
                            <div class='widget-8-chart line-chart' data-line-color="black" data-points="true" data-point-color="success" data-stroke-width="2">
                              <svg></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                    
                    
                    
                    
                    
                  </div>
                </div>
                
                
                <div class="row">
                  <div class="col-md-12 m-b-10">
                  
                  
                    <!-- START WIDGET -->
                    <div class="widget-9 panel no-border bg-danger no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading  top-left top-right">
                              <div class="panel-title text-black">
                                <span class="font-montserrat fs-11 all-caps"><a href="#cancelledApptModal"  data-toggle="modal" >
Cancelled Appointments <i class="fa fa-chevron-right"></i></a>
                                                    </span>
                              </div>
                              <div class="panel-controls">
                                <ul>
                                  <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="p-l-20 p-t-15">
                              <h3 class="no-margin p-b-5 text-white bold">2</h3>
                              <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>
                                                        </a>
                              <span class="small hint-text">65% lower than last month</span>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-bottom">
                            <div class="progress progress-small m-b-20">
                              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                              <div class="progress-bar progress-bar-white" data-percentage="45%"></div>
                              <!-- END BOOTSTRAP PROGRESS -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                    
                    
                  </div>
                </div>
                
                
                <div class="row">
                  <div class="col-md-12">
                    <!-- START WIDGET -->
                    <div class="widget-10 panel no-border bg-warning no-margin widget-loader-bar">
                      <div class="panel-heading top-left top-right ">
                        <div class="panel-title text-black hint-text">
                          <span class="font-montserrat fs-11 all-caps"><a href="appointments_list.php">All Appointments <i class="fa fa-chevron-right"></i></a>
                                        </span>
                        </div>
                        <div class="panel-controls">
                          <ul>
                            <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="panel-body p-t-40">
                        <div class="row">
                          <div class="col-sm-12">
                            <h4 class="no-margin p-b-5 text-danger bold">265</h4>
                            <div class="pull-left small">
                              <span>WMHC</span>
                              <span class=" text-success font-montserrat">
                                                    <i class="fa fa-caret-up m-l-10"></i> 9%
                                                </span>
                            </div>
                            <div class="pull-left m-l-20 small">
                              <span>HCRS</span>
                              <span class=" text-danger font-montserrat">
                                                    <i class="fa fa-caret-up m-l-10"></i> 21%
                                                </span>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="p-t-10 full-width">
                          <a href="#" class="btn-circle-arrow b-grey"><i class="pg-arrow_minimize text-danger"></i></a>
                          <span class="hint-text small">Show more</span>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
                
                
                
                
              </div>
              
              
              
              
              
              <div class="col-md-8 col-lg-5 col-xlg-6 m-b-10">
              
              
                <div class="row">
                  <div class="col-md-12">
                    <!-- START WIDGET -->
                    <div class="widget-12 panel no-border widget-loader-circle no-margin">
                      <div class="row">
                        <div class="col-xlg-8 ">
                          <div class="panel-heading pull-up top-right ">
                            <div class="panel-controls">
                              <ul>
                                <li class="hidden-xlg">
                                  <div class="dropdown">
                                    <a data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                      <i class="portlet-icon portlet-icon-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                      <li><a href="#">AAPL</a>
                                      </li>
                                      <li><a href="#">YHOO</a>
                                      </li>
                                      <li><a href="#">GOOG</a>
                                      </li>
                                    </ul>
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xlg-8 ">
                            <div class="p-l-10">
                              <h2 class="pull-left">Fun Graph</h2>
                              <h2 class="pull-left m-l-50 text-danger">
                                                    <span class="bold">448.97</span>
                                                    <span class="text-danger fs-12">-318.24</span>
                                                </h2>
                              <div class="clearfix"></div>
                              <div class="full-width">
                                <ul class="list-inline">
                                  <li><a href="#" class="font-montserrat text-master">1D</a>
                                  </li>
                                  <li class="active"><a href="#" class="font-montserrat  bg-master-light text-master">5D</a>
                                  </li>
                                  <li><a href="#" class="font-montserrat text-master">1M</a>
                                  </li>
                                  <li><a href="#" class="font-montserrat text-master">1Y</a>
                                  </li>
                                </ul>
                              </div>
                              <div class="nvd3-line line-chart text-center" data-x-grid="false">
                                <svg></svg>
                              </div>
                            </div>
                          </div>
                          <div class="col-xlg-4 visible-xlg">
                            <div class="widget-12-search">
                              <p class="pull-left">Company
                                <span class="bold">List</span>
                              </p>
                              <button class="btn btn-default btn-xs pull-right">
                                <span class="bold">+</span>
                              </button>
                              <div class="clearfix"></div>
                              <input type="text" placeholder="Search list" class="form-control m-t-5">
                            </div>
                            <div class="company-stat-boxes">
                              <div data-index="0" class="company-stat-box m-t-15 active padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-success fs-11"></span> AAPL
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-important p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div data-index="1" class="company-stat-box m-t-15  padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-primary fs-11"></span> YHOO
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div data-index="2" class="company-stat-box m-t-15  padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-complete fs-11"></span> GOOG
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 hidden-xlg m-b-10">
                <!-- START WIDGET -->
                <div class="widget-11-2 panel no-border panel-condensed no-margin widget-loader-circle">
                  <div class="panel-heading top-right">
                    <div class="panel-controls">
                      <ul>
                        <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="padding-25">
                    <div class="pull-left">
                      <h2 class="text-success no-margin">Fun Table</h2>
                      <p class="no-margin">Fun Facts</p>
                    </div>
                    <h3 class="pull-right semi-bold"><sup><small class="semi-bold">$</small></sup> 102,967</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="auto-overflow widget-11-2-table">
                    <table class="table table-condensed table-hover">
                      <tbody>
                        
                        
                        
                        
                        
                        
                        
                        <tr>
                          <td class="font-montserrat all-caps fs-12 col-lg-6">Purchase CODE #2345</td>
                          <td class="text-right hidden-lg">
                            <span class="hint-text small">dewdrops</span>
                          </td>
                          <td class="text-right b-r b-dashed b-grey col-lg-3">
                            <span class="hint-text small">Qty 1</span>
                          </td>
                          <td class="col-lg-3">
                            <span class="font-montserrat fs-18">$27</span>
                          </td>
                        </tr>
                       
                       
                       
                       
                       
                      </tbody>
                    </table>
                  </div>
                  <div class="padding-25">
                    <p class="small no-margin">
                      <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                      <span class="hint-text ">Show more details of APPLE . INC</span>
                    </p>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
              
              
              
              
              
              
              
              <div class="col-md-6 hidden-lg visible-md visible-xlg col-xlg-4 m-b-10">
                <!-- START WIDGET -->
                <div class="widget-15 panel panel-condensed  no-margin no-border widget-loader-circle">
                  <div class="panel-heading">
                    <div class="panel-controls">
                      <ul>
                        <li><a href="#" class="portlet-collapse" data-toggle="collapse"><i class="portlet-icon portlet-icon-collapse"></i></a>
                        </li>
                        <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="panel-body no-padding">
                    <ul class="nav nav-tabs nav-tabs-simple">
                      <li class="active">
                        <a href="#widget-15-tab-1" class="p-t-5">
                                    APPL<br>
                                    22.23<br>
                                    <span class="text-success">+60.223%</span>
                                </a>
                      </li>
                      <li><a href="#widget-15-tab-2" class="p-t-5">
                                    FB<br>
                                    45.97<br>
                                    <span class="text-danger">-06.56%</span>
                              </a>
                      </li>
                      <li><a href="#widget-15-tab-3" class="p-t-5">
                                    GOOG<br>
                                    22.23<br>
                                    <span class="text-success">+60.223%</span>                      
                              </a>
                      </li>
                    </ul>
                    <div class="tab-content p-l-20 p-r-20">
                      <div class="tab-pane no-padding active" id="widget-15-tab-1">
                        <div class="full-width">
                          <div class="full-width">
                            <div class="widget-15-chart rickshaw-chart"></div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane no-padding" id="widget-15-tab-2">
                      </div>
                      <div class="tab-pane" id="widget-15-tab-3">
                      </div>
                    </div>
                    <div class="p-t-20 p-l-20 p-r-20 p-b-30">
                      <div class="row">
                        <div class="col-md-9">
                          <p class="fs-16 text-black">Apple’s Motivation - Innovation
                            <br>distinguishes between A leader and a follower.
                          </p>
                          <p class="small hint-text">VIA Apple Store (Consumer and Education Individuals)
                            <br>(800) MY-APPLE (800-692-7753)
                          </p>
                        </div>
                        <div class="col-md-3 text-right">
                          <p class="font-montserrat bold text-success m-r-20 fs-16">+0.94</p>
                          <p class="font-montserrat bold text-danger m-r-20 fs-16">-0.63</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
            </div>
            
            
            
            
            
            
            <div class="row hide">
              <div class="col-md-8 m-b-10 ">
                <!-- START WIDGET -->
                <div class="widget-13 panel no-border  no-margin widget-loader-circle">
                  <div class="panel-heading pull-up top-right ">
                    <div class="panel-controls">
                      <ul>
                        <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="container-sm-height no-overflow">
                    <div class="row row-sm-height">
                      <div class="col-sm-5 col-lg-4 col-xlg-3 col-sm-height col-top no-padding">
                        <div class="panel-heading ">
                          <div class="panel-title">3
                          </div>
                        </div>
                        <div class="panel-body">
                          <ul class="nav nav-pills" role="tablist">
                            <li class="active">
                              <a href="#tab1" data-toggle="tab" role="tab" class="b-a b-grey text-master">
                                                            fb
                                                        </a>
                            </li>
                            <li>
                              <a href="#tab2" data-toggle="tab" role="tab" class="b-a b-grey text-master">
                                                            js
                                                        </a>
                            </li>
                            <li>
                              <a href="#tab3" data-toggle="tab" role="tab" class="b-a b-grey text-master">
                                                            sa
                                                        </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                              <h3>Facebook</h3>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Views</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Today</p>
                              <p class="all-caps font-montserrat  no-margin text-warning ">24</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Week</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">56</p>
                            </div>
                            <div class="tab-pane " id="tab2">
                              <h3>Google</h3>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Views</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Today</p>
                              <p class="all-caps font-montserrat  no-margin text-warning ">24</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Week</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">56</p>
                            </div>
                            <div class="tab-pane" id="tab3">
                              <h3>Amazon</h3>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Views</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Today</p>
                              <p class="all-caps font-montserrat  no-margin text-warning ">24</p>
                              <br>
                              <p class="hint-text all-caps font-montserrat small no-margin ">Week</p>
                              <p class="all-caps font-montserrat  no-margin text-success ">56</p>
                            </div>
                          </div>
                        </div>
                        <div class="bg-master-light p-l-20 p-r-20 p-t-10 p-b-10 pull-bottom full-width hidden-xs">
                          <p class="no-margin">
                            <a href="#"><i class="fa fa-arrow-circle-o-down text-success"></i></a>
                            <span class="hint-text">Super secret options</span>
                          </p>
                        </div>
                      </div>
                      <div class="col-sm-7 col-lg-8 col-xlg-9 col-sm-height col-top no-padding relative">
                        <div class="bg-success widget-13-map ">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
              
              
              
              <div class="col-md-4 m-b-10">
                <!-- START WIDGET -->
                <div class="widget-14 panel no-border  no-margin widget-loader-circle">
                  <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                      <div class="col-xs-height">
                        <div class="panel-heading">
                          <div class="panel-title">Server load
                          </div>
                          <div class="panel-controls">
                            <ul>
                              <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row-xs-height">
                      <div class="col-xs-height">
                        <div class="p-l-20 p-r-20">
                          <p>S</p>
                          <div class="row">
                            <div class="col-lg-3 col-md-12">
                              <h4 class="bold no-margin">5.2GB</h4>
                              <p class="small no-margin">Total usage</p>
                            </div>
                            <div class="col-lg-3 col-md-6">
                              <h5 class=" no-margin p-t-5">227.34KB</h5>
                              <p class="small no-margin">Currently</p>
                            </div>
                            <div class="col-lg-3 col-md-6">
                              <h5 class=" no-margin p-t-5">117.65MB</h5>
                              <p class="small no-margin">Average</p>
                            </div>
                            <div class="col-lg-3 visible-xlg">
                              <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row-xs-height">
                      <div class="col-xs-height relative bg-master-lightest">
                        <div class="widget-14-chart_y_axis"></div>
                        <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
            </div>
            
            
            
            
            
            
            
            <div class="row hide">
              <div class="col-lg-4 visible-lg hidden-xlg">
                <!-- START WIDGET -->
                <div class="widget-15-2 panel no-margin no-border widget-loader-circle">
                  <div class="panel-heading top-right">
                    <div class="panel-controls">
                      <ul>
                        <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <ul class="nav nav-tabs nav-tabs-simple">
                    <li class="active">
                      <a href="#widget-15-2-tab-1">
                                    APPL<br>
                                    22.23<br>
                                    <span class="text-success">+60.223%</span>
                                </a>
                    </li>
                    <li><a href="#widget-15-2-tab-2">
                                    FB<br>
                                    45.97<br>
                                    <span class="text-danger">-06.56%</span>
                              </a>
                    </li>
                    <li><a href="#widget-15-2-tab-3">
                                    GOOG<br>
                                    22.23<br>
                                    <span class="text-success">+60.223%</span>                      
                              </a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane no-padding active" id="widget-15-2-tab-1">
                      <div class="full-width">
                        <div class="widget-15-chart2 rickshaw-chart full-height"></div>
                      </div>
                    </div>
                    <div class="tab-pane no-padding" id="widget-15-2-tab-2">
                    </div>
                    <div class="tab-pane" id="widget-15-2-tab-3">
                    </div>
                  </div>
                  <div class="p-t-10 p-l-20 p-r-20 p-b-30">
                    <div class="row">
                      <div class="col-md-9">
                        <p class="fs-16 text-black">Apple’s Motivation - Innovation distinguishes between A leader and a follower.
                        </p>
                        <p class="small hint-text">VIA Apple Store (Consumer and Education Individuals)
                          <br>(800) MY-APPLE (800-692-7753)
                        </p>
                      </div>
                      <div class="col-md-3 text-right">
                        <h5 class="font-montserrat bold text-success">+0.94</h5>
                        <h5 class="font-montserrat bold text-danger">-0.63</h5>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
              
              
              
              
              
              
              
              
              <div class="col-md-4 col-lg-3 col-xlg-3 m-b-10">
                <!-- START WIDGET -->
                <div class="widget-16 panel no-border  no-margin widget-loader-circle">
                  <div class="panel-heading">
                    <div class="panel-title">Page Options
                    </div>
                    <div class="panel-controls">
                      <ul>
                        <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="widget-16-header padding-20">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Pages name</p>
                      <h5 class="no-margin overflow-ellipsis ">Webarch Sales Analysis</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="p-l-25 p-r-45 p-t-25 p-b-25">
                    <div class="row">
                      <div class="col-md-4 ">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Views</p>
                        <p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
                      </div>
                      <div class="col-md-4 text-center">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Today</p>
                        <p class="all-caps font-montserrat  no-margin text-warning ">24</p>
                      </div>
                      <div class="col-md-4 text-right">
                        <p class="hint-text all-caps font-montserrat small no-margin ">Week</p>
                        <p class="all-caps font-montserrat  no-margin text-success ">56</p>
                      </div>
                    </div>
                  </div>
                  <div class="relative no-overflow">
                    <div class="widget-16-chart line-chart" data-line-color="success" data-points="true" data-point-color="white" data-stroke-width="2">
                      <svg></svg>
                    </div>
                  </div>
                  <div class="b-b b-t b-grey p-l-20 p-r-20 p-b-10 p-t-10">
                    <p class="pull-left">Post is Public</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="b-b b-grey p-l-20 p-r-20 p-b-10 p-t-10">
                    <p class="pull-left">Maintenance mode</p>
                    <div class="pull-right">
                      <input type="checkbox" data-init-plugin="switchery" checked="checked" />
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="p-l-20 p-r-20 p-t-10 p-b-10 ">
                    <p class="pull-left no-margin hint-text">Super secret options</p>
                    <a href="#" class="pull-right"><i class="fa fa-arrow-circle-o-down text-success fs-16"></i></a>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div>
              
              
              
              
              
              
              
                          
              
              
              <div class="col-md-8 col-lg-5 col-xlg-5">
                <!-- START WIDGET -->
                <div class="widget-17 panel  no-border no-margin widget-loader-circle">
                  <div class="panel-heading">
                    <div class="panel-title">
                      <i class="pg-map"></i> California, USA
                      <span class="caret"></span>
                    </div>
                    <div class="panel-controls">
                      <ul>
                        <li class="">
                          <div class="dropdown">
                            <a data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                              <i class="portlet-icon portlet-icon-settings"></i>
                            </a>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="#">1</a>
                              </li>
                              <li><a href="#">2</a>
                              </li>
                              <li><a href="#">3</a>
                              </li>
                            </ul>
                          </div>
                        </li>
                        <li>
                          <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="panel-body">
                    <div class="p-l-5">
                      <div class="row">
                        <div class="col-md-12 col-xlg-6">
                          <div class="row m-t-20">
                            <div class="col-md-5">
                              <h4 class="no-margin">Monday</h4>
                              <p class="small hint-text">9th August 2014</p>
                            </div>
                            <div class="col-md-7">
                              <div class="pull-left">
                                <p class="small hint-text no-margin">Currently</p>
                                <h4 class="text-danger bold no-margin">32°
                            <span class="small">/ 30C</span>
                        </h4>
                              </div>
                              <div class="pull-right">
                                <canvas height="64" width="64" class="clear-day"></canvas>
                              </div>
                            </div>
                          </div>
                          <h5>Feels like
                <span class="semi-bold">rainy</span>
            </h5>
                          <p>Weather information</p>
                          <div class="widget-17-weather">
                            <div class="row">
                              <div class="col-sm-6 p-r-10">
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Wind</p>
                                    <p class="pull-right bold">11km/h</p>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Sunrise</p>
                                    <p class="pull-right bold">05:20</p>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Humidity</p>
                                    <p class="pull-right bold">20%</p>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Precipitation</p>
                                    <p class="pull-right bold">60%</p>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-6 p-l-10">
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Sunset</p>
                                    <p class="pull-right bold">21:05</p>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <p class="pull-left">Visibility</p>
                                    <p class="pull-right bold">21km</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row m-t-10 timeslot">
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">13:30</p>
                              <canvas height="25" width="25" class="partly-cloudy-day"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">14:00</p>
                              <canvas height="25" width="25" class="cloudy"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">14:30</p>
                              <canvas height="25" width="25" class="rain"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">15:00</p>
                              <canvas height="25" width="25" class="sleet"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">15:30</p>
                              <canvas height="25" width="25" class="snow"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                            <div class="col-xs-2 p-t-10 text-center">
                              <p class="small">16:00</p>
                              <canvas height="25" width="25" class="wind"></canvas>
                              <p class="text-danger bold">30°C</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-xlg-6 visible-xlg">
                          <div class="row">
                            <div class="forecast-day col-md-6 text-center m-t-10 ">
                              <div class="bg-master-lighter p-b-10 p-t-10">
                                <h4 class="p-t-10 no-margin">Tuesday</h4>
                                <p class="small hint-text m-b-20">11th Augest 2014</p>
                                <canvas class="rain" width="64" height="64"></canvas>
                                <h5 class="text-danger">32°</h5>
                                <p>Feels like
                                  <span class="bold">sunny</span>
                                </p>
                                <p class="small">Wind
                                  <span class="bold p-l-20">11km/h</span>
                                </p>
                                <div class="m-t-20 block">
                                  <div class="padding-10">
                                    <div class="row">
                                      <div class="col-md-6 text-center">
                                        <p class="small">Noon</p>
                                        <canvas class="sleet" width="25" height="25"></canvas>
                                        <p class="text-danger bold">30°C</p>
                                      </div>
                                      <div class="col-md-6 text-center">
                                        <p class="small">Night</p>
                                        <canvas class="wind" width="25" height="25"></canvas>
                                        <p class="text-danger bold">30°C</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6 text-center m-t-10 ">
                              <div class="bg-master-lighter p-b-10 p-t-10">
                                <h4 class="p-t-10 no-margin">Wednesday</h4>
                                <p class="small hint-text m-b-20">11th Augest 2014</p>
                                <canvas class="rain" width="64" height="64"></canvas>
                                <h5 class="text-danger">32°</h5>
                                <p>Feels like
                                  <span class="bold">sunny</span>
                                </p>
                                <p class="small">Wind
                                  <span class="bold p-l-20">11km/h</span>
                                </p>
                                <div class="m-t-20 block">
                                  <div class="padding-10">
                                    <div class="row">
                                      <div class="col-md-6 text-center">
                                        <p class="small">Noon</p>
                                        <canvas class="sleet" width="25" height="25"></canvas>
                                        <p class="text-danger bold">30°C</p>
                                      </div>
                                      <div class="col-md-6 text-center">
                                        <p class="small">Night</p>
                                        <canvas class="wind" width="25" height="25"></canvas>
                                        <p class="text-danger bold">30°C</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END WIDGET -->
              </div> 
            </div>
            
            
            
            
            
            
            
            
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2014 </span>
              <span class="font-montserrat">MediCoupe</span>.
              <span class="hint-text">All rights reserved. </span>
              <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
              <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        <li class="">
          <a href="#quickview-notes" data-toggle="tab">Notes</a>
        </li>
        <li>
          <a href="#quickview-alerts" data-toggle="tab">Alerts</a>
        </li>
        <li class="active">
          <a href="#quickview-chat" data-toggle="tab">Chat</a>
        </li>
      </ul>
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- BEGIN Notes !-->
        <div class="tab-pane fade  in no-padding" id="quickview-notes">
          <div class="view-port clearfix quickview-notes" id="note-views">
            <!-- BEGIN Note List !-->
            <div class="view list" id="quick-note-list">
              <div class="toolbar clearfix">
                <ul class="pull-right ">
                  <li>
                    <a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
                  </li>
                  <li>
                    <a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
                  </li>
                </ul>
                <button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
              </div>
              <ul>
                <!-- BEGIN Note Item !-->
                <li data-noteid="1" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox1" type="checkbox" value="1">
                      <label for="qncheckbox1"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox2" type="checkbox" value="1">
                      <label for="qncheckbox2"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox3" type="checkbox" value="1">
                      <label for="qncheckbox3"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="3" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox4" type="checkbox" value="1">
                      <label for="qncheckbox4"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                <!-- BEGIN Note Item !-->
                <li data-noteid="4" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox5" type="checkbox" value="1">
                      <label for="qncheckbox5"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
              </ul>
            </div>
            <!-- END Note List !-->
            <div class="view note" id="quick-note">
              <div>
                <ul class="toolbar">
                  <li><a href="#" class="close-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="pg-arrow_left"></i></a>
                  </li>
                  <li><a href="#" class="Bold"><i class="fa fa-bold"></i></a>
                  </li>
                  <li><a href="#" class="Italic"><i class="fa fa-italic"></i></a>
                  </li>
                  <li><a href="#" class=""><i class="fa fa-link"></i></a>
                  </li>
                </ul>
                <div class="body">
                  <div>
                    <div class="top">
                      <span>21st april 2014 2:13am</span>
                    </div>
                    <div class="content">
                      <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END Notes !-->
        <!-- BEGIN Alerts !-->
        <div class="tab-pane fade no-padding" id="quickview-alerts">
          <div class="view-port clearfix" id="alerts">
            <!-- BEGIN Alerts View !-->
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default navbar-sm">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Notications
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-search"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <!-- BEGIN Alert List !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <!-- BEGIN List Group !-->
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Calendar
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">David Nester Birthday</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today <br></span>
                          <span class="text-master">All Day</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                      <!-- BEGIN List Group Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12">
                          <span class="text-master">Meeting at 2:30</span>
                        </p>
                        <p class="p-r-10 col-xs-height col-middle fs-12 text-right">
                          <span class="text-warning">Today</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
                <!-- END List Group !-->
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Social
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Jame Smith commented on your status<br></span>
                          <span class="text-master">“Perfection Simplified - Company Revox"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="javascript:;" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">Jame Smith commented on your status<br></span>
                          <span class="text-master">“Perfection Simplified - Company Revox"</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Sever Status
                  </div>
                  <!-- END List Group Header!-->
                  <ul>
                    <!-- BEGIN List Group Item!-->
                    <li class="alert-list">
                      <!-- BEGIN Alert Item Set Animation using data-view-animation !-->
                      <a href="#" class="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <p class="col-xs-height col-middle">
                          <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                        </p>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12">
                          <span class="text-master link">12:13AM GTM, 10230, ID:WR174s<br></span>
                          <span class="text-master">Server Load Exceeted. Take action</span>
                        </p>
                      </a>
                      <!-- END Alert Item!-->
                    </li>
                    <!-- END List Group Item!-->
                  </ul>
                </div>
              </div>
              <!-- END Alert List !-->
            </div>
            <!-- EEND Alerts View !-->
          </div>
        </div>
        <!-- END Alerts !-->
        <div class="tab-pane fade in active no-padding" id="quickview-chat">
          <div class="view-port clearfix" id="chat">
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-plus"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Chat List
                    <div class="fs-11">Show All</div>
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a href="#" class="inline action p-r-10 pull-right link text-master">
                    <i class="pg-more"></i>
                  </a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    a</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">ava flores</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">b</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">bella mccoy</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">bob stephens</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">c</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">carole roberts</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">christopher perez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">d</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">danielle fletcher</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">david sutton</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">e</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">earl hamilton</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">elaine lawrence</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">ellen grant</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">erik taylor</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">everett wagner</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">f</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">freddie gomez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">g</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">glen jensen</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">gwendolyn walker</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">j</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">janet romero</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">k</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">kim martinez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">l</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lawrence white</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">leroy bell</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">letitia carr</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">lucy castro</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">m</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mae hayes</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marilyn owens</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marlene cole</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha warren</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">marsha dean</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">mia diaz</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">n</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">noah elliott</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">p</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">phyllis hamilton</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">r</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">raul rodriquez</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">rhonda barnett</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">roberta king</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">s</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/6x.jpg" data-src="assets/img/profiles/6.jpg" src="assets/img/profiles/6x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">scott armstrong</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/7x.jpg" data-src="assets/img/profiles/7.jpg" src="assets/img/profiles/7x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sebastian austin</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">sofia davis</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">t</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/9x.jpg" data-src="assets/img/profiles/9.jpg" src="assets/img/profiles/9x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">terrance young</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">theodore woods</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">todd wood</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">tommy jenkins</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">w</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">wilma hicks</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
              </div>
            </div>
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    John Smith
                    <div class="fs-11 hint-text">Online</div>
                  </div>
                  <a href="#" class="link text-master inline action p-r-10 pull-right ">
                    <i class="pg-more"></i>
                  </a>
                </div>
              </div>
              <!-- END Header  !-->
              <!-- BEGIN Conversation  !-->
              <div class="chat-inner" id="my-conversation">
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Hello there
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Hey
                  </div>
                </div>
                <!-- END From Them Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Did you check out Pages framework ?
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Its an awesome chat
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Yea
                  </div>
                </div>
                <!-- END From Them Message  !-->
              </div>
              <!-- BEGIN Conversation  !-->
              <!-- BEGIN Chat Input  !-->
              <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="col-xs-8 no-padding">
                    <input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
                  </div>
                  <div class="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                    <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    <!-- START OVERLAY -->
    <div class="overlay hide" data-pages="search">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-20">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          <img class="overlay-brand" src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/medicoupe.png" height="22">
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close text-black fs-16">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Controls !-->
          <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">
          <br>
          <div class="inline-block">
            <div class="checkbox right">
              <input id="checkboxn" type="checkbox" value="1" checked="checked">
              <label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
            </div>
          </div>
          <div class="inline-block m-l-10">
            <p class="fs-13">Press enter to search</p>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- BEGIN Overlay Search Results, This part is for demo purpose, you can add anything you like !-->
        <div class="container-fluid">
          <span>
                <strong>suggestions :</strong>
            </span>
          <span id="overlay-suggestions"></span>
          <br>
          <div class="search-results m-t-40">
            <p class="bold">Pages Search Results</p>
            <div class="row">
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>
                      <img width="50" height="50" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on pages</h5>
                    <p class="hint-text">via john smith</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div>T</div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> related topics</h5>
                    <p class="hint-text">via pages</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                    <div><i class="fa fa-headphones large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> music</h5>
                    <p class="hint-text">via pagesmix</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
              <div class="col-md-6">
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
                    <div><i class="fa fa-facebook large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on facebook</h5>
                    <p class="hint-text">via facebook</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
                    <div><i class="fa fa-twitter large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Tweats on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via twitter</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
                <!-- BEGIN Search Result Item !-->
                <div class="">
                  <!-- BEGIN Search Result Item Thumbnail !-->
                  <div class="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
                    <div><i class="fa fa-google-plus large-text "></i>
                    </div>
                  </div>
                  <!-- END Search Result Item Thumbnail !-->
                  <div class="p-l-10 inline p-t-5">
                    <h5 class="m-b-5">Circles on<span class="semi-bold result-name"> ice cream</span></h5>
                    <p class="hint-text">via google plus</p>
                  </div>
                </div>
                <!-- END Search Result Item !-->
              </div>
            </div>
          </div>
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/utils.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
    <script src="assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="assets/plugins/mapplic/js/hammer.js"></script>
    <script src="assets/plugins/mapplic/js/jquery.mousewheel.js"></script>
    <script src="assets/plugins/mapplic/js/mapplic.js"></script>
    <script src="assets/plugins/rickshaw/rickshaw.min.js"></script>
    <script src="assets/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="assets/plugins/skycons/skycons.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/dashboard.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  </body>
</html>