  <?php
    $image = $_POST['fileInput'];
    //Stores the filename as it was on the client computer.
    $imagename = $_FILES['fileInput']['name'];
    //Stores the filetype e.g image/jpeg
    $imagetype = $_FILES['fileInput']['type'];
    //Stores any error codes from the upload.
    $imageerror = $_FILES['fileInput']['error'];
    //Stores the tempname as it is given by the host when uploaded.
    $imagetemp = $_FILES['fileInput']['tmp_name'];

    //The path you wish to upload the image to
    $imagePath = "user_img/";

    if(is_uploaded_file($imagetemp)) {
        if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
            echo "Sussecfully uploaded your image.";
        }
        else {
            echo "Failed to move your image.";
        }
    }
    else {
        echo "Failed to upload your image.";
    }
?>