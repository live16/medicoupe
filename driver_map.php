<?php
session_start();
//database connection info db createAccount
//$_SESSION["user_id"] = $row['user_id'];
//$_SESSION["first_name"] = $row['first_name'];
//$_SESSION["last_name"] = $row['last_name'];



if ($_SESSION['user_type'] != 3 ){  //User_type = 3 = Driver
                    //echo htmlentities($row['user_id']);

                    header("Location:login.php");
                    exit;
                    } 




//database connection info db createAccount
//database connection info db createAccount
include 'connect.php';
include 'upload.php';

   
   //echo 'POST' . "<br>";
   //echo $_POST['username'];
   $getUsers = $db->prepare("SELECT * FROM appointments");

   
 
   // $getAppointmentsToday = $db->prepare("SELECT * FROM appointments WHERE appointment_date = :appointment_date");
   // $getAppointmentsToday->bindValue(':appointment_date', '2015-01-16') ;
   //$getAppointmentsToday->execute();appointments_list.php
   
   
   $getAppointmentsToday = $db->prepare("SELECT * FROM appointments WHERE DATE(appointment_date) = CURDATE() AND driver_user_id = :driver_user_id AND appointment_status != 'Cancelled'");
   $getAppointmentsToday->bindValue(':driver_user_id', $_SESSION['user_id']);
   $getAppointmentsToday->execute();
   
   
    $getAppointmentsWeek = $db->prepare("SELECT * FROM appointments WHERE DATE(appointment_date) BETWEEN CURDATE() + INTERVAL 1 DAY AND CURDATE() + INTERVAL 7 DAY AND driver_user_id = :driver_user_id");
    $getAppointmentsWeek->bindValue(':driver_user_id', $_SESSION['user_id']);
   $getAppointmentsWeek->execute();
   
   

//$getAppointmentsWeek = $db->prepare("SELECT * FROM appointments");
   //$getAppointmentsWeek->execute();
   
   $getAppointmentsCancelled = $db->prepare("SELECT * FROM appointments WHERE appointment_status = 'Cancelled' AND driver_user_id = :driver_id");
   $getAppointmentsCancelled->bindValue(':driver_id', $_SESSION['user_id']);
   $getAppointmentsCancelled->execute();
   
   //$appCancelled = $getAppointmentsCancelled->rowCount();

  // echo "<script>alert('Cancelled: ".$appCancelled."');</script>";
   
   $getAppointmentsCancelledAlerts = $db->prepare("SELECT * FROM appointments WHERE appointment_status = 'Cancelled' AND driver_user_id = :driver_id AND has_to_show_notification=1");
   $getAppointmentsCancelledAlerts->bindValue(':driver_id', $_SESSION['user_id']);
   $getAppointmentsCancelledAlerts->execute();

   $appCancelled = $getAppointmentsCancelledAlerts->rowCount();

   $setAppointmentsCancelled = $db->prepare("UPDATE appointments SET has_to_show_notification=0 WHERE appointment_status = 'Cancelled' AND driver_user_id = :driver_id ");
   $setAppointmentsCancelled->bindValue(':driver_id', $_SESSION['user_id']);
   $setAppointmentsCancelled->execute();

   $getAppointmentsAssigned = $db->prepare("SELECT * FROM appointments WHERE appointment_status = 'Assigned' AND driver_user_id = :driver_id");
   $getAppointmentsAssigned->bindValue(':driver_id', $_SESSION['user_id']);
   $getAppointmentsAssigned->execute();

   //print_r($newsAPPAsigned);
   //$appAssigned = $getAppointmentsAssigned->rowCount();
   
   
   
         
 //This is the PHP for the SESSION Image Icon in the Header  
     $userImage1 = 'user_img/' . $_SESSION["user_id"] . '.jpg';
	 $defaultImage1 = 'assets/img/default-user.png';
	 $image1 = (file_exists($userImage1)) ? $userImage1 : $defaultImage1;
   
   

   

?>




<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>MediCoupe Driver</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
   
   
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    





<!------------------------------------------------------ START NOTIFICATIONS/ALERTS ---------------------------------------------->
    <script>
     window.onload = function()
    {
        // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'

        <?php 

        if ($appCancelled > 0) { 
            echo "$('body').pgNotification({
            style: 'circle',
            title: '".$appCancelled." Cancellations',
            message: '', //Name has cancelled your ride.
            position: 'top-right',
            timeout: 0,
            type: 'danger',
            thumbnail: '' 
        }).show();";
          };

          while ($row = $getAppointmentsAssigned->fetch(PDO::FETCH_ASSOC)) :
              echo "$('body').pgNotification({
                  style: 'circle',
                  title: '".$row['appointment_date']." ".$row['appointment_time']."',
                  message: '&nbsp; &nbsp;  A new ride has been assigned.',
                  position: 'top-right',
                  appId: '".$row['appointment_id']."',
                  timeout: 0,
                  type: 'info',
                  thumbnail: '' 
              }).show();";
          endwhile;
        ?>
    } 
    </script>
    
    
    
<!------------------------------------------------------ END NOTIFICATIONS/ALERTS ---------------------------------------------->
    
   
    
    
    
    
    
    
    
   <?php 

	include 'map.php';

   
   ?>
   
   <script src="assets/js/distance.js" type="text/javascript"></script>
    
    
    
   
    
    
    
     <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      

   

      #control {
        background: #fff;
        padding: 5px;
        font-size: 14px;
        font-family: Arial;
        border: 1px solid #ccc;
        box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
        display: none;
      }

      @media print {
        #map-canvas {
          height: 500px;
          margin: 0;
        }
   
    </style>
    

    
    
    <style>
    
          #directions-panel {
        position: absolute;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
      
      }
      
      
      #directions-panel {
        height: 100%;
        float: right;
        width: 250px;
        overflow: auto;
      }
      
      </style>
    

	 <style>

       #directions-panel {
        position: absolute;
        top: 5px;
        left: 50%;
        margin-left: -180px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
      
      
      #directions-panel {
        height: 100%;
        float: right;
        width: 390px;
        overflow: auto;
      }



        #directions-panel {
          float: none;
          width: auto;
        }
      }
    </style>
   
    
    
    
    
    
    
    
    

    
    
  </head>
  
  
  
  
  
  
  
  
  
  
  
  <body class="fixed-header">


  
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
    <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        
        
        
        
        
         <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
              <img src="assets/img/medicoupe.png" alt="logo" data-src="assets/img/medicoupe.png" data-src-retina="assets/img/logo_2x.png" height="40">
            </div>
           
               <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20 ">
            
            
              <li class="p-r-15 inline">
                   <a href="#"><button href="#" class="pg-map" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Map View"></button></a>
              </li>
              
             
              <!--<li class="p-r-15 inline active-view ">
                   <a href="driver_cal.php"><button href="driver_cal.php" class="pg-calender" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="Calendar View"></button></a>
              </li>-->
              
                <li class="p-r-15 inline">
                  <a href="driver_list.php"><button href="driver_list.php" class="pg-menu_justify" style="border: none; background-color: #fff;" data-placement="bottom" title="" data-toggle="tooltip" type="button" data-original-title="List View"></button></a>
              </li>
            </ul>
            <!-- END NOTIFICATIONS LIST -->

            <a href="#" class="search-link hide" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set globe-fill m-l-20 sm-no-margin hidden-sm hidden-xs hide" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
          
          <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="bold"><?php echo($_SESSION["first_name"]);?></span> <span class="text-master"><?php echo($_SESSION["last_name"]);  ?></span>
            </div>
            
                 <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php echo $image1; ?>" alt="" data-src="<?php echo $image1; ?>" data-src-retina="<?php echo $image1; ?>" width="32" height="32">
            </span>
              </button>
                <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="#userProfile" data-toggle="modal"><i class="fa fa-user"></i> Profile</a>
                </li>
                <li class="hide"><a href="#payPal" data-toggle="modal"><i class="fa fa-user"></i> Payment</a>
                </li>
                <li ><a href="tel:+18556926873"><i class="fa fa-phone"></i> Help</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="logout.php" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      
      
      
      
      
      
      
      
        <!-- MODAL STICK UP  -->
    <div class="modal fade stick-up" id="userProfile" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header clearfix text-left ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="p-t-30">
            <span class="font-raleway-bold text-medi fs-40">user <span class="text-medi-dark">profile</span></span>
            </div>
          
            <h5><span>General Information</span></h5>
          </div>
          <div class="modal-body">
         
          
          
          <script>
          
     var switchToInput = function () {
        var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
        });
        $input.addClass("loadNum");
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };
    var switchToSpan = function () {
        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
    }
    $(".loadNum").on("click", switchToInput);
          
          </script>
          
           <div>
          
         
          
          </div>
          
           <form id="form-work" class="form-horizontal" role="form" >
                     
                      
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="fname" placeholder="<?php echo($_SESSION["first_name"]);?>" name="name">
                        </div>
                        
                       
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="lname" placeholder="<?php echo($_SESSION["last_name"]);?>" name="name">
                        </div>
                      </div>


             
                      <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="phone" placeholder="<?php echo($_SESSION["phone"]);?>" name="phone">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" placeholder="<?php echo($_SESSION["email"]);?>" name="email">
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="addressname" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="address" placeholder="<?php echo($_SESSION["mapAddress"]);?>" name="mapAddress">
                        </div>
                      </div>
                       <div class="row m-t-10">
                        <div class="col-sm-3">
                          <p> </p>
                        </div>
                        <div class="col-sm-6">
                      
                        
                <button type="button" class="btn btn-success btn-block m-t-5">Update Profile</button>
                          
                        </div>
                      </div>
                    </form>

                      
                      
                                             
                     
                     
         
                                         
                    
                    
                    
                    
                    
   







            
            
            <div class="row">
            
            
            <div class="form-group">
                        <label for="fname" class="col-sm-3 control-label">Picture</label>
                        <div class="col-sm-9">
                          
                          <div class="m-t-10">
                          
                          
                          <img src="user_img/<?php echo($_SESSION["user_id"]);?>.jpg" alt="" data-src="user_img/<?php echo($_SESSION["user_id"]);?>.jpg" data-src-retina="user_img/<?php echo($_SESSION["user_id"]);?>.jpg" width="60" height="60">
                      
<div class="p-t-10">
<form id="upload" action="driver_map.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <div class="p-t-10">
    <input class="btn btn-master" type="submit" value="Upload Image" name="submit">
    </div>
</form id="upload">
</div>

                        </div>
                          
                        </div>
                      </div>

            
            </div>
            
            
            
            
            
            
            
            
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP  -->
    


      
      
      
      
      
      
      
      
      
      
      
  
      
      
      
      
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper full-height no-padding">
        <!-- START PAGE CONTENT -->
        <div class="content full-width full-height overlay-footer no-padding">
          <!-- START CONTENT INNER -->
          
          
          
          
          
        <div id="control hide">
        
        
    	<div hidden>
      <strong>Start:</strong>
      <select id="start" onchange="calcRoute();">
       
       
       
       
        <option value="laguna beach, ca">Laguna</option>
      
      </select>
    	</div>
      
      
 
    </div>
    
    <div id="map-canvas"></div>  
          
          
          
        
        
        
          <!-- END CONTENT INNER -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2014 </span>
              <span class="font-montserrat">MediCoupe</span>.
              <span class="hint-text">All rights reserved. </span>
              
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
              <span class="sm-block"><a href="terms.html" class="m-l-10 m-r-10">Terms & Conditions</a></span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
    
      <!-- Nav tabs -->
		      <ul class="nav nav-tabs">
		      
		       <li class="active">
		          <a href="#quickview-alerts" data-toggle="tab">TODAY</a>
		        </li>
		        <li>
		          <a href="#quickview-chat" data-toggle="tab">WEEK</a>
		        </li>
		        <li>
		          <a href="#quickview-notes" data-toggle="tab">PAST</a>
		        </li>
 
		      </ul>
		      
		      
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->
      
      
      
      <div class="tab-content">
      
   
      
      
      
       <!-- BEGIN Notes !-->
        <div class="tab-pane fade in no-padding" id="quickview-notes">
          <div class="view-port clearfix quickview-notes" id="note-views">
            <!-- BEGIN Note List !-->
            <div class="view list" id="quick-note-list">
              <div class="toolbar clearfix">
                <ul class="pull-right ">
                  <li>
                    <a href="#" class="delete-note-link"><i class="fa fa-trash-o"></i></a>
                  </li>
                  <li>
                    <a href="#" class="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="fa fa-plus"></i></a>
                  </li>
                </ul>
                <button class="btn-remove-notes btn btn-xs btn-block hide"><i class="fa fa-times"></i> Delete</button>
              </div>
              <ul>
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="1" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox1" type="checkbox" value="1">
                      <label for="qncheckbox1"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox2" type="checkbox" value="1">
                      <label for="qncheckbox2"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="2" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox3" type="checkbox" value="1">
                      <label for="qncheckbox3"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="3" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox4" type="checkbox" value="1">
                      <label for="qncheckbox4"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
                <!-- BEGIN Note Item !-->
                <li data-noteid="4" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                  <div class="left">
                    <!-- BEGIN Note Action !-->
                    <div class="checkbox check-warning no-margin">
                      <input id="qncheckbox5" type="checkbox" value="1">
                      <label for="qncheckbox5"></label>
                    </div>
                    <!-- END Note Action !-->
                    <!-- BEGIN Note Preview Text !-->
                    <p class="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    <!-- BEGIN Note Preview Text !-->
                  </div>
                  <!-- BEGIN Note Details !-->
                  <div class="right pull-right">
                    <!-- BEGIN Note Date !-->
                    <span class="date">12/12/14</span>
                    <a href="#"><i class="fa fa-chevron-right"></i></a>
                    <!-- END Note Date !-->
                  </div>
                  <!-- END Note Details !-->
                </li>
                <!-- END Note List !-->
                
                
              </ul>
            </div>
            <!-- END Note List !-->
            
            
            
            
            <div class="view note" id="quick-note">
              <div>
                <ul class="toolbar">
                  <li><a href="#" class="close-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i class="pg-arrow_left"></i></a>
                  </li>
                  <li><a href="#" class="Bold"><i class="fa fa-bold"></i></a>
                  </li>
                  <li><a href="#" class="Italic"><i class="fa fa-italic"></i></a>
                  </li>
                  <li><a href="#" class=""><i class="fa fa-link"></i></a>
                  </li>
                </ul>
                <div class="body">
                  <div>
                    <div class="top">
                      <span>21st april 2014 2:13am</span>
                    </div>
                    <div class="content">
                      <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
       <!-- END Notes !-->
      
      
      

      
      
        <!-- BEGIN Alerts !-->
        <div class="tab-pane fade in active no-padding" id="quickview-alerts">
          <div class="view-port clearfix" id="alerts">
            <!-- BEGIN Alerts View !-->
            <div class="view bg-white">
       
             
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default navbar-sm">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Rides
                  </div>
                  <!-- BEGIN Header Controler !-->
                   
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
          
              
              
              <!-- BEGIN Alert List !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <!-- BEGIN List Group !-->
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Today
                  </div>
                  <!-- END List Group Header!-->
                  
                      
                 
              
            <div class="row">
              <div class="col-md-12">
                <div class="sm-m-l-5 sm-m-r-5">
                  
                
                  
                <div class="panel panel-group panel-transparent" data-toggle="collapse" id="accordion">
                  
                  
                   
                <?php while ($row = $getAppointmentsToday->fetch(PDO::FETCH_ASSOC)) : ?>
                
                
                
               
                   
                   <div class="panel panel-default">
                      
                      <div class="panel-heading">
                      
                      
                      <div class="pull-right p-t-20 p-r-10">
                    
                     <span                                     
                                   				<?php if($row['appointment_status'] == 'Confirmed')
                                                            {
                                                                echo "class='label label-success'>"; 

                                                                echo 'Confirmed';
																
                                                                
                                                            } 
                                                       
                                                                elseif ($row['appointment_status'] == 'Scheduled') 
                                                            {
                                                            	echo "class='label label-warning'>";
                                                                echo 'Scheduled';
																
                                                                
                                                            }
                                                            
														
															
													
                                                            
                                                                elseif ($row['appointment_status'] == 'Cancelled') 
                                                            {
                                                            	echo "class='label label-important'>";
                                                                echo 'Cancelled';
															
                                                                
                                                            }
                                                            
                                                            

                                                            
                                                            
                                                                else 
                                                            {
                                                            echo "class='label label-default'>";
                                                                echo 'N/A';
                                                            }
                                                    ?>
                         </span>  
                    
                    </div>
                      
                  
                      
                      <div class="card-header clearfix">
                         <a class="collapsed" data-parent="#accordion" data-toggle=
                            "collapse" href="#collapse<? echo $row['appointment_id'] ;  ?>">
                        <h5><? echo $row['appointment_title'] ;  ?></h5>
                        <h6>(<? echo $row['appointment_id'] ;  ?>)</h6>
                         </a>
                      </div>

	                     
                      </div>
                      <div class="panel-collapse collapse"  id="collapse<? echo $row['appointment_id'] ;  ?>">
                      <div class="p-l-20 p-b-20"> 
                       
                             <label class="font-montserrat fs-11 m-t-15">PICK UP ADDRESS</label>
                           
                            <p>
						  	<a href="#"><? echo $row['pickup_mapAddress'] ;  ?></a>
						  </p> 
						   
<label class="font-montserrat fs-11 m-t-15">DESTINATION ADDRESS</label>
                           
                            <p>
						  	<a href="#"><? echo $row['destination_mapAddress'] ;  ?></a>
						  </p> 
                   
				       
				       
				       
				        
                       
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseNote<? echo $row['appointment_id'] ;  ?>" aria-expanded="true" aria-controls="collapseOne">
                             <label class="font-montserrat fs-11 m-t-15">NOTES</label>
                            </a>
                        
                  
 
                      
                      <div id="collapseNote<? echo $row['appointment_id'] ;  ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body no-padding">
                          
                        
                      
                            <p>
						  	<? echo $row['notes'] ;  ?>
						  </p>  
						  
                                      

                   
                        </div>
                      </div>
                      </div>


                  
                              
						 <div>
						 </div>
                        </div>
       <?php endwhile; ?>
          
                        </div>
                   
     
     

     
 

                      
                        
                  </div>
                </div>
                </div>
                </div>
                </div>
                
                
                
                
                
                
                <!-- END List Group !-->
                <div class="list-view-group-container">
                
                
                
                
                
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    This Week
                  </div>
                  <!-- END List Group Header!-->
                  
                  
                  
                        
            <div class="row">
              <div class="col-md-12">
                <div class="sm-m-l-5 sm-m-r-5">
                  
                
                  
                  <div class="panel panel-group panel-transparent" data-toggle="collapse" id="accordion">
                  
              <?php while ($row = $getAppointmentsWeek->fetch(PDO::FETCH_ASSOC)) : ?>
                
                
                
               
                   
                   <div class="panel panel-default">
                      
                      <div class="panel-heading">
                      
                      
                      <div class="pull-right p-t-20 p-r-10">
                    
                     <span                                     
                                   				<?php if($row['appointment_status'] == 'Confirmed')
                                                            {
                                                                echo "class='label label-success'>"; 

                                                                echo 'Confirmed';
																
                                                                
                                                            } 
                                                       
                                                                elseif ($row['appointment_status'] == 'Scheduled') 
                                                            {
                                                            	echo "class='label label-warning'>";
                                                                echo 'Scheduled';
																
                                                                
                                                            }
                                                            
														
															
													
                                                            
                                                                elseif ($row['appointment_status'] == 'Cancelled') 
                                                            {
                                                            	echo "class='label label-important'>";
                                                                echo 'Cancelled';
															
                                                                
                                                            }
                                                            
                                                            

                                                            
                                                            
                                                                else 
                                                            {
                                                            echo "class='label label-default'>";
                                                                echo 'N/A';
                                                            }
                                                    ?>
                         </span>  
                    
                    </div>
                      
                  
                      
                      <div class="card-header clearfix">
                         <a class="collapsed" data-parent="#accordion" data-toggle=
                            "collapse" href="#collapse<? echo $row['appointment_id'] ;  ?>">
                        <h5><? echo $row['appointment_title'] ;  ?></h5>
                        <h6>(<? echo $row['appointment_id'] ;  ?>)</h6>
                         </a>
                      </div>

	                     
                      </div>
                      <div class="panel-collapse collapse"  id="collapse<? echo $row['appointment_id'] ;  ?>">
                      <div class="p-l-20 p-b-20"> 
                       
                             <label class="font-montserrat fs-11 m-t-15">PICK UP ADDRESS</label>
                           
                            <p>
						  	<a href="#"><? echo $row['pickup_mapAddress'] ;  ?></a>
						  </p> 
						   
<label class="font-montserrat fs-11 m-t-15">DESTINATION ADDRESS</label>
                           
                            <p>
						  	<a href="#"><? echo $row['destination_mapAddress'] ;  ?></a>
						  </p> 
                   
				       
				       
				       
				        
                       
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseNote<? echo $row['appointment_id'] ;  ?>" aria-expanded="true" aria-controls="collapseOne">
                             <label class="font-montserrat fs-11 m-t-15">NOTES</label>
                            </a>
                        
                  
 
                      
                      <div id="collapseNote<? echo $row['appointment_id'] ;  ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body no-padding">
                          
                        
                      
                            <p>
						  	<? echo $row['notes'] ;  ?>
						  </p>  
						  
                                      

                   
                        </div>
                      </div>
                      </div>


                  
                              
						 <div>
						 </div>
                        </div>
       <?php endwhile; ?>                    
                 
                    
                    
               
                    
                    </div>
                    
                    
                 
                    
                    
                                      
                    
                    
                  </div>
                </div>
                </div>
              

                  
                
                </div>
                
                
                
                
                
                
                
                <div class="list-view-group-container">
                  <!-- BEGIN List Group Header!-->
                  <div class="list-view-group-header text-uppercase">
                    Cancellations
                  </div>
                  <!-- END List Group Header!-->
                 
                 
                 
                 
                 
                       
            <div class="row">
              <div class="col-md-12">
                <div class="sm-m-l-5 sm-m-r-5">
                  
                
                  
                  <div class="panel panel-group panel-transparent" data-toggle="collapse" id="accordion">
                  
                <?php while ($row = $getAppointmentsCancelled->fetch(PDO::FETCH_ASSOC)) : ?>
                   
                
                   
                   
                      <div class="panel panel-default alert-danger">
                      
                      <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-parent="#accordion" data-toggle=
                            "collapse" href="#collapse<? echo $row['appointment_id'] ;  ?>">
                            <? echo $row['appointment_date'] ;  ?> &nbsp; <? echo $row['appointment_time'] ;  ?></a>
                          </h4>
                      </div>
                      
                      <div class="panel-collapse collapse"  id="collapse<? echo $row['appointment_id'] ;  ?>">
                      
                        
                        <div class="p-l-20 p-t-20 list-view-group-header-custom ">
                          
                          
                          <span class="text-master text-uppercase">Ride ID # &nbsp;</span>
                          <span class="text-master link text-uppercase" ><? echo $row['appointment_id'] ;  ?><br></span>
                          
                            <span class="text-master link text-uppercase" ><? echo $row['transport_type'] ;  ?><br></span>
                          
                          
                          <h4 class="text-complete" ><? echo $row['appointment_title'] ;  ?></h4>
                          
                          <span class "text-master">Cancelled By: <span class="text-danger text-uppercase" ><? echo $row['cancelBy_user_id'] ;  ?></span><br>
                          <span class="text-danger text-uppercase" ><? echo $row['appointment_cancel_timeStamp'] ;  ?></span>
                          
                          
                       
                          
                          <p class="text-master fs-12" ><? echo $row['notes'] ;  ?></p><br>
    
                        </div>
                        
                         <div class="panel">
                         
                         	<div class="panel-default">
                         	<div class="pickup-address"></div>
							 	<div class="panel-heading">
                       
                          
								 	<span class="text-master"> <? echo $row['pickup_mapAddress'] ;  ?><br></span>
								 </div>
							</div>
                          </div>



						 <div class="panel">
                        
                         	<div class="panel-default">
                         	 <div class="destination-address"></div>
							 	<div class="panel-heading">
                       
                          
								 	<span class="text-master"> <? echo $row['destination_mapAddress'] ;  ?><br></span>
								 </div>
							</div>
                          </div>




                          
                              
     <div>
    <button id="end" onclick="calcRoute();" href="#modalSlideLeft" data-toggle="modal" class="btn btn-block btn-default disabled" value="<? echo $row['destination_mapAddress'] ;  ?>">Get Directions</button>
     </div>
    


                       
                        
                      
                        
                      </div>
                      
                    </div>                   
                    
                    
                    
                    
                    
                    
                      <?php endwhile; ?>
                    
                 
                    
                    
               
                    
                    </div>
                    
                    
                 
                    
                    
                                      
                    
                    
                  </div>
                </div>
                </div>
              

                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                                 </div>
              </div>
              <!-- END Alert List !-->
            </div>
            <!-- EEND Alerts View !-->
          </div>
        </div>
        <!-- END Alerts !-->
        
        
        
        
        
        
          <div class="tab-pane fade in active no-padding" id="quickview-chat">
           
          <div class="view-port clearfix" id="chat">
          
            <div class="view bg-white">
            
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-plus"></i>
                  </a>
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Customer Support
                    
                  </div>
                  <!-- BEGIN Header Controler !-->
              
                  <!-- END Header Controler !-->
                </div>
              </div>
              
              
              
              
              
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    Recent</div>
                 
                 
                  <ul>
                  
                  
                  
                  
                  
                    <!-- BEGIN Chat User List Item  !-->
                    
                    
                    
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" src="assets/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-success"> </span> Active</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    
                    
                    
                    
                    
                    
                    
                    
                    
                  </ul>
                </div>
                
                
                
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">Old</div>
                  
                  
                  
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/2x.jpg" data-src="assets/img/profiles/2.jpg" src="assets/img/profiles/2x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" src="assets/img/profiles/3x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">MediCoupe Support</span>
                           <span class="pull-right"><span class="badge badge-empty badge-important"> </span> Closed</span>
                          <span class="block text-master hint-text fs-12">Hey ya'll! My driver ...</span>

                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
               
               
               
               
              </div>
            </div>
            
            
            
            
            
            
            
            
            
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    MediCoupe Support
                    <div class="fs-11 hint-text">Online</div>
                  </div>
                  <a href="#" class="link text-master inline action p-r-10 pull-right ">
                    <i class="pg-more"></i>
                  </a>
                </div>
              </div>
              <!-- END Header  !-->
              <!-- BEGIN Conversation  !-->
              <div class="chat-inner" id="my-conversation">
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Hello there
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Hey
                  </div>
                </div>
                <!-- END From Them Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Did you check out Pages framework ?
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Its an awesome chat
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Yea
                  </div>
                </div>
                <!-- END From Them Message  !-->
              </div>
              <!-- BEGIN Conversation  !-->
              <!-- BEGIN Chat Input  !-->
              <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="col-xs-8 no-padding">
                    <input type="text" class="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something">
                  </div>
                  <div class="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                    <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
         
         
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    
    
    
    
    
    
    
    
    
     <div class="modal fade slide-right" id="modalSlideLeft" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
          <div class="modal-content">
           
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
      
           
                <div class="modal-body full-height">
                 
                 
                 <div id="directions-panel"></div>
                 

                  <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                </div>
              </div>
     
          </div>
     
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END MODAL STICK UP SMALL ALERT -->
    
    
    
    
    
    






    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
   <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    
    
    
    <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
    <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
    
    
    
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <script src="ajaxCall.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc" type="text/javascript"></script> -->
    <!--<script src="assets/js/google_map.js" type="text/javascript"></script> -->
    <script src="notifications.js" type="text/javascript"></script>
    <script src="assets/js/form_elements.js" type="text/javascript"></script>
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    
    
    
    

    
    
    
    
    
  </body>
</html>