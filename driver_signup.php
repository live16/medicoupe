<?php

//database connection info db createAccount
include 'connect.php';

if(isset($_POST['user_signup']))
{
   try {
        $stmt = $db->prepare("INSERT INTO user (user_id,user_type,first_name,last_name,phone,gender,address1,
								city,state,zip_code,email,password,emergency_first_name,emergency_last_name,emergency_phone,timeStamp,active) 
					  VALUES (:user_id,:user_type,:first_name,:last_name,:phone,:gender,:address1,
								:city,:state,:zip_code,:email,:password,:emergency_first_name,:emergency_last_name,:emergency_phone,:timeStamp,:active)"); 
						
						$date = new DateTime();

		
								       
        
		$stmt->bindValue(':user_id', uniqid() );
		$stmt->bindValue(':user_type', 3);
		$stmt->bindValue(':first_name', $_POST['first_name']);
		$stmt->bindValue(':last_name', $_POST['last_name']);
		$stmt->bindValue(':phone', $_POST['phone']);
		$stmt->bindValue(':gender', NULL);
		$stmt->bindValue(':address1', NULL);
		$stmt->bindValue(':city', $_POST['city']);
		$stmt->bindValue(':state', NULL);
		$stmt->bindValue(':zip_code', NULL);
		$stmt->bindValue(':email', $_POST['email']);
		$stmt->bindValue(':password', $_POST['password']);
		$stmt->bindValue(':emergency_first_name', NULL);
		$stmt->bindValue(':emergency_last_name', NULL);
		$stmt->bindValue(':emergency_phone', NULL);
		$stmt->bindValue(':timeStamp', $date->format('Y-m-d H:i:s') );
		$stmt->bindValue(':active', 1);


$stmt->execute();

        //$stmt->execute();
        // This is in the PHP file and sends a Javascript alert to the client
        //$message = $_POST[$date];
       
}   catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'fail';
}

}

?>




 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>





<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Signup for MediCoupe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header   " onload="initialize()">
    <!-- START PAGE-CONTAINER -->
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="assets/img/demo/medicoupe-background.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          
          <p class="small">
            © 2014-2015 MediCoupe.
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        
        	
        	
        	<a class="btn btn-default btn-block" href="login.php">Already have a <strong>MediCoupe</strong> account?</a>
        	
        	        	 
    
          
          <p class="p-t-25" style="text-align: center;">Create a New Account</p>
          
          
          
          <!-- START SIGNUP Form -->
          <form id="form-register" class="p-t-15" role="form" action="signup.php" method="post">
          	<input type="hidden" name="user_signup" id="user_signup" value="1"/>
              
              
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>First Name</label>
                    <input type="text" name="first_name" placeholder="John" class="form-control" required>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="form-group form-group-default">
                    <label>Last Name</label>
                    <input type="text" name="last_name" placeholder="Smith" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control" required>
                  </div>
                </div>
              </div>
              
            
              
              
              
               <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Email</label>
                    <input type="email" name="email" placeholder="We will send loging details to you" class="form-control" required>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>Password</label>
                    <input type="password" name="pass" placeholder="Minimum of 4 Charactors" class="form-control" required>
                  </div>
                </div>
              </div>
              
              
                  <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default" id="locationField">
                    <label>City</label>
                    <input id="autocomplete" type="text" name="location" placeholder="Enter Your Location" class="form-control"  onFocus="geolocate()" required>
                  </div>
                </div>
              </div>
              
    

    <table id="address" hidden="">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number" name="address1"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3"><input class="field" id="locality" name="city"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" name="state" disabled="true"></input></td>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code" name="zip_code"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>
              
                 
             
              
              <div class="row m-t-10">
                <div class="col-md-12">
                
                
                <div class="pull-left">
                        <div class="checkbox check-success  ">
                          <input type="checkbox" checked="checked" value="1" id="checkbox-agree">
                          <label for="checkbox-agree"><p>I plan to drive a commercially insured and licensed livery or For-Hire vehicle</p></label>
                        </div>
                      </div>
                      
                      
              	
                 
                </div>
                
                 <button class="btn btn-success btn-block" type="submit">Create a new account</button>
                 
                 <br>
                <div class="col-md-12" style="text-align: center;">
                  <p>By clicking Next, I agree that MediCoupe or its representatives may contact me by email, phone, or SMS (including by automatic telephone dialing system) at the email address or number I provide, including for marketing purposes. I understand that MediCoupe is a request tool, not a transportation carrier. I also confirm that I have read and understand MediCoupe's Driver Application Privacy Statement and Terms and Conditions.</p>
                </div>
              </div>
             
            </form>
            
        <!--END SIGNUP Form-->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- BEGIN VENDOR JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
    <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script>
    $(function()
    {
      $('#form-register').validate()
    })
    </script>
  </body>
</html>